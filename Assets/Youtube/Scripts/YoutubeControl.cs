using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using DG.Tweening;

namespace YoutubePlayer
{
    [RequireComponent(typeof(VideoPlayer))]
    public class YoutubeControl : MonoBehaviour
    {
        public enum VideoState
        {
            Paused,
            Playing,

            Changing,
            LoadChanging,
            LoadBadRate

        }

        private VideoState State = VideoState.Paused;

        //[SerializeField] private YTVideoScriptableObject source;

        [SerializeField] private YoutubePlayer ytPlayer;
        private VideoPlayer _videoPlayer;

        [SerializeField] private Button btnPlay;
        [SerializeField] private Slider slider;

        private bool isPreparing;

        [SerializeField] private Sprite playSprite;
        [SerializeField] private Sprite pauseSprite;

        public void SetURL(string url) => ytPlayer.youtubeUrl = url;

        //bool AttempedPaused { get => _paused; set { _paused = value; btnPlay.transform.GetChild(0).GetComponent<Image>().sprite = value ? playSprite : pauseSprite; } }
        public void OnVideoFinish(VideoPlayer _videoPlayer)
        {
            // 
            Debug.Log("Video Finished");
            PauseVideo();
        }


        public void SetAttempPause(bool value, bool DoAnim = false)
        {
            _paused = value;

            btnPlay.transform.GetChild(0).GetComponent<Image>().sprite = value ? playSprite : pauseSprite;

            if (DoAnim)
            {
                PlayPauseAnimRect.GetChild(0).GetComponent<Image>().sprite = value ? playSprite : pauseSprite;

                PlayPauseAnimRect.DOScale(1.3f, 0.5f);

                CanvasGroup animGroup = PlayPauseAnimRect.GetComponent<CanvasGroup>();

                animGroup.DOFade(1, 0.25f).OnComplete(() =>
                {

                    animGroup.DOFade(0, 0.25f).OnComplete(() => PlayPauseAnimRect.SetLocalScale(1, 1));
                });
            }
        }

        public bool GetAttempPause() => _paused;

        bool _paused = true;

        [SerializeField] private LoadingCover loadingCover;

        [SerializeField] private RectTransform PlayPauseAnimRect;

        [SerializeField] private Text txtTime;

        #region SCREEN

        public void OnScreenTouchClick()
        {
            OnPlayButtonClick(true);
        }

        #endregion

        #region VIDEO SLIDER
        public void OnSliderEnter()
        {
            RectTransform SliderRect = slider.GetComponent<RectTransform>();
            slider.handleRect.DOScale(1, 0.3f);
            SliderRect.DOSizeDelta(new Vector2(SliderRect.sizeDelta.x, 13), 0.3f);
        }

        public void OnSliderExit()
        {
            RectTransform SliderRect = slider.GetComponent<RectTransform>();
            slider.handleRect.DOScale(0, 0.3f);
            SliderRect.DOSizeDelta(new Vector2(SliderRect.sizeDelta.x, 8), 0.3f);
        }

        public void OnVideoSliderDown()
        {

            State = VideoState.Changing;
        }

        public void OnVideoSliderUp()
        {

            Debug.Log(_videoPlayer.time);
            targetTime = Mathf.Lerp(0, (float)_videoPlayer.length, slider.value);
            _videoPlayer.time = targetTime;
            slider.SetValueWithoutNotify(Mathf.InverseLerp(0, (float)_videoPlayer.length, targetTime));

            StartCoroutine(WaitUnitlReproduce());

            State = VideoState.LoadChanging;
        }


        void InitVideoSlider()
        {
            RectTransform SliderRect = slider.GetComponent<RectTransform>();
            slider.handleRect.SetLocalScale(0, 0);
            SliderRect.SetAnchorSize(SliderRect.sizeDelta.x, 8);
        }
        #endregion

        #region SOUND SLIDER

        [SerializeField] private RectTransform SoundCRect;
        bool isSoundSliderDragging = false;
        bool isSoundSliderHover = false;
        public void OnSoundSliderDown()
        {
            isSoundSliderDragging = true;
        }

        public void OnSoundSliderUp()
        {
            isSoundSliderDragging = false;
            if (!isSoundSliderHover)
            {
                SoundCRect.DOSizeDelta(new Vector2(50, SoundCRect.sizeDelta.y), 0.25f);
                txtTime.rectTransform.DOAnchorPosX(165, 0.25f);
            }
        }

        public void OnSoundSliderEnter()
        {
            isSoundSliderHover = true;
            SoundCRect.DOSizeDelta(new Vector2(145, SoundCRect.sizeDelta.y), 0.25f);
            txtTime.rectTransform.DOAnchorPosX(245, 0.25f);

        }

        public void OnSoundSliderExit()
        {
            isSoundSliderHover = false;

            if (!isSoundSliderDragging)
            {
                SoundCRect.DOSizeDelta(new Vector2(50, SoundCRect.sizeDelta.y), 0.25f);
                txtTime.rectTransform.DOAnchorPosX(165, 0.25f);

            }

        }

        public void OnSoundSliderValueChanged(float value)
        {
            _videoPlayer.SetDirectAudioVolume(0, value);

            if (value > 0)
            {
                if (isSoundOff)
                {
                    SetSoundOff(false);
                }

                lastValue = value;

            }
            else
            {
                SetSoundOff(true);
            }

        }

        void SetSoundOff(bool value)
        {
            isSoundOff = value;
            SoundCRect.GetChild(0).GetChild(1).gameObject.SetActive(value);
        }

        float lastValue;
        bool isSoundOff;
        void InitSound()
        {
            SoundCRect.GetChild(0).GetComponent<Button>().onClick.AddListener(() => {

                Slider soundSlider = SoundCRect.GetChild(1).GetComponent<Slider>();

                if (isSoundOff)
                {
                    soundSlider.value = lastValue;
                    SetSoundOff(false);
                }
                else
                {
                    lastValue = soundSlider.value;
                    soundSlider.value = 0;
                    SetSoundOff(true);
                }
            });
        }

        #endregion


        #region UI FADE
        bool checkUI = true;
        [SerializeField] private CanvasGroup canvasGroup;
        float timerUI;
        bool UI_Showed;

        void CheckUIShowed()
        {
            if (!checkUI)
                return;

            Vector2 mouseVel = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")).normalized;

            if (!UI_Showed)
            {

                if (mouseVel.sqrMagnitude > 0.1f || Input.GetMouseButtonDown(0))
                {
                    canvasGroup.DOFade(1, 0.3f);
                    canvasGroup.blocksRaycasts = true;
                    UI_Showed = true;
                    timerUI = 2;
                }
            }
            else
            {
                if (Input.GetMouseButton(0))
                {
                    timerUI = 2;
                    return;
                }
                if (mouseVel.sqrMagnitude < 0.1f)
                {
                    timerUI -= Time.deltaTime;
                    if (timerUI <= 0)
                    {
                        canvasGroup.DOFade(0, 0.3f);
                        canvasGroup.blocksRaycasts = false;
                        UI_Showed = false;
                    }
                }
                else
                {
                    timerUI = 2;
                }
            }

        }

        #endregion
        // Start is called before the first frame update
        void Start()
        {
            //ytPlayer.youtubeUrl = source.GetURL(Session.Year, (int)Session.Course, Session.Unit);

            _videoPlayer = GetComponent<VideoPlayer>();

            _videoPlayer.prepareCompleted += OnPrepareCompleted;

            _videoPlayer.loopPointReached += OnVideoFinish;

            btnPlay.onClick.AddListener(() => OnPlayButtonClick());

            slider.SetValueWithoutNotify(0);

            InitSound();
            InitVideoSlider();
        }


        private void LateUpdate()
        {

            if (State == VideoState.Playing)
            {
                slider.SetValueWithoutNotify(Mathf.InverseLerp(0, (float)_videoPlayer.length, (float)_videoPlayer.time));
                SetTime();
            }

            PrintState();

            CheckUIShowed();

        }

        float targetTime;


        void OnPlayButtonClick(bool DoAnim = false)
        {
            if (_videoPlayer.isPlaying) // Pausar
            {
                PauseVideo();
                //AttempedPaused = true;
                SetAttempPause(true, DoAnim);
            }
            else //
            {

                if (_videoPlayer.isPrepared)
                {
                    //AttempedPaused = false;
                    SetAttempPause(false, DoAnim);
                    PlayVideo();
                }
                else
                {
                    if (isPreparing)
                    {
                        //AttempedPaused = !AttempedPaused;
                        SetAttempPause(!GetAttempPause(), DoAnim);
                    }
                    else
                    {
                        Prepare();
                        //AttempedPaused = false;
                        SetAttempPause(false, DoAnim);

                    }
                }
            }

        }

        IEnumerator CheckForBadStopRate()
        {
            yield return new WaitForSeconds(0.1f);

            while (_videoPlayer.isPlaying)
            {
                long frame = _videoPlayer.frame;
                yield return new WaitForSeconds(0.05f);
                if (frame == _videoPlayer.frame)
                {

                    if (State == VideoState.Playing)
                    {
                        State = VideoState.LoadBadRate;
                        targetTime = (float)_videoPlayer.time;
                        StartCoroutine(WaitUnitlReproduce());
                    }

                    yield break;
                }
            }
        }

        IEnumerator WaitUnitlReproduce()
        {
            long frame = _videoPlayer.frame;
            loadingCover.StartLoading();

            yield return new WaitUntil(() => _videoPlayer.frame != frame);

            if (State == VideoState.LoadBadRate)
            {
                _videoPlayer.time = targetTime;
                print("After Bad Rate");
            }
            else
                print("After Changing");
            State = VideoState.Playing;

            loadingCover.FinishLoading();
            yield return new WaitForSeconds(0.1f);


            StartCoroutine(CheckForBadStopRate());
        }

        public async void Prepare()
        {
            loadingCover.StartLoading();
            isPreparing = true;
            print("Cargando...");
            try
            {
                await ytPlayer.PrepareVideoAsync();
                print("Video cargado");
                isPreparing = false;

            }
            catch (System.Exception ex)
            {
                print("No fue posible cargar el video : " + ex.Message);
                isPreparing = false;
            }

        }

        public void OnPrepareCompleted(VideoPlayer player)
        {
            //...
            if (GetAttempPause() == false)
            {
                loadingCover.FinishLoading();
                player.time = 0;
                PlayVideo();
                State = VideoState.Playing;
            }
            else
                State = VideoState.Paused;

        }

        void SetTime()
        {
            float maxTime = (float)_videoPlayer.length;

            float CurrentTime = (float)_videoPlayer.time;

            float second = CurrentTime % 60;
            float secondMax = maxTime % 60;
            txtTime.text = string.Format("{0:00}:{1:00}", Mathf.FloorToInt(CurrentTime / 60), second) + " / " + string.Format("{0:00}:{1:00}", Mathf.FloorToInt(maxTime / 60), secondMax);
        }

        public void PlayVideo() { _videoPlayer.Play(); if (loadingCover.isLoading) loadingCover.FinishLoading(); StartCoroutine(CheckForBadStopRate()); }
        public void PauseVideo() => _videoPlayer.Pause();
        public void ResetVideo() { _videoPlayer.Stop(); _videoPlayer.Play(); }
        public void UnloadVideo()
        {
            _videoPlayer.Stop();
            _videoPlayer.targetTexture.Release();
            _videoPlayer.time = 0;
        }

        void OnDestroy()
        {
            _videoPlayer.targetTexture.Release();
            _videoPlayer.prepareCompleted -= OnPrepareCompleted;

        }

        void PrintState() => print("STATE: " + State);
    }
}

