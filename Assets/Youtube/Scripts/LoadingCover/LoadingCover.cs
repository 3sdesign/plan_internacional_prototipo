﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LoadingCover : MonoBehaviour
{
    [SerializeField]
    private Image thisBg = null;

    [SerializeField]
    private Image loadingCircle = null;

    [SerializeField] private float targetBgAlpha = 0;

    [SerializeField]
    private float timing = 0.35f;

    [HideInInspector]
    public bool isLoading = false;

    public void StartLoading()
    {
        isLoading = true;
        thisBg.SetAlpha(0);
        loadingCircle.SetAlpha(0);

        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);

        }

        thisBg.DOFade(targetBgAlpha, timing);
        loadingCircle.DOFade(1, timing);
    }

    public void StartedLoading()
    {
        isLoading = true;


        thisBg.SetAlpha(targetBgAlpha);
        loadingCircle.SetAlpha(1);

        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);

        }
    }

    public void FinishLoading()
    {
        loadingCircle.DOFade(0, timing).OnComplete(() =>
        {
            thisBg.DOFade(0, timing).OnComplete(Hide);
        });

    }

    public void Hide()
    {
        if (!isLoading)
            return;

        gameObject.SetActive(false);
        isLoading = false;
    }
}
