using UnityEngine;
using DG.Tweening;

public class YTVideoLoader : MonoBehaviour
{
    [SerializeField] private YTVideoScriptableObject source;
    [SerializeField] private YoutubePlayer.YoutubeControl ytControl;


    string targetURL;

    private void Start()
    {
        targetURL = source.GetURL(Session.GlobalGrade, Session.Eda, Session.Ada);
        ytControl.transform.SetLocalScale(0, 0);
        ytControl.gameObject.SetActive(false);
    }

    public void LaunchVideo()
    {
        ytControl.gameObject.SetActive(true);
        ytControl.SetURL(targetURL);
        ytControl.Prepare();
        ytControl.SetAttempPause(false);
        ytControl.transform.DOScale(1, 0.6f).SetEase(Ease.OutBack);
    }

    public void CloseVideo()
    {
        ytControl.UnloadVideo();
        ytControl.transform.DOScale(0, 0.6f).SetEase(Ease.InBack).OnComplete(() => ytControl.gameObject.SetActive(false));
    }
}
