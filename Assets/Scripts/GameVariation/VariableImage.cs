using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class VariableImage : MonoBehaviour
{
    [SerializeField] private Sprite[] arts;
    [SerializeField] private bool preserveAspect = true;
    [SerializeField] private bool setNativeSize = false;

    private void Start() => Refresh();

    public void Refresh()
    {
        Image image = GetComponent<Image>();

        int index = Mathf.Clamp(Session.GetLevel(), 0, arts.Length - 1);

        if (arts[index] != null)
            image.sprite = arts[index];

        image.preserveAspect = preserveAspect;

        if (setNativeSize)
            image.SetNativeSize();
    }
}
