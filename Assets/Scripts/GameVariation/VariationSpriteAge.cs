using UnityEngine;

public class VariationSpriteAge : MonoBehaviour
{
    [SerializeField] private Sprite[] sprts;

    private void Start()
    {
        Refresh();
    }
    public void Refresh()
    {
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();

        if (sprts[Session.KidType()] != null)
            sprite.sprite = sprts[Session.KidType()];
    }
}
