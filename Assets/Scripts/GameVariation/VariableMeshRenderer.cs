using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class VariableMeshRenderer : MonoBehaviour
{
    [SerializeField] private Texture2D[] arts;

    private void Start() => Refresh();

    public void Refresh()
    {
        MeshRenderer mesh = GetComponent<MeshRenderer>();

        int index = Mathf.Clamp(Session.GetLevel(), 0, arts.Length - 1);

        if (arts[index] != null)
        {
            MaterialPropertyBlock block = new MaterialPropertyBlock();
            block.SetTexture("_MainTex", arts[index]);
            mesh.SetPropertyBlock(block);
        }
    }
}
