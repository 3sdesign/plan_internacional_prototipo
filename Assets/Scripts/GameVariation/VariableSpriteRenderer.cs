using UnityEngine;

public class VariableSpriteRenderer : MonoBehaviour
{
    [SerializeField] private Sprite[] arts;

    private void Start() => Refresh();

    public void Refresh()
    {
        SpriteRenderer image = GetComponent<SpriteRenderer>();

        int index = Mathf.Clamp(Session.GetLevel(), 0, arts.Length - 1);

        if (arts[index] != null)
            image.sprite = arts[index];
    }
}
