using UnityEngine;

public static class AndroidBridge
{
    private static AndroidJavaObject _context = null;
    public static AndroidJavaObject Context
    {
        get
        {
            if (_context == null)
            {
                AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                _context = unity.GetStatic<AndroidJavaObject>("currentActivity");
                return _context;
            }
            else return _context;
        }
    }

    public static void Toast(string message)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidToast(message);
#else
        Debug.Log(message);
#endif
    }

    private static void AndroidToast(string message)
    {
        AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
        AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", Context, message, Toast.GetStatic<int>("LENGTH_LONG"));
        toast.Call("show");
    }
}
