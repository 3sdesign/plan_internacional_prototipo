using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using static UnityEngine.SceneManagement.SceneManager;

public class VideoScreen : MonoBehaviour
{
    private static string _nextScene = string.Empty;
    private static string _videoURL = string.Empty;

    [SerializeField] private Text Percent = null;
    [SerializeField] private Image Bar = null;

    public static void Load(string videoURL, string nextScene)
    {
        _nextScene = nextScene;
        _videoURL = videoURL;
        LoadVideoScene();
    }

    public static void LoadVideoScene()
    {
        if (!string.IsNullOrEmpty(_nextScene)) LoadScene("Video");
    }

    private IEnumerator Start()
    {
        string path = Path.Combine(Application.persistentDataPath, "video.mp4");

        using (UnityWebRequest www = UnityWebRequest.Get(_videoURL))
        {
            AndroidBridge.Toast("Descargando video...");
            www.SendWebRequest();

            while (!www.isDone)
            {
                Percent.text = $"{Mathf.Floor(www.downloadProgress * 100)}%";
                Bar.fillAmount = www.downloadProgress;
                yield return null;
            }

            Percent.text = "100%";
            Bar.fillAmount = 1;

            if (www.result == UnityWebRequest.Result.Success)
            {
                Debug.Log("Video descargado");

                if (SaveVideo(path, www.downloadHandler.data)) StartCoroutine(PlayVideo(path));
                else
                {
                    Debug.Log("Video no grabado");
                    Next();
                }
            }
            else
            {
                AndroidBridge.Toast("No hay video");
                Next();
            }
        }
    }

    private bool SaveVideo(string path, byte[] bytes)
    {
        try
        {
            File.WriteAllBytes(path, bytes);
            return true;
        }
        catch
        {
            return false;
        }
    }

    private IEnumerator PlayVideo(string path)
    {
        this.Put($"edaMark/status-video/{Session.Eda + 1}/true", "{}");
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        yield return new WaitForSeconds(1f);

#if UNITY_IPHONE
        path = "file://" + path;
#endif

        Bar.gameObject.SetActive(false);
        Percent.gameObject.SetActive(false);
        Handheld.PlayFullScreenMovie(path, Color.black, FullScreenMovieControlMode.Full);

        yield return new WaitForSeconds(0.5f);
        Next();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StopAllCoroutines();
            Next();
        }
    }

    private void Next() => RotateScreen.Load(_nextScene, RotateScreen.ORIENTATION.PORTRAIT);
}
