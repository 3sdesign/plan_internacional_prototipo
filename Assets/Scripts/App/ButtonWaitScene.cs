using UnityEngine;

public class ButtonWaitScene : ButtonToScene
{
    [SerializeField] private GameObject Panel = null;

    protected override void OnClick()
    {
        Panel.SetActive(true);
        base.OnClick();
    }
}
