using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image), typeof(Button))]
public class Eye : MonoBehaviour
{
    [SerializeField] private Sprite Open = null;
    [SerializeField] private Sprite Close = null;
    [SerializeField] private InputField Target = null;

    private void Awake()
    {
        Image image = GetComponent<Image>();
        GetComponent<Button>().onClick.AddListener(() =>
        {
            Target.contentType = Target.contentType == InputField.ContentType.Password ? InputField.ContentType.Standard : InputField.ContentType.Password;
            image.sprite = Target.contentType == InputField.ContentType.Password ? Close : Open;
            Target.ForceLabelUpdate();
        });
    }
}
