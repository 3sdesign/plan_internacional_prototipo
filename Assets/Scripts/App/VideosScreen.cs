using UnityEngine;

public class VideosScreen : MonoBehaviour
{
    [SerializeField] private GameObject VideoCardPrefab = null;
    [SerializeField] private Sprite[] Sprites = null;

    private void Start()
    {
        int length = Sprites.Length;
        for (int i = 0; i < length; i++)
        {
            VideoCard faq = Instantiate(VideoCardPrefab, VideoCardPrefab.transform.parent).GetComponent<VideoCard>();
            faq.Configure(i + 1, Sprites[i]);
        }
    }
}
