using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ShowHide : MonoBehaviour
{
    [SerializeField] private GameObject Target = null;

    private void Awake() => GetComponent<Button>().onClick.AddListener(() => Target.SetActive(!Target.activeSelf));
}
