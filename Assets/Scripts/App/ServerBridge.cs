using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public static class ServerBridge
{
    private static readonly string SERVER_URL = "https://a5c6m8urk6.execute-api.us-east-1.amazonaws.com/prod/api/";

    #region Get
    public static void Get(this MonoBehaviour mono, string operation, Action<string> onSuccess = null, Action<UnityWebRequest> onError = null) => mono.StartCoroutine(Get(operation, (UnityWebRequest www) => onSuccess(www.downloadHandler.text), onError));
    public static void Get(this MonoBehaviour mono, string operation, Action<byte[]> onSuccess = null, Action<UnityWebRequest> onError = null) => mono.StartCoroutine(Get(operation, (UnityWebRequest www) => onSuccess(www.downloadHandler.data), onError));

    private static IEnumerator Get(string operation, Action<UnityWebRequest> onSuccess = null, Action<UnityWebRequest> onError = null)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(SERVER_URL + operation))
        {
            www.SetRequestHeader("Authorization", "Bearer " + Session.Token);

            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success) onSuccess?.Invoke(www);
            else onError?.Invoke(www);
        }
    }
    #endregion

    #region Post
    public static void Post(this MonoBehaviour mono, string operation, WWWForm form, Action<string> onSuccess = null, Action<UnityWebRequest> onError = null) => mono.StartCoroutine(Post(operation, form, onSuccess, onError));

    private static IEnumerator Post(string operation, WWWForm form, Action<string> onSuccess = null, Action<UnityWebRequest> onError = null)
    {
        using (UnityWebRequest www = UnityWebRequest.Post(SERVER_URL + operation, form))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success) onSuccess?.Invoke(www.downloadHandler.text);
            else onError?.Invoke(www);
        }
    }
    #endregion

    #region Put
    public static void Put(this MonoBehaviour mono, string operation, string bodyData, Action<string> onSuccess = null, Action<UnityWebRequest> onError = null) => mono.StartCoroutine(Put(operation, bodyData, onSuccess, onError));

    private static IEnumerator Put(string operation, string bodyData, Action<string> onSuccess = null, Action<UnityWebRequest> onError = null)
    {
        using (UnityWebRequest www = UnityWebRequest.Put(SERVER_URL + operation, bodyData))
        {
            www.SetRequestHeader("Content-Type", "application/json");
            www.SetRequestHeader("Authorization", "Bearer " + Session.Token);

            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success) onSuccess?.Invoke(www.downloadHandler.text);
            else onError?.Invoke(www);
        }
    }
    #endregion
}
