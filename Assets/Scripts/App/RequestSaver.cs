using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class RequestSaver
{
    private const string PUT_DATA = "PUT_DATA";

    [System.Serializable]
    public struct PutData
    {
        public string operation;
        public string bodyData;
        public int eda;
        public int ada;
    }

    public static void Upload(MonoBehaviour mono)
    {
        try
        {
            List<PutData> list = LoadData();
            if (list == null) return;

            int corrects = 0;
            int completed = 0;
            int lenght = list.Count;
            for (int i = 0; i < lenght; i++)
            {
                PutData data = list[i];
                mono.Put(data.operation, data.bodyData, (string result) =>
                {
                    list.Remove(data);

                    corrects++;
                    completed++;
                    if (completed == lenght) SaveData(list, $"PUT Update: Attempts {completed} - Success {corrects}");

                }, (UnityWebRequest www) =>
                {
                    completed++;
                    if (completed == lenght) SaveData(list, $"PUT Update: Attempts {completed} - Success {corrects}");
                });
            }
        }
        catch
        {
            Debug.LogWarning("Data error");
        }
    }

    public static void SavePutData(string operation, string bodyData)
    {
        try
        {
            List<PutData> list = LoadData();
            list ??= new List<PutData>();

            PutData data = new PutData
            {
                operation = operation,
                bodyData = bodyData,
                eda = Session.Eda,
                ada = Session.Ada
            };

            list.Add(data);

            SaveData(list, "Put Data saved");
            ProfileUpdater.instance.Refresh();
        }
        catch
        {
            Debug.LogError("Error saving");
        }
    }

    public static List<PutData> LoadData()
    {
        string json = PlayerPrefs.GetString(PUT_DATA);
        if (string.IsNullOrEmpty(json)) return null;

        PutData[] array = JsonHelper.FromJson<PutData>(json);
        List<PutData> list = new List<PutData>();
        list.AddRange(array);

        return list;
    }

    public static int RequestCount()
    {
        List<PutData> data = LoadData();
        return data == null ? 0 : data.Count;
    }

    private static void SaveData(List<PutData> list, string log)
    {
        PutData[] array = list.ToArray();
        string json = JsonHelper.ToJson(array);
        PlayerPrefs.SetString(PUT_DATA, json);
        PlayerPrefs.Save();

        ProfileUpdater.instance.Refresh();

        Debug.Log(log);
    }
}
