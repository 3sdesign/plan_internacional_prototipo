using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class VideoCard : MonoBehaviour
{
    private const string VIDEOS_URL = "https://cuper-develop-files.s3.amazonaws.com/videos/FEM/";

    public void Configure(int n, Sprite sprite)
    {
        Button button = GetComponent<Button>();

        button.onClick.AddListener(() => VideoScreen.Load($"{VIDEOS_URL}Video {n} Sync. (480p).mp4", "Videos"));
        button.image.sprite = sprite;
        gameObject.SetActive(true);
    }
}
