using System.Collections;
using UnityEngine;

public abstract class RefreshableScreen : MonoBehaviour, IRefreshable
{
    [SerializeField] private GameObject Loading = null;

    protected virtual void Start()
    {
        Refresh();
        StartCoroutine(MultiRefresh());
        ProfileUpdater.instance.Refresh();
    }

    private IEnumerator MultiRefresh()
    {
        WaitForSeconds wait = new WaitForSeconds(5);
        yield return null;

        while (RequestSaver.RequestCount() != 0)
        {
            Loading.SetActive(true);
            RequestSaver.Upload(this);
            yield return wait;
        }
    }

    public virtual void Refresh() => Loading.SetActive(false);
}
