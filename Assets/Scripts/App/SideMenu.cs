using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SideMenu : MonoBehaviour
{
    [SerializeField] private RectTransform Panel = null;
    [SerializeField] private Button Close = null;

    private void Awake()
    {
        Close.onClick.RemoveAllListeners();
        Close.onClick.AddListener(() =>
        {
            Close.enabled = false;
            Panel.DOAnchorPosX(Panel.sizeDelta.x, 0.5f).OnComplete(() => gameObject.SetActive(false));
        });
    }

    private void OnEnable()
    {
        Close.enabled = false;
        Panel.anchoredPosition = new Vector2(Panel.sizeDelta.x, 0);
        Panel.DOAnchorPosX(0, 0.5f).OnComplete(() => Close.enabled = true);
    }


}
