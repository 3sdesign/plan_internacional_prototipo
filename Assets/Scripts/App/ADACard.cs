using System;
using UnityEngine;
using UnityEngine.UI;

public class ADACard : MonoBehaviour
{
    [SerializeField] private Text Tittle = null;
    [SerializeField] private Image Status = null;
    [SerializeField] private Image Thumbnail = null;
    [SerializeField] private GameObject Synchronization = null;

    [Header("Buttons")]
    [SerializeField] private Button Video = null;
    [SerializeField] private Button Game = null;
    [SerializeField] private Button Book = null;

    private int _id = -1;

    public void SetID(int id) => _id = id;

    public void SetStatus(Sprite sprite)
    {
        if (sprite)
        {
            Status.sprite = sprite;
            Status.gameObject.SetActive(true);
        }

        SetSynchronization(true);
    }

    public void SetSynchronization(bool value) => Synchronization.SetActive(!value);

    public void Configure(GradeScriptableObject.Card data, Action onVideo, Action<int> onGame, Action<Button> onBook)
    {
        Tittle.text = data.name;
        Thumbnail.sprite = data.sprite;

        Video.onClick.AddListener(() => onVideo?.Invoke());
        Game.onClick.AddListener(() => onGame?.Invoke(_id));
        Book.onClick.AddListener(() => onBook?.Invoke(Book));

        if (Session.GlobalGrade != 0 || Session.Eda != 0) Video.gameObject.SetActive(false);
    }
}
