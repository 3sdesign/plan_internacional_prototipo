using UnityEngine;

public class AvatarLoader : MonoBehaviour
{
    [SerializeField] private Avatar[] Avatars = null;
    [SerializeField] private GameObject Silhouette = null;

    private void Awake()
    {
        if (Session.Avatar != null) Load(Session.Avatar.ToString());
        else ShowSilhouette();
    }

    public void Load(string json)
    {
        int length = Avatars.Length;
        for (int i = 0; i < length; i++) Avatars[i].Load(json, ShowSilhouette);
    }

    private void ShowSilhouette()
    {
        if (Silhouette) Silhouette.SetActive(true);
    }
}
