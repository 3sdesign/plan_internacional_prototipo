using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Cover : MonoBehaviour
{
    [SerializeField] private Sprite[] Sprites = null;

    private void Start()
    {
        Image image = GetComponent<Image>();

        int grade = Session.GlobalGrade;
        if (grade < Sprites.Length) image.sprite = Sprites[grade];
    }
}
