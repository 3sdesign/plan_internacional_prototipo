using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using static UnityEngine.SceneManagement.SceneManager;

public class ChangeScreen : MonoBehaviour
{
    [System.Serializable]
    private struct Data
    {
        public string email;
        public string password;
        public string old_password;
    }

    [SerializeField] private Button Change = null;
    [SerializeField] private CanvasGroup Group = null;

    [Header("Passwords")]
    [SerializeField] private InputField CurrentPassword = null;
    [SerializeField] private InputField NewPassword = null;
    [SerializeField] private InputField RepeatPassword = null;

    private void Awake()
    {
        Regex hasSymbol = new Regex(@"\W|_");
        Regex hasNumber = new Regex(@"[0-9]+");
        Regex hasLowerChar = new Regex(@"[a-z]+");
        Regex hasUpperChar = new Regex(@"[A-Z]+");
        Regex hasMinimum9Chars = new Regex(@".{9,}");

        Change.onClick.AddListener(() =>
        {
            if (NewPassword.text == RepeatPassword.text)
            {
                if (!hasMinimum9Chars.IsMatch(NewPassword.text))
                {
                    AndroidBridge.Toast("La contrase�a debe tener m�s de 8 caracteres");
                    return;
                }

                if (!hasSymbol.IsMatch(NewPassword.text))
                {
                    AndroidBridge.Toast("La contrase�a debe tener al menos un car�cter especial");
                    return;
                }

                if (!hasUpperChar.IsMatch(NewPassword.text))
                {
                    AndroidBridge.Toast("La contrase�a debe tener al menos una letra may�scula");
                    return;
                }

                if (!hasLowerChar.IsMatch(NewPassword.text))
                {
                    AndroidBridge.Toast("La contrase�a debe tener al menos una letra min�scula");
                    return;
                }

                if (!hasNumber.IsMatch(NewPassword.text))
                {
                    AndroidBridge.Toast("La contrase�a debe tener al menos un d�gito");
                    return;
                }

                Group.interactable = false;

                WWWForm form = new WWWForm();
                form.AddField("email", Session.Email);
                form.AddField("password", NewPassword.text);
                form.AddField("old_password", CurrentPassword.text);

                this.Post("auth/change-password", form, (string json) =>
                {
                    AndroidBridge.Toast("Contrase�a actualizada");
                    LoadScene("Main");

                }, (UnityWebRequest www) =>
                {
                    AndroidBridge.Toast("Contrase�a no actualizada");
                    Group.interactable = true;
                });
            }
            else AndroidBridge.Toast("Las contrase�as no coinciden");
        });
    }
}
