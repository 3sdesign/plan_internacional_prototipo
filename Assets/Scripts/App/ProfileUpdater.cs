using SimpleJSON;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class ProfileUpdater : GenericSingletonClass<ProfileUpdater>
{
    private bool _working = false;

    public void Refresh()
    {
        if (_working) return;
        _working = true;

        this.Get("user/profile", (string json) =>
        {
            try
            {
                JSONNode node = JSON.Parse(json);
                if (node.IsObject)
                {
                    JSONNode data = node["data"];

                    JSONNode avatar = data["avatar"];
                    if (avatar.IsObject) Session.Avatar = avatar;

                    JSONNode course = data["user_classrooms"][0];
                    Session.Advance = course["avg_progress"];
                    Session.Points = course["avg_eda_mark"];

                    JSONNode edas = course["eda_marks"];
                    if (edas.IsArray) Session.EdAs = edas;

                    RefreshAll();
                    Debug.Log("Profile updated");
                }
            }
            catch
            {
                Debug.LogWarning("Profile update error");
            }
            finally
            {
                _working = false;
            }

        }, (UnityWebRequest www) =>
        {
            Debug.Log(www.downloadHandler.text);
            _working = false;
        });
    }

    private void RefreshAll()
    {
        IEnumerable<IRefreshable> refreshables = FindObjectsOfType<MonoBehaviour>().OfType<IRefreshable>();
        foreach (IRefreshable refreshable in refreshables)
        {
            refreshable.Refresh();
        }
    }
}
