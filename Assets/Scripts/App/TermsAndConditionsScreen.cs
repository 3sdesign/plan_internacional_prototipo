using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using static UnityEngine.SceneManagement.SceneManager;

public class TermsAndConditionsScreen : MonoBehaviour
{
    [SerializeField] private Text Label = null;
    [SerializeField] private Button Back = null;
    [SerializeField] private Button Accept = null;

    [SerializeField, TextArea] private string TempText = string.Empty;

    [System.Serializable]
    private struct Data
    {
        public bool tyc;
    }

    private void Start() => Load();

    private void Load()
    {
        Label.text = string.Empty;

        Back.onClick.AddListener(() => LoadScene(Session.Logged && Session.TyC ? "Main" : "Authentication"));

        if (Session.Logged && !Session.TyC)
        {
            Accept.gameObject.SetActive(true);
            Accept.onClick.AddListener(() =>
            {
                Session.TyC = true;
                Back.interactable = false;
                Accept.interactable = false;

                string data = JsonConvert.SerializeObject(new Data { tyc = true }, Formatting.None);
                this.Put("user/tyc", data, (string json) =>
                {
                    Debug.Log("TyC: Updated");
                    LoadScene("Main");

                }, (UnityWebRequest www) =>
                {
                    Debug.Log("TyC: Not Updated");
                    LoadScene("Main");
                });
            });
        }
        else Accept.gameObject.SetActive(false);

        Label.text = TempText;

        /*this.Get("tyc/1", (string json) =>
        {      
            JSONNode node = JSON.Parse(json);
            if (node.IsObject)
            {
                JSONNode data = node["data"];
                if (data.IsObject) Label.text = data["description"].ToString().Replace("<p>", "").Replace("</p>", "").Replace("<br>", "\n\n");
            }

        }, (UnityWebRequest www) => Load());*/
    }

    private void OnDestroy() => StopAllCoroutines();
}
