using UnityEngine;

[CreateAssetMenu(fileName = "Tittles", menuName = "ScriptableObjects/GradeScriptableObject", order = 1)]
public class GradeScriptableObject : ScriptableObject
{
    public enum GAME
    {
        Jetpack,
        Memory,
        Crane,
        Chocolate,
        Ludo
    }

    [System.Serializable]
    public class Card
    {
        public string name = string.Empty;
        public Sprite sprite = null;
    }

    [System.Serializable]
    public class ADA : Card
    {
        public GAME game;
    }

    [System.Serializable]
    public class EDA : Card
    {
        public ADA[] adas;
    }

    public EDA[] edas = null;
}