using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Dropdown))]
public class YearSelector : MonoBehaviour
{
    private void Awake() => GetComponent<Dropdown>().onValueChanged.AddListener((int n) => Session.GlobalGrade = n);
}
