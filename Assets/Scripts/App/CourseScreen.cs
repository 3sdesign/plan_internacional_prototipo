using System.Collections.Generic;
using UnityEngine;
using static RequestSaver;
using static UnityEngine.SceneManagement.SceneManager;

public class CourseScreen : RefreshableScreen
{
    [SerializeField] private EDACard[] Cards = null;
    [SerializeField] private TittlesScriptableObject Data = null;

    protected override void Start()
    {
        base.Start();

        int length = Cards.Length;
        for (int i = 0; i < length; i++)
        {
            int temp = i;

            Cards[i].Configure(Data.grades[Session.GlobalGrade].edas[i], () =>
            {
                for (int j = 0; j < length; j++)
                {
                    Cards[j].Interactable(false);
                }

                Session.Eda = temp;
                LoadScene("EDA");
            });
        }
    }

    public override void Refresh()
    {
        base.Refresh();

        if (Session.EdAs != null && Session.EdAs.IsArray)
        {
            List<PutData> allData = LoadData();

            int length = Cards.Length;
            for (int i = 0; i < length; i++)
            {
                bool match = false;
                if (allData != null)
                {
                    for (int k = 0; k < allData.Count; k++)
                    {
                        PutData data = allData[k];
                        if (data.eda == i) match = true;
                    }
                }

                if (match) Cards[i].SetSynchronization(false);
                else
                {
                    float percent = Session.EdAs[i]["avg_progress"] / 100.0f;
                    Cards[i].SetAdvance(percent);
                }
            }
        }
    }
}
