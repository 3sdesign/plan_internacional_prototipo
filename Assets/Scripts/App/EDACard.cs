using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class EDACard : MonoBehaviour
{
    private Button _button = null;

    [SerializeField] private Text Tittle = null;
    [SerializeField] private Image Thumbnail = null;
    [SerializeField] private Text PercentText = null;
    [SerializeField] private Image PercentGraphic = null;
    [SerializeField] private GameObject Synchronization = null;

    private void Awake() => _button = GetComponent<Button>();

    public void Interactable(bool value) => _button.interactable = value;

    public void SetAdvance(float percent)
    {
        PercentGraphic.fillAmount = percent;
        PercentText.text = Mathf.Floor(percent * 100).ToString() + "%";

        SetSynchronization(true);
    }

    public void SetSynchronization(bool value)
    {
        PercentGraphic.gameObject.SetActive(value);
        PercentText.gameObject.SetActive(value);
        Synchronization.SetActive(!value);
    }

    public void Configure(GradeScriptableObject.Card data, Action onClick)
    {
        Tittle.text = data.name;
        Thumbnail.sprite = data.sprite;
        _button.onClick.AddListener(() => onClick?.Invoke());
    }
}
