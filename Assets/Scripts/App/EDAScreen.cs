using Paroxe.PdfRenderer;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using static RequestSaver;
using static RotateScreen;

public class EDAScreen : RefreshableScreen
{
    [Header("Video")]
    [SerializeField] private Button Video = null;

    [Header("Status")]
    [SerializeField] private Sprite Gold = null;
    [SerializeField] private Sprite Silver = null;

    [Header("PDF")]
    [SerializeField] private PDFViewer Viewer = null;
    [SerializeField] private Button CloseViewer = null;

    [Header("Cards")]
    [SerializeField] private ADACard[] Cards = null;
    [SerializeField] private TittlesScriptableObject Data = null;

    private void Awake() => CloseViewer.onClick.AddListener(() => Viewer.gameObject.SetActive(false));

    protected override void Start()
    {
        base.Start();

        Video.onClick.AddListener(() =>
        {
            Video.interactable = false;

            if (Session.GlobalGrade == 0 && Session.Eda == 0 && Session.Ada == 0)
            {
                PlayVideo("https://cuper-develop-files.s3.us-east-2.amazonaws.com/videos/adicional/EDA+1-intro.mp4");
                return;
            }

            this.Get($"eda?grade={Session.Grade}&level={Session.Level}", (string json) =>
            {
                JSONNode node = JSON.Parse(json);
                if (node.IsObject)
                {
                    JSONNode data = node["data"];
                    if (data.IsArray)
                    {
                        string url = data[Session.Eda]["video_url"];
                        if (!string.IsNullOrEmpty(url))
                        {
                            PlayVideo(url);
                            return;
                        }
                    }
                }

                AndroidBridge.Toast("No hay video");
                Video.interactable = true;

            }, (UnityWebRequest www) =>
            {
                AndroidBridge.Toast("No hay video");
                Video.interactable = true;
            });
        });

        int length = Cards.Length;
        for (int i = 0; i < length; i++)
        {
            if (!Session.IsPrimary || i < 3)
            {
                int temp = i;
                GradeScriptableObject.ADA data = Data.grades[Session.GlobalGrade].edas[Session.Eda].adas[i];

                Cards[i].Configure(data, () =>
                {
                    PlayVideo($"https://cuper-develop-files.s3.us-east-2.amazonaws.com/videos/adicional/video_1_1_{temp + 1}.mp4");

                }, (int id) =>
                {
                    if (id != -1)
                    {
                        Session.Ada = temp;
                        Session.CurrentID = id;
                        Load(data.game.ToString(), ORIENTATION.LANDSCAPE);
                    }

                }, (Button button) =>
                {
                    button.interactable = false;
                    string file = $"{Session.GlobalGrade + 1}_{Session.Eda + 1}_{temp + 1}";

                    print($"{Application.persistentDataPath}/{file}.pdf");

                    if (Exists(file, "pdf"))
                    {
                        button.interactable = true;
                        ShowPDF(file);
                    }
                    else
                    {
                        AndroidBridge.Toast("Descargando PDF...");
                        this.Get($"file/getByEdaAda/{Session.Eda + 1}/{temp + 1}", (string json) =>
                        {
                            JSONNode node = JSON.Parse(json);
                            if (node.IsObject)
                            {
                                string url = node["data"]["url"].Value;
                                print(url);

                                StartCoroutine(DownloadBytes(url, (byte[] bytes) =>
                                {
                                    string path = Application.persistentDataPath + "/" + file + ".pdf";
                                    File.WriteAllBytes(path, bytes);
                                    button.interactable = true;
                                    ShowPDF(file);

                                }, (UnityWebRequest www) =>
                                {
                                    print(www.error);
                                    button.interactable = true;
                                    AndroidBridge.Toast("No hay PDF");
                                }));
                            }

                        }, (UnityWebRequest www) =>
                        {
                            Debug.Log(www.downloadHandler.text);
                            AndroidBridge.Toast("No hay PDF");
                            button.interactable = true;
                        });
                    }
                });
            }
            else Cards[i].gameObject.SetActive(false);
        }
    }

    private bool Exists(string file, string extension) => File.Exists($"{Application.persistentDataPath}/{file}.{extension}");

    private IEnumerator DownloadBytes(string url, Action<byte[]> onDownload, Action<UnityWebRequest> onError = null)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.Success) onDownload(www.downloadHandler.data);
            else onError?.Invoke(www);
        }
    }

    private void PlayVideo(string url) => VideoScreen.Load(url, "EDA");

    private void ShowPDF(string file)
    {
        Viewer.FileName = file + ".pdf";
        Viewer.gameObject.SetActive(true);
    }

    public override void Refresh()
    {
        base.Refresh();

        if (Session.EdAs != null && Session.EdAs.IsArray)
        {
            if (Session.Eda < Session.EdAs.Count)
            {
                JSONNode adas = Session.EdAs[Session.Eda]["ada_marks"];
                if (adas != null && adas.IsArray)
                {
                    List<PutData> allData = LoadData();

                    int length = Cards.Length;
                    for (int i = 0; i < length; i++)
                    {
                        if (!Cards[i].gameObject.activeSelf) continue;

                        int id = i + 1;
                        for (int j = 0; j < adas.Count; j++)
                        {
                            if (adas[j]["ada_id"] == id)
                            {
                                bool match = false;
                                if (allData != null)
                                {
                                    for (int k = 0; k < allData.Count; k++)
                                    {
                                        PutData data = allData[k];
                                        if (data.eda == Session.Eda && data.ada == i) match = true;
                                    }
                                }

                                if (match) Cards[i].SetSynchronization(false);
                                else
                                {
                                    float mark = adas[j]["mark"];
                                    Cards[i].SetStatus(GetStatus(mark));
                                }

                                if (adas[j]["id"].IsNumber)
                                {
                                    Cards[i].SetID(adas[j]["id"]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private Sprite GetStatus(float mark)
    {
        if (mark < 80)
        {
            if (mark < 50) return null;
            else return Silver;
        }
        else return Gold;
    }
}
