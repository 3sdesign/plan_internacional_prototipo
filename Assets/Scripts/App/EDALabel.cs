using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class EDALabel : MonoBehaviour
{
    private void Awake()
    {
        GetComponent<Text>().text = $"EdA {Session.Eda + 1}";
    }
}
