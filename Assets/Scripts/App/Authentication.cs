using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using static UnityEngine.SceneManagement.SceneManager;

public class Authentication : MonoBehaviour
{
    [Header("Login")]
    [SerializeField] private InputField Usercode = null;
    [SerializeField] private InputField Password = null;
    [SerializeField] private Button Login = null;
    [SerializeField] private bool Force = false;

    [Header("Recover")]
    [SerializeField] private InputField DNI = null;
    [SerializeField] private Button Recover = null;

    private void Awake()
    {
        Session.Logged = false;

        Login.onClick.AddListener(() =>
        {
            Login.interactable = false;

            if (Force)
            {
                LoadScene("Main");
                return;
            }

            WWWForm form = new WWWForm();
            form.AddField("code", Usercode.text);
            form.AddField("password", Password.text);

            this.Post("auth/signin/student", form, (string json) =>
            {
                JSONNode node = JSON.Parse(json);
                if (node.IsObject)
                {
                    JSONNode data = node["data"];

                    JSONNode token = data["token"];
                    JSONNode grade = data["grade_id"];
                    JSONNode level = data["level_id"];
                    if (token.IsString && grade.IsNumber && level.IsNumber)
                    {
                        Session.Level = level;
                        Session.Grade = grade;
                        Session.Token = token.Value;

                        int levelGap = (level - 1) * 6;
                        Session.GlobalGrade = grade - 1 + levelGap;

                        this.Get("user/profile", (string json) =>
                        {
                            try
                            {
                                JSONNode node = JSON.Parse(json);
                                if (node.IsObject)
                                {
                                    JSONNode data = node["data"];
                                    if (data.IsObject)
                                    {
                                        Session.Name = data["name"].Value;
                                        Session.Lastname = $"{data["lastname"].Value} {data["second_lastname"].Value}";

                                        Session.Email = data["email"].Value;
                                        Session.TyC = data["tyc"].AsBool;

                                        JSONNode avatar = data["avatar"];
                                        if (avatar.IsObject) Session.Avatar = avatar;

                                        JSONNode school = data["school"];
                                        if (school.IsObject) Session.School = school["name"].Value;

                                        JSONNode course = data["user_classrooms"][0];
                                        if (course.IsObject)
                                        {
                                            JSONNode edas = course["eda_marks"];
                                            if (edas.IsArray) Session.EdAs = edas;

                                            Session.Points = course["avg_eda_mark"];
                                            Session.Advance = course["avg_progress"];
                                            Session.Section = course["classroom"]["section"]["name"].Value;
                                        }

                                        JSONNode teacher = data["teacher"];
                                        if (teacher.IsObject) Session.Teacher = teacher["name"].Value;

                                        Session.Logged = true;
                                        LoadScene(Session.TyC ? "Main" : "TermsAndConditions");
                                        return;
                                    }
                                }

                                Login.interactable = true;
                                AndroidBridge.Toast("Datos incompletos");

                            }
                            catch
                            {
                                Login.interactable = true;
                                AndroidBridge.Toast("Error al leer datos");
                            }

                        }, (UnityWebRequest www) =>
                        {
                            Login.interactable = true;
                            AndroidBridge.Toast("Error al obtener datos");
                        });
                        return;
                    }
                }

                Login.interactable = true;

            }, (UnityWebRequest www) =>
            {
                Login.interactable = true;
                print(www.downloadHandler.text);
                AndroidBridge.Toast("Error de credenciales");
            });
        });

        Recover.onClick.AddListener(() =>
        {
            Recover.interactable = false;

            WWWForm form = new WWWForm();
            form.AddField("code", DNI.text);

            this.Post("auth/recover/student", form, (string json) =>
            {
                Recover.interactable = true;
                AndroidBridge.Toast("Revisa tu correo");

            }, (UnityWebRequest www) =>
            {
                Recover.interactable = true;
                print(www.downloadHandler.text);
                AndroidBridge.Toast("Revisa tu correo");
            });
        });
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
