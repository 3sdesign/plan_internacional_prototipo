using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static RequestSaver;

public class MainScreen : RefreshableScreen
{
    [Header("Name")]
    [SerializeField] private LayoutGroup Group;
    [SerializeField] private Text NameLabel = null;
    [SerializeField] private Text FullNameLabel = null;

    [Header("Advance")]
    [SerializeField] private Image PercentBar = null;
    [SerializeField] private Text PercentLabel = null;
    [SerializeField] private GameObject Synchronization = null;

    [Header("Avatar")]
    [SerializeField] private AvatarLoader[] Avatars = null;

    protected override void Start()
    {
        base.Start();

        NameLabel.text = Session.Name;
        Canvas.ForceUpdateCanvases();
        Group.gameObject.SetActive(false);
        Group.gameObject.SetActive(true);
        FullNameLabel.text = $"{Session.Name} {Session.Lastname}";
    }

    public override void Refresh()
    {
        base.Refresh();

        List<PutData> allData = LoadData();

        bool match = false;
        if (allData != null)
        {
            if (allData.Count > 0) match = true;
        }

        PercentLabel.text = $"{Mathf.Floor(Session.Advance)}%";
        PercentBar.fillAmount = Session.Advance / 100f;
        PercentLabel.gameObject.SetActive(!false);
        Synchronization.SetActive(match);

        if (Session.Avatar != null && Session.Avatar.IsObject)
        {
            int length = Avatars.Length;
            string temp = Session.Avatar.ToString();

            for (int i = 0; i < length; i++)
            {
                Avatars[i].Load(temp);
            }
        }
    }
}
