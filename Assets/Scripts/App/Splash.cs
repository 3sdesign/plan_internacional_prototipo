using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class Splash : MonoBehaviour
{
    private static bool _firstTime = true;

    private void Start()
    {
        if (_firstTime)
        {
            _firstTime = false;
            GetComponent<CanvasGroup>().DOFade(0, 1.5f).SetDelay(3).OnComplete(Deactivate);
        }
        else Deactivate();
    }

    private void Deactivate() => gameObject.SetActive(false);
}
