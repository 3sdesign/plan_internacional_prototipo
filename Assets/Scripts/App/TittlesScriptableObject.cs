using UnityEngine;

[CreateAssetMenu(fileName = "Tittles", menuName = "ScriptableObjects/TittlesScriptableObject", order = 1)]
public class TittlesScriptableObject : ScriptableObject
{
    public GradeScriptableObject[] grades = null;
}