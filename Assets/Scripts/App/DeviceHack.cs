using UnityEngine;
using UnityEngine.UI;

public class DeviceHack : MonoBehaviour
{
    [SerializeField] private bool Force = false;
    [SerializeField] private CanvasScaler Scaler = null;

    private void Start()
    {
        if (Force || SystemInfo.deviceModel.StartsWith("iPad"))
        {
            Scaler.matchWidthOrHeight = 1;
            Scaler.referenceResolution = new Vector2(1200, 2100);
        }
    }
}
