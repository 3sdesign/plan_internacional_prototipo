using UnityEngine;
using UnityEngine.UI;

public class FAQuestion : MonoBehaviour
{
    private RectTransform _rectTransform = null;
    [SerializeField] private Text Answer = null;
    [SerializeField] private Text Question = null;
    [SerializeField] private Button ShowHide = null;

    private void Awake()
    {
        _rectTransform = transform as RectTransform;

        ShowHide.onClick.AddListener(() =>
        {
            Answer.gameObject.SetActive(!Answer.gameObject.activeSelf);

            Canvas.ForceUpdateCanvases();

            float gap = Answer.gameObject.activeSelf ? Answer.rectTransform.sizeDelta.y + 75 : 0;
            _rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x, 125 + gap);
            Canvas.ForceUpdateCanvases();
        });
    }

    public void Configure(string question, string answer)
    {
        Answer.text = answer;
        Question.text = question;
        gameObject.SetActive(true);
    }
}
