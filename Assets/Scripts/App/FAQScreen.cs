using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;

public class FAQScreen : MonoBehaviour
{
    [SerializeField] private GameObject FAQPrefab = null;

    private void Start() => Load();

    private void Load()
    {
        this.Get("faq", (string json) =>
        {
            JSONNode node = JSON.Parse(json);
            if (node.IsObject)
            {
                JSONNode data = node["data"];
                if (data.IsArray)
                {
                    int length = data.Count;
                    for (int i = 0; i < length; i++)
                    {
                        FAQuestion faq = Instantiate(FAQPrefab, FAQPrefab.transform.parent).GetComponent<FAQuestion>();
                        faq.Configure(data[i]["question"], data[i]["description"]);
                    }
                }
            }

        }, (UnityWebRequest www) => Load());
    }

    private void OnDestroy() => StopAllCoroutines();
}
