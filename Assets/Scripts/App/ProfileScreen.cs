using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static RequestSaver;

public class ProfileScreen : RefreshableScreen
{
    [SerializeField] private Text Name = null;
    [SerializeField] private Text School = null;
    [SerializeField] private Text Teacher = null;
    [SerializeField] private Text Grade = null;
    [SerializeField] private Text Section = null;
    [SerializeField] private Text Points = null;
    [SerializeField] private Text Advance = null;
    [SerializeField] private Text Anexo = null;

    [Header("Synchronization")]
    [SerializeField] private GameObject[] Synchronizations = null;

    protected override void Start()
    {
        base.Start();

        Name.text = $"{Session.Name} {Session.Lastname}";
        School.text = Session.School;
        Teacher.text = Session.Teacher;
        Grade.text = $"{Session.Grade}�";
        Section.text = Session.Section;
        Anexo.text = "--";
    }

    public override void Refresh()
    {
        base.Refresh();

        List<PutData> allData = LoadData();

        bool match = false;
        if (allData != null)
        {
            if (allData.Count > 0) match = true;
        }

        Points.text = Session.Points.ToString();
        Advance.text = $"{Mathf.Floor(Session.Advance)}%";

        Points.gameObject.SetActive(!match);
        Advance.gameObject.SetActive(!match);
        for (int i = 0; i < Synchronizations.Length; i++)
        {
            Synchronizations[i].SetActive(match);
        }
    }
}
