using DG.Tweening;
using UnityEngine;

public class Floating : MonoBehaviour
{
    private void Start()
    {
        (transform as RectTransform).DOAnchorPosY(-50, 2).SetLoops(-1, LoopType.Yoyo);
    }
}
