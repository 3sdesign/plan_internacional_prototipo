using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.SceneManagement.SceneManager;

[RequireComponent(typeof(Button))]
public class ButtonToScene : MonoBehaviour
{
    [SerializeField] protected string SceneName = string.Empty;

    private void Awake() => GetComponent<Button>().onClick.AddListener(OnClick);

    protected virtual void OnClick() => LoadScene(SceneName);
}
