using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonSound : MonoBehaviour
{
    [SerializeField] private string sound = string.Empty;

    private void Awake() => GetComponent<Button>().onClick.AddListener(() => AudioManager.instance.PlaySFX(sound));
}
