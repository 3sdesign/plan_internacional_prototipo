using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HttpConnection;
using Newtonsoft.Json;

public class Login : MonoBehaviour
{
    [SerializeField] private Dropdown yearOptions;
    [SerializeField] private Button btnAccess;

    [Header("Login test")]
    [SerializeField] private bool isOnTest = true;
    [SerializeField] private string URL;
    [SerializeField] private InputField inputDNI;
    [SerializeField] private InputField inputPassword;
    [SerializeField] private Button btnLogin;

    void Start()
    {
        yearOptions.onValueChanged.AddListener(index => Session.GlobalGrade = index + 1); //Session.Year
        btnAccess.onClick.AddListener(() => UnityEngine.SceneManagement.SceneManager.LoadScene("Avatar"));
        //
        btnLogin.onClick.AddListener(LoginAccount);
    }

    // Test

    public void LoginAccount()
    {
        if (string.IsNullOrEmpty(inputDNI.text))
        {
            Debug.Log("Ingrese su DNI");
            return;
        }

        if (string.IsNullOrEmpty(inputPassword.text))
        {
            Debug.Log("Ingrese su contrase�a");
            return;
        }

        if (isOnTest)
        {
            OnLoginSuccess("");
            return;
        }
        string json = JsonConvert.SerializeObject(new { dni = inputDNI.text, password = inputPassword.text });

        this.HTTP_REQUEST("Login account", URL, METHOD.POST, json, OnLoginSuccess, OnLoginError);
    }

    void OnLoginSuccess(string response)
    {
        Debug.Log("Inicio de sesi�n exitoso");
    }

    void OnLoginError(HttpErrorResponse error)
    {
        if (error.code == CODE.SERVER_UNAVAILABLE_CODE || error.code == CODE.BAD_GATEWAY_CODE)
        {
            Debug.Log("El servidor no est� disponible");
            return;
        }
        Debug.Log(error.text);
    }
}
