using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Games : MonoBehaviour
{
    [SerializeField] private Button[] Buttons = null;
    [SerializeField] private GameObject[] Points = null;

    private void Awake()
    {
        if (Session.GlobalGrade < 7)
        {
            Buttons[3].gameObject.SetActive(false);
            Buttons[4].gameObject.SetActive(false);
            Points[3].SetActive(false);
            Points[4].SetActive(false);
        }

        for (int i = 0; i < Buttons.Length; i++)
        {
            int temp = i;
            Buttons[i].onClick.AddListener(() =>
            {
                Session.Ada = temp + 1;
                SceneManager.LoadScene(Buttons[temp].name);
            });
        }
    }
}
