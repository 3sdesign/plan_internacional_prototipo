using UnityEngine;
using static UnityEngine.SceneManagement.SceneManager;

public class Grade : MonoBehaviour
{
    public void CourseButton_Click(int Grade)
    {
        Session.GlobalGrade = Grade;
        LoadScene("Course");
    }
}
