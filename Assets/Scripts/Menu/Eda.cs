using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.SceneManagement.SceneManager;

public class Eda : MonoBehaviour
{
    [SerializeField] private Text txtTitle;

    private void Start() => txtTitle.text = $"Unidad {Session.Ada}";

    public void GamesButton_Click() => LoadScene("Games");
}
