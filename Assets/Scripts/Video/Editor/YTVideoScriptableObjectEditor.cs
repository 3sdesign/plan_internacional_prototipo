using UnityEditor;

[CustomEditor(typeof(YTVideoScriptableObject))]
public class YTVideoScriptableObjectEditor : Editor
{
    YTVideoScriptableObject source;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        source = (YTVideoScriptableObject)target;

        Year[] years = source.years;

        if (years.Length == 0)
            return;

        for (int i = 0; i < years.Length; i++)
        {
            Year year = years[i];
            year.yearName = "Year " + (i + 1).ToString();

            UnitYT[] courseA = year.courseA;
            UnitYT[] courseB = year.courseB;

            if (courseA.Length > 0) for (int j = 0; j < courseA.Length; j++) courseA[j].unitName = "U " + (j + 1).ToString();
            if (courseB.Length > 0) for (int j = 0; j < courseB.Length; j++) courseB[j].unitName = "U " + (j + 1).ToString();
        }
    }
}
