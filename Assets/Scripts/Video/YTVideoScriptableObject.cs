using UnityEngine;
using System;

[Serializable]
public class Year
{
    [HideInInspector] public string yearName;

    public UnitYT[] courseA = new UnitYT[10];
    public UnitYT[] courseB = new UnitYT[10];
}

[Serializable]
public class UnitYT
{
    [HideInInspector] public string unitName;
    public string URL;
}

[CreateAssetMenu(fileName = "YTVideo", menuName = "ScriptableObjects/YTVideo", order = 2)]
public class YTVideoScriptableObject : ScriptableObject
{
    public Year[] years = new Year[11];

    public string GetURL(int year, int course, int unit)
    {
        if (course != 1 && course != 2)
            return string.Empty;

        for (int i = 0; i < years.Length; i++)
        {
            if (i + 1 != year) continue;

            UnitYT[] units = course == 1 ? years[i].courseA : years[i].courseB;

            for (int k = 0; k < units.Length; k++)
            {
                if (k + 1 != unit) continue;

                return units[k].URL;
            }
        }

        return string.Empty;
    }
}