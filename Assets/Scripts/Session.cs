using SimpleJSON;
using UnityEngine;

public static class Session
{
    // Profile
    public static string Token = string.Empty;
    public static string Name = string.Empty;
    public static string Lastname = string.Empty;
    public static string School = string.Empty;
    public static string Teacher = string.Empty;
    public static string Section = string.Empty;
    public static int Points = 0;
    public static float Advance = 0;

    public static bool Logged = false;
    public static bool TyC = false;

    public static JSONNode EdAs = null;
    public static JSONNode Avatar = null;
    public static string Email = string.Empty;

    public static int CurrentID = -1;

    // Navigation
    public static int GlobalGrade = 0;
    public static int Grade = 0;
    public static int Level = 0;
    public static int Eda = 0;
    public static int Ada = 0;

    // Game
    public static int Friend = 0;

    public static QuizScriptableObject GetQuizData()
    {
        string path = $"QuizData/{GlobalGrade + 1}/Eda_{Eda + 1}/Ada_{Ada + 1}/data";
        Debug.Log($"Loading {path}");

        var data = Resources.Load<QuizScriptableObject>(path);
        if (!data)
        {
            Debug.LogWarning("Quiz data not found, loading from test folder...");
            data = Resources.Load<QuizScriptableObject>($"QuizData/data");
        }

        return data;
    }

    public static int GetLevel()
    {
        switch (GlobalGrade)
        {
            case 2:
            case 3:
                return 1;
            case 4:
            case 5:
                return 2;
            case 6:
            case 7:
                return 3;
            case 8:
            case 9:
            case 10:
                return 4;
            default: return 0;
        }
    }

    public static bool IsPrimary => Level == 1;

    public static int KidType() => Mathf.Min(GetLevel(), 3);
}
