using UnityEngine;

public static class RotationManager
{

    public static void ForcePortrait()
    {
        SetupAutoRotate(false);
        Screen.orientation = ScreenOrientation.Portrait;
        Screen.orientation = ScreenOrientation.AutoRotation;
    }

    public static void ForceLandscape()
    {
        SetupAutoRotate(true);
        Screen.orientation = ScreenOrientation.Landscape;
        Screen.orientation = ScreenOrientation.AutoRotation;
    }

    private static void SetupAutoRotate(bool landscape)
    {
        Screen.autorotateToPortrait = !landscape;
        Screen.autorotateToLandscapeLeft = landscape;
        Screen.autorotateToLandscapeRight = landscape;
        Screen.autorotateToPortraitUpsideDown = !landscape;
    }
}
