using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/AvatarScriptableObject", order = 1)]
public class AvatarScriptableObject : ScriptableObject
{
    [System.Serializable]
    public class SpriteArray
    {
        public Sprite[] sprites = null;
    }

    [System.Serializable]
    public class BodySprites : SpriteArray
    {
        public enum BODY_COLOR
        {
            RED,
            GREEN,
            BLUE
        }

        public BODY_COLOR color = default;
    }

    [System.Serializable]
    public class HairSprites
    {
        [System.Serializable]
        public class HairColor : SpriteArray
        {
            public enum HAIR_COLOR
            {
                BLONDE,
                BROWN,
                BLACK
            }

            public HAIR_COLOR color = default;
        }

        public HairColor[] hairColors;
    }

    [System.Serializable]
    public class HeadSprites : SpriteArray
    {
        public enum SKIN_COLOR
        {
            NORMAL,
            COLOR,
            ARYAN
        }

        public SKIN_COLOR color = SKIN_COLOR.NORMAL;
    }

    public BodySprites[] body;

    [SerializeField] private HairSprites[] hair;
    public HairSprites GetHairs(int value) => hair[Mathf.Clamp(value, 0, hair.Length - 1)];

    public HeadSprites[] head;
    public Sprite[] shadows;

    public Sprite[] mouth;
    public Sprite[] eyes;
    public Sprite[] nose;
    public Sprite[] eyebrown;
}