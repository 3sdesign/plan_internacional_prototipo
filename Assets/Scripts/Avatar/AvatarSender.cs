using UnityEngine;
using UnityEngine.UI;
using static Avatar;

[RequireComponent(typeof(Button))]
public class AvatarSender : MonoBehaviour
{
    [SerializeField] private AVATAR_PART Part = default;
    private AvatarCreator _creator = null;
    private int _color = 0;

    [SerializeField] private bool Shadow = false;

    private void Awake()
    {
        _creator = FindObjectOfType<AvatarCreator>();
        GetComponent<Button>().onClick.AddListener(() =>
        {
            AudioManager.instance.PlaySFX("Answer");
            _creator.Set(Part, transform.GetSiblingIndex(), _color);
        });
    }

    public void ChangeSprite(AVATAR_PART part, int color)
    {
        if (part == Part)
        {
            _color = color;
            Sprite[] sprites = _creator.GetSprites(Part, _color, Shadow);
            if (sprites != null) transform.GetChild(0).GetComponent<Image>().sprite = sprites[transform.GetSiblingIndex()];
        }
    }
}
