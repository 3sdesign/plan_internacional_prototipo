using DG.Tweening;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using static Avatar;

public class AvatarCreator : MonoBehaviour
{
    [SerializeField] private Text Title = null;

    [System.Serializable]
    private class AvatarScreen
    {
        public GameObject root;

        [Header("Styles")]
        public GameObject girl;
        public GameObject boy;

        public Transform transform => root.transform;
        public void SetActive(bool value) => root.SetActive(value);

        public void CheckStyles(int style)
        {
            if (girl) girl.SetActive(style == 0);
            if (boy) boy.SetActive(style == 1);
        }
    }

    [Header("Flow")]
    [SerializeField] private Transform Container = null;
    [SerializeField] private AvatarScreen[] Screens = null;
    private int _currentScreen = 0;

    [System.Serializable]
    public struct AvatarAndButton
    {
        public Avatar avatar;
        public Button button;
    }

    [Header("Characters")]
    private int _currentAvatar = 0;
    [SerializeField] private AvatarAndButton[] Avatars = null;

    [Header("Navigation")]
    [SerializeField] private Button PrevButton = null;
    [SerializeField] private Button NextButton = null;

    private void Awake()
    {
        for (int i = 0; i < Avatars.Length; i++)
        {
            int temp = i;
            Avatars[temp].button.onClick.AddListener(() =>
            {
                _currentAvatar = temp;
                AudioManager.instance.PlaySFX("Star");

                AvatarSender[] senders = FindObjectsOfType<AvatarSender>();
                for (int i = 0; i < senders.Length; i++)
                {
                    senders[i].ChangeSprite(AVATAR_PART.Head, 0);
                }
            });
        }

        PrevButton.onClick.AddListener(Prev);
        NextButton.onClick.AddListener(Next);
    }

    private void Start() => AudioManager.instance.PlayBgm("Avatar", 0.75f);

    public void Prev()
    {
        if (_currentScreen > 0)
        {
            Screens[_currentScreen].SetActive(false);

            _currentScreen--;
            if (_currentScreen == 0)
            {
                Avatars[_currentAvatar].button.enabled = true;
                Avatars[_currentAvatar].button.transform.SetParent(Screens[0].transform);
                Avatars[_currentAvatar].button.transform.SetSiblingIndex(_currentAvatar);
            }

            ShowCurrentScreen(false);
        }
    }

    public void Next()
    {
        if (_currentScreen < Screens.Length - 1)
        {
            Screens[_currentScreen].SetActive(false);

            if (_currentScreen == 0)
            {
                SetNav(false);

                Avatars[_currentAvatar].button.enabled = false;
                Avatars[_currentAvatar].button.transform.SetParent(Container);

                Screens[_currentScreen].SetActive(false);

                Vector3 pos = Avatars[_currentAvatar].button.transform.position;
                Avatars[_currentAvatar].button.image.rectTransform.anchorMin = Avatars[_currentAvatar].button.image.rectTransform.anchorMax = Vector2.one * 0.5f;
                Avatars[_currentAvatar].button.transform.position = pos;

                Avatars[_currentAvatar].button.image.rectTransform.DOAnchorPosX(-470.3065f, 0.5f * _currentAvatar).SetEase(Ease.InBack).OnComplete(() =>
                {
                    SetNav(true);
                    _currentScreen++;
                    ShowCurrentScreen(true);
                });
            }
            else
            {
                _currentScreen++;
                ShowCurrentScreen(true);
            }
        }
    }

    private void ShowCurrentScreen(bool next)
    {
        RefreshTitle();
        Screens[_currentScreen].SetActive(true);
        Screens[_currentScreen].CheckStyles(_currentAvatar);
        PrevButton.gameObject.SetActive(_currentScreen != 0);
        NextButton.gameObject.SetActive(_currentScreen < Screens.Length - 1);

        bool interaction = true;
        if (next)
        {
            if (2 < _currentScreen && _currentScreen < 7)
            {
                int part = 0;
                switch (_currentScreen)
                {
                    case 3:
                        part = 3;
                        break;

                    case 4:
                        part = 6;
                        break;

                    case 5:
                        part = 4;
                        break;

                    case 6:
                        part = 2;
                        break;
                }

                interaction = Avatars[_currentAvatar].avatar.Selected((AVATAR_PART)part);
            }
        }
        NextButton.interactable = interaction;
    }

    private void SetNav(bool state)
    {
        PrevButton.enabled = state;
        NextButton.enabled = state;
    }

    private void RefreshTitle()
    {
        string text = "Escoge el tipo de ";

        switch (_currentScreen)
        {
            case 0:
                text = "Escoge el g�nero de tu avatar";
                break;

            case 1:
                text += "ropa";
                break;

            case 2:
                text += "cabello";
                break;

            case 3:
                text += "ojos";
                break;

            case 4:
                text += "cejas";
                break;

            case 5:
                text += "nariz";
                break;

            case 6:
                text += "boca";
                break;

            case 7:
                text = "Confirmar avatar";
                break;
        }

        Title.text = text;
    }

    public void Set(AVATAR_PART part, int style, int color)
    {
        NextButton.interactable = true;

        if (part == AVATAR_PART.Head)
        {
            for (int i = 0; i < Avatars.Length; i++)
            {
                Avatars[i].avatar.Set(part, style, color);
            }
        }
        else Avatars[_currentAvatar].avatar.Set(part, style, color);
    }

    public Sprite[] GetSprites(AVATAR_PART part, int color, bool shadow) => Avatars[_currentAvatar].avatar.GetSprites(part, color, shadow);

    public void SaveAvatar()
    {
        if (Avatars[_currentAvatar].avatar.Completed)
        {
            Avatar avatar = Avatars[_currentAvatar].avatar;
            string save = avatar.Save();

            JSONNode temp = Session.Avatar;
            if (temp != null)
            {
                Data data = avatar.GetData();
                temp["head"] = data.head;
                temp["head_color"] = data.head_color;
                temp["body"] = data.body;
                temp["body_color"] = data.body_color;
                temp["hair"] = data.hair;
                temp["hair_color"] = data.hair_color;
                temp["eyebrown"] = data.eyebrown;
                temp["eyes"] = data.eyes;
                temp["mouth"] = data.mouth;
                temp["nose"] = data.nose;
                temp["genre"] = data.genre;
                temp["has_updated"] = true;
            }

            this.Put("avatar", save, (string json) =>
            {
                Back();

            }, (UnityWebRequest www) =>
            {
                Debug.LogWarning("Send later...");
                Back();
            });
        }
        else print("El avatar no est� completo!");
    }

    private void Back() => RotateScreen.Load("Main", RotateScreen.ORIENTATION.PORTRAIT);

    private void OnDisable() => AudioManager.instance.SlideVolume(0.0f);
}
