using Newtonsoft.Json;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Avatar : MonoBehaviour
{
    [System.Serializable]
    public class Data
    {
        public int genre;
        public int head;
        public int head_color;
        public int body;
        public int body_color;
        public int eyes;
        public int hair;
        public int hair_color;
        public int nose;
        public int mouth;
        public int eyebrown;
        public bool has_updated;
    }

    public enum AVATAR_PART
    {
        Body,
        Head,
        Mouth,
        Eyes,
        Nose,
        Hair,
        Eyebrown
    }

    [Header("Data")]
    private Data _data = new Data();
    [SerializeField] private bool isBoy = false;
    [SerializeField] private bool _forceHide = false;
    [SerializeField] private AvatarScriptableObject Sprites = null;

    [Header("Parts")]
    [SerializeField] private Image Body = null;
    [SerializeField] private Image Head = null;
    [SerializeField] private Image Mouth = null;
    [SerializeField] private Image Eyes = null;
    [SerializeField] private Image Nose = null;
    [SerializeField] private Image Hair = null;
    [SerializeField] private Image Eyebrown = null;

    public void Load(string json, Action showSilhouette)
    {
        if (string.IsNullOrEmpty(json))
        {
            print("No avatar data");
            Hide();
            return;
        }

        _data = JsonConvert.DeserializeObject<Data>(json);

        if (_data == null)
        {
            print("Avatar data error");
            Hide();
            return;
        }

        bool boy = _data.genre == 1;
        if (boy == isBoy)
        {
            if (!_data.has_updated)
            {
                showSilhouette?.Invoke();
                Hide();
                return;
            }

            ChangeSprite(Body, Sprites.body[_data.body_color].sprites[_data.body]);
            ChangeSprite(Hair, Sprites.GetHairs(_data.head).hairColors[_data.hair_color].sprites[_data.hair]);
            ChangeSprite(Head, Sprites.head[_data.head_color].sprites[_data.head]);
            ChangeSprite(Eyes, Sprites.eyes[_data.eyes]);
            ChangeSprite(Nose, Sprites.nose[_data.nose]);
            ChangeSprite(Mouth, Sprites.mouth[_data.mouth]);
            ChangeSprite(Eyebrown, Sprites.eyebrown[_data.eyebrown]);
            gameObject.SetActive(true);
        }
        else Hide();
    }

    private void Hide() => gameObject.SetActive(!_forceHide);

    public void Set(AVATAR_PART part, int style, int color = 0)
    {
        switch (part)
        {
            case AVATAR_PART.Body:
                _data.body = style;
                _data.body_color = color;
                ChangeSprite(Body, Sprites.body[color].sprites[style]);
                break;
            case AVATAR_PART.Head:
                _data.head = style;
                _data.head_color = color;
                ChangeSprite(Head, Sprites.head[color].sprites[style]);
                ChangeSprite(Hair, Sprites.GetHairs(_data.head).hairColors[_data.hair_color].sprites[_data.hair]);
                break;
            case AVATAR_PART.Mouth:
                _data.mouth = style;
                ChangeSprite(Mouth, Sprites.mouth[style]);
                break;
            case AVATAR_PART.Eyes:
                _data.eyes = style;
                ChangeSprite(Eyes, Sprites.eyes[style]);
                break;
            case AVATAR_PART.Nose:
                _data.nose = style;
                ChangeSprite(Nose, Sprites.nose[style]);
                break;
            case AVATAR_PART.Hair:
                _data.hair = style;
                _data.hair_color = color;
                ChangeSprite(Hair, Sprites.GetHairs(_data.head).hairColors[color].sprites[style]);
                break;
            case AVATAR_PART.Eyebrown:
                _data.eyebrown = style;
                ChangeSprite(Eyebrown, Sprites.eyebrown[style]);
                break;
        }
    }

    public void ChangeColor(AVATAR_PART part, int color)
    {
        switch (part)
        {
            case AVATAR_PART.Body:
                _data.body_color = color;
                ChangeSprite(Body, Sprites.body[color].sprites[_data.body]);
                break;
            case AVATAR_PART.Hair:
                _data.hair_color = color;
                ChangeSprite(Hair, Sprites.GetHairs(_data.head).hairColors[color].sprites[_data.hair]);
                break;
            case AVATAR_PART.Head:
                _data.head_color = color;
                ChangeSprite(Head, Sprites.head[color].sprites[_data.head]);
                break;
        }
    }

    private void ChangeSprite(Image image, Sprite sprite)
    {
        image.sprite = sprite;
        image.SetNativeSize();
        image.enabled = true;
    }

    public Sprite[] GetSprites(AVATAR_PART part, int color, bool shadow)
    {
        return part switch
        {
            AVATAR_PART.Body => Sprites.body[color].sprites,
            AVATAR_PART.Head => shadow ? Sprites.shadows : Sprites.head[color].sprites,
            AVATAR_PART.Mouth => Sprites.mouth,
            AVATAR_PART.Eyes => Sprites.eyes,
            AVATAR_PART.Nose => Sprites.nose,
            AVATAR_PART.Hair => Sprites.GetHairs(_data.head).hairColors[color].sprites,
            AVATAR_PART.Eyebrown => Sprites.eyebrown,
            _ => null,
        };
    }

    public bool Selected(AVATAR_PART part)
    {
        return part switch
        {
            AVATAR_PART.Body => Body.enabled,
            AVATAR_PART.Head => Head.enabled,
            AVATAR_PART.Mouth => Mouth.enabled,
            AVATAR_PART.Eyes => Eyes.enabled,
            AVATAR_PART.Nose => Nose.enabled,
            AVATAR_PART.Hair => Hair.enabled,
            AVATAR_PART.Eyebrown => Eyebrown.enabled,
            _ => true,
        };
    }

    public bool Completed => Eyes.enabled && Nose.enabled && Mouth.enabled && Eyebrown.enabled;

    public Data GetData() => _data;

    public string Save()
    {
        _data.has_updated = true;
        _data.genre = isBoy ? 1 : 0;
        return JsonConvert.SerializeObject(_data, Formatting.None);
    }
}
