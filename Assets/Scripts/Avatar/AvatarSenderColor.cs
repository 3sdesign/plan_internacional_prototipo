using UnityEngine;
using UnityEngine.UI;
using static Avatar;

public class AvatarSenderColor : MonoBehaviour
{
    [SerializeField] private AVATAR_PART Part = default;

    private void Awake()
    {
        int index = transform.GetSiblingIndex();
        GetComponent<Button>().onClick.AddListener(() =>
        {
            AudioManager.instance.PlaySFX("Star");

            AvatarSender[] senders = FindObjectsOfType<AvatarSender>();
            for (int i = 0; i < senders.Length; i++)
            {
                senders[i].ChangeSprite(Part, index);
            }

            Avatar[] avatars = FindObjectsOfType<Avatar>();
            for (int i = 0; i < avatars.Length; i++)
            {
                avatars[i].ChangeColor(Part, index);
            }
        });
    }
}
