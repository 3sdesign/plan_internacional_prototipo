using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class AtlasSetter : MonoBehaviour
{
    [SerializeField] private SpriteAtlas Atlas = null;
    [SerializeField] private Image[] Images = null;

    private void Awake()
    {
        if (Images.Length == 0) Images = transform.GetComponentsInChildren<Image>(true);

        for (int i = 0; i < Images.Length; i++)
        {
            Sprite sprite = Atlas.GetSprite(Images[i].sprite.name);
            if (sprite) Images[i].sprite = sprite;
        }

        Destroy(this);
    }
}
