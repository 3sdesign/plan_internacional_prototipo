using UnityEngine;
using static RotateScreen;

public class ButtonToRotatedScene : ButtonToScene
{
    [SerializeField] private ORIENTATION Orientation = default;

    protected override void OnClick() => Load(SceneName, Orientation);
}
