﻿using UnityEngine;

public abstract class GenericSingletonClass<T> : MonoBehaviour where T : Component {

	private static T _instance;
    [SerializeField] private bool dontDestroyOnLoad = true;
	public static T instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<T>();
				if (_instance == null) {
					_instance = new GameObject { name = typeof(T).Name }.AddComponent<T>();
				}
			}
			return _instance;
		}
	}

	public virtual void Awake () {
		if (_instance == null) {
			_instance = this as T;
            if(dontDestroyOnLoad)
			    DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}
}