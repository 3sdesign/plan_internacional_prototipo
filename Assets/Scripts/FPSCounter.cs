using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FPSCounter : MonoBehaviour
{
    private Text _label = null;
    private float _deltaTime = 0;

    private void Awake() => _label = GetComponent<Text>();

    private void Update()
    {
        _deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;
        _label.text = Mathf.Ceil(1.0f / _deltaTime).ToString();
    }
}
