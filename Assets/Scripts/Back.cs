using UnityEngine;
using UnityEngine.SceneManagement;

public class Back : MonoBehaviour
{
    [SerializeField] private string BackScene = null;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(BackScene);
        }
    }
}