using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DisplayMessage : MonoBehaviour
{
    private static DisplayMessage _Instance;

    [SerializeField] private Text txtTitle;
    [SerializeField] private Text txtMessage;
    [SerializeField] private RectTransform displayRect;

    private float timeOnScreen;
    System.Action OnFinish;

    public static void Show(string title, string message, System.Action OnFinish, float timeOnScreen = 2f)
    {
        if (_Instance == null)
        {
            //_Instance = Instantiate(Resources.Load<DisplayMessage>("DisplayMessage")); // -- Not async
            //_Instance.displayRect.localScale = Vector2.zero;
            //DontDestroyOnLoad(_Instance.gameObject);

            var a = Resources.LoadAsync<DisplayMessage>("DisplayMessage");
            a.completed += r =>
            {
                _Instance = Instantiate(a.asset as DisplayMessage);
                _Instance.displayRect.localScale = Vector2.zero;
                DontDestroyOnLoad(_Instance.gameObject);

                _Instance.ShowMessage(title, message, OnFinish, timeOnScreen);
                Debug.Log("Display Message Loaded");
            };
            return;
        }

        _Instance.ShowMessage(title, message, OnFinish, timeOnScreen);
    }

    void ShowMessage(string title, string message, System.Action OnFinish, float timeOnScreen)
    {
        gameObject.SetActive(true);
        txtTitle.text = title;
        txtMessage.text = message;
        this.timeOnScreen = timeOnScreen;
        this.OnFinish = OnFinish;
        displayRect.DOScale(1, 0.6f).SetEase(Ease.OutBack).OnComplete(() => Invoke("Hide", timeOnScreen));
    }

    void Hide()
    {
        displayRect.DOScale(0, 0.6f).SetEase(Ease.InBack).OnComplete(() => { OnFinish?.Invoke(); gameObject.SetActive(false); });
    }
}
