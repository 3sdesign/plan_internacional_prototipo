using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

public class EnergyHUD : MonoBehaviour
{
    [SerializeField] private Image bar = null;
    [SerializeField] private Text numbers = null;
    private float ActualValue;
    private float QuantityValue;
    private int txt_MaxValue = 0;
    private int txt_Value = 0;
    public Action OnLimitOver = null;
    public void Config(float MaxLimit, Action onLimitOver = null)
    {
        QuantityValue = (1 / MaxLimit);
        txt_MaxValue = (int)MaxLimit;
        numbers.text = ActualValue + " / " + txt_MaxValue;
        if (onLimitOver != null) OnLimitOver = onLimitOver;
    }
    public void PlusBar()
    {
        Refresh(1);
        ActualValue += QuantityValue;
        bar.DOFillAmount(ActualValue,0.5f);
        if (ActualValue >= 1) OnLimitOver?.Invoke();
    }
    public void ReduceBar()
    {
        Refresh(-1);
        ActualValue -= QuantityValue;
        if (ActualValue >= 0) {
            bar.DOFillAmount(ActualValue, 0.5f);
        }
    }
    private void Refresh(int value)
    {
        txt_Value += value;
        numbers.text = txt_Value + " / " + txt_MaxValue;
    }
    public void ResetEnergyBar() {
        txt_Value = 0;
        ActualValue = 0;
        bar.DOFillAmount(ActualValue, 0.5f); 
        numbers.text = ActualValue + " / " + txt_MaxValue; }
    public float GetActualValue() { return ActualValue; }
}
