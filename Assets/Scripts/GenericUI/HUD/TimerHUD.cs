using System;
using UnityEngine;
using UnityEngine.UI;

public class TimerHUD : MonoBehaviour
{
    private float CurrentTime = 0;
    private bool Running = false;
    private int TimeSense = 1;
    private Text Label = null;

    public Action OnTimeOver = null;

    protected void Awake() => Label = GetComponentInChildren<Text>();

    public void SetTimer(int time,Action onTimeOver = null)
    {
        CurrentTime = time;
        TimeSense = -1;
        Refresh();
        if (onTimeOver != null) OnTimeOver = onTimeOver;
    }

    public void Play() => Running = true;
    public void Pause() => Running = false;
    public void PlusTime(int seconds) => CurrentTime += seconds;
    public float GetCurrentTime() { return CurrentTime; }

    private void Update()
    {
        if (Running)
        {
            CurrentTime += Time.deltaTime * TimeSense;

            if (TimeSense == -1 && CurrentTime < 0)
            {
                CurrentTime = 0;
                Running = false;
            }

            Refresh();

            if (!Running) OnTimeOver?.Invoke();
        }
    }
    private void Refresh() => Label.text = string.Format("{0:00}:{1:00}", Mathf.FloorToInt(CurrentTime / 60), CurrentTime % 60);
}
