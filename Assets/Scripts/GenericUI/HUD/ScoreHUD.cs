using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class ScoreHUD : MonoBehaviour
{
    [SerializeField] private Text txtCoins = null;
    private int _RoundPoints = 0;
    private int _Totalpoints = 0;

    private void Awake()=> txtCoins.text = _Totalpoints.ToString();
    public void PlusPoints(int value, float time = 1f, System.Action onComplete = null) {
        if(time > 0) StartCoroutine(AddPoints(value, time, onComplete));
        else
        {
            AudioManager.instance.PlaySFX("Answer");
            _RoundPoints += value;
            txtCoins.text = (_Totalpoints + _RoundPoints).ToString();
            onComplete?.Invoke();
        }
    }
    public void RestartPoints() => SetPoints(_Totalpoints);
    public void SetPoints(int value)
    {
        _RoundPoints = 0;
        _Totalpoints = value;
        txtCoins.text = _Totalpoints.ToString();
    }
    public int GetTotalPoints() => _Totalpoints;
    public int GetRoundPoints() => _RoundPoints;
    public void CompleteRound(){
        _Totalpoints += _RoundPoints;
        _RoundPoints = 0;
    }
    public IEnumerator AddPoints(int value,float time, System.Action onComplete = null)
    {
        yield return new WaitForSeconds(time);

        for (int i = 0; i < value; i++)
        {
            AudioManager.instance.PlaySFX("Answer");
            _RoundPoints++;
            txtCoins.text = (_Totalpoints + _RoundPoints).ToString();
            yield return new WaitForSeconds((time - 0.5f) / value);
        }

        onComplete?.Invoke();
    }
}
