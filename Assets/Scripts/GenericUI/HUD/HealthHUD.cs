using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class HealthHUD : MonoBehaviour
{
    private Tween tween;
    private int MaxHealth = 3;
    private int ActHealth = 3;
    private Slider healthslider = null;
    public Action OnHealthOver = null;
    private bool isFullHealth = true;

    protected void Awake() => healthslider = GetComponentInChildren<Slider>();
    public void SetHealth(int health, Action onHealthOver = null)
    {
        healthslider.maxValue = health;
        healthslider.value = health;
        MaxHealth = health;
        ActHealth = health;
        isFullHealth = true;
        if (onHealthOver != null) OnHealthOver = onHealthOver;
    }
    public void AddHealth(int value = 1)
    {
        int target = ActHealth + value;
        if (target >= MaxHealth) { 
            isFullHealth = true;
            if (ActHealth < MaxHealth) {
                tween.Kill();
                tween = healthslider.DOValue(MaxHealth, 0.4f);
                ActHealth = MaxHealth;
            }
            return;
        }
        tween.Kill();
        ActHealth = target;
        tween = healthslider.DOValue(target, 0.4f); 
    }

    public void SubtractHealth(int value = 1)
    {
        isFullHealth = false;
        int target = ActHealth - value;
        if (target <= 0)
        {
            if(ActHealth > 0)
            {
                tween.Kill();
                tween = healthslider.DOValue(0, 0.4f);
                ActHealth = 0;
                OnHealthOver?.Invoke();
            }
            return;
        }
        tween.Kill();
        ActHealth = target;
        tween = healthslider.DOValue(target, 0.4f);

       
    }
    public float GetHealthValue() { return ActHealth; }
    public bool IsFullHealth() { return isFullHealth; }
}
