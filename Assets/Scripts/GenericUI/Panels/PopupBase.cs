using UnityEngine;
using System;
using DG.Tweening;

[RequireComponent(typeof(Canvas))]
[RequireComponent(typeof(UnityEngine.UI.GraphicRaycaster))]
public class PopupBase : MonoBehaviour
{
    [SerializeField] protected RectTransform container;
    public bool IsShown { get => _canvas.enabled; }
    protected Canvas _canvas;

    protected virtual void Start()
    {
        _canvas = GetComponent<Canvas>();
        _canvas.enabled = false;
        if (container == null) container = GetComponent<RectTransform>();
        container.SetLocalScale(0, 0);
    }

    public void ShowStatic()
    {
        _canvas.enabled = true;
        container.SetLocalScale(1, 1);
    }

    public virtual void ShowPopup(Action OnComplete = null, bool update = false)
    {
        _canvas.enabled = true;
        container.DOScale(1, 0.5f).SetEase(Ease.OutBack).SetUpdate(update).OnComplete(() => OnComplete?.Invoke());
    }

    public virtual void ClosePopup(Action OnComplete = null, bool update = false)
    {
        container.DOScale(0, 0.5f).SetEase(Ease.InBack).SetUpdate(update).OnComplete(() =>
        {
            _canvas.enabled = false;
            OnComplete?.Invoke();
        });
    }
}
