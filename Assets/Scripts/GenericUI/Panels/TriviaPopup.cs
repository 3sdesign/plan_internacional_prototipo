using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static QuizScriptableObject;

public class TriviaPopup : MonoBehaviour
{
    [SerializeField] private RectTransform QuestionRt = null;
    [SerializeField] private Text QuestionTitle = null;
    [SerializeField] private TextMeshProUGUI QuestionText = null;
    [SerializeField] private Image QuestionImage = null;
    [SerializeField] private Answer AnswerRef = null;
    [SerializeField] private Button btnOK = null;
    [SerializeField] private Transform background = null;
    [SerializeField] private Transform questTrans = null;

    [Header("JetpackData")]
    [SerializeField] private TriviaScriptableObject triviaBGdata = null;

    [Header("Question")]
    [SerializeField] private Text Instruction = null;

    private List<Answer> Answers = new List<Answer>();
    private Transform answersParent;

    private void Start()
    {
        answersParent = AnswerRef.transform.parent;
        AnswerRef.gameObject.SetActive(false);
        gameObject.SetActive(false);
        ClearAnswers();
        background.GetComponent<Image>().sprite = triviaBGdata.GetBackground(Session.GetLevel(), Session.Eda);
    }

    void SetQuestionText(string text, string title)
    {
        /*if (!string.IsNullOrEmpty(title))
        {
            text = $"<size=+6>{title}</size>\n" + text;
        }*/
        QuestionText.text = text;
        QuestionText.enableAutoSizing = false;
        QuestionText.fontSize = 50;
        QuestionText.ForceMeshUpdate();

        /*float target = QuestionText.preferredHeight + 189.5f;

        if (target > 400)
        {
            target = 400;
            QuestionText.enableAutoSizing = true;
        }

        QuestionRt.SetAnchorHeight(target);*/
    }

    void ClearAnswers()
    {
        if (Answers.Count == 0)
            return;

        for (int i = 0; i < Answers.Count; i++)
        {
            Destroy(Answers[i].gameObject);
        }

        Answers.Clear();
    }

    void EnableLayout(bool enable) => answersParent.GetComponent<VerticalLayoutGroup>().enabled = enable;

    internal void LaunchQuestion(QuestionData data, int n, Action<bool> onComplete, bool HideBg = true)
    {
        gameObject.SetActive(true);
        background.DOScale(1, 0.5f);
        QuestionImage.transform.parent.gameObject.SetActive(false);

        var answersShuffle = Utils.GetCopyShuffle(data.answers);

        // Reset
        QuestionRt.anchoredPosition = QuestionRt.localScale = Vector2.zero;

        for (int i = 0; i < answersShuffle.Length; i++)
        {
            Answer _answer = Instantiate(AnswerRef, answersParent);
            _answer.gameObject.SetActive(true);
            Answers.Add(_answer);
        }

        EnableLayout(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)answersParent);
        EnableLayout(false);

        transform.localScale = Vector3.one;

        SetQuestionText(data.text, data.title);

        QuestionTitle.text = "Pregunta #" + n;

        btnOK.transform.localScale = Vector2.zero;
        btnOK.interactable = false;
        btnOK.onClick.RemoveAllListeners();
        btnOK.onClick.AddListener(() => btnOK.interactable = false);

        if (data.quizType == QUIZ_TYPE.Default)
        {
            QuestionText.rectTransform.anchoredPosition = new Vector2(0, -20);

            Instruction.gameObject.SetActive(false);

            for (int i = 0; i < answersShuffle.Length; i++)
            {
                Answers[i].Init(answersShuffle[i].isCorrect, answersShuffle[i].text, answer => DefaultListener(answer, onComplete));
                Answers[i].PrepareDafault();
            }

            btnOK.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("Star");
                SetAnswers(false);

                this.AfterTime(0.5f, () =>
                {
                    ShowResults(0, () =>
                    {
                        // pausa de tiempo SOBA!!!
                        this.AfterTime(1, () =>
                        {
                            bool defaultOK = false;
                            for (int j = 0; j < Answers.Count; j++)
                            {
                                Answer answer = Answers[j];
                                answer.Disapear();

                                if (answer.isTarget && answer.isMultipleSelected) defaultOK = true;
                            }

                            if (defaultOK)
                            {
                                QuestionRt.DOScale(0, 0.5f).SetEase(Ease.InBack).SetDelay(1).OnComplete(() =>
                                {
                                    QuestionRt.anchoredPosition = Vector2.zero;

                                    for (int i = 0; i < Answers.Count; i++)
                                    {
                                        int temp = i;
                                        Answers[i].transform.DOScale(0, 0.25f).SetEase(Ease.InBack).OnComplete(() =>
                                        {
                                            Answers[temp].ToDown();
                                        });
                                    }

                                    End(onComplete, true, HideBg);
                                });
                            }
                            else
                            {
                                End(onComplete, false, HideBg);
                            }
                        });

                    });
                });

            });
        }
        else
        {
            bool multiple = data.quizType == QUIZ_TYPE.Multiple;

            QuestionText.rectTransform.anchoredPosition = new Vector2(0, -5);
            //QuestionText.rectTransform.sizeDelta = new Vector2(925, 70);
            if (QuestionText.text == string.Empty) Instruction.fontSize = 36;
            else Instruction.fontSize = 30;
            Instruction.text = multiple ? "Puedes seleccionar m�s de una respuesta." : "Responde todas las preguntas marcando Verdadero o Falso.";
            if(Session.GetLevel() > 0) Instruction.gameObject.SetActive(true);


            if (multiple)
            {
                for (int i = 0; i < answersShuffle.Length; i++)
                {
                    Answers[i].Init(answersShuffle[i].isCorrect, answersShuffle[i].text, answer => MultipleListener(answer, onComplete));
                    Answers[i].PrepareDafault();
                }

                btnOK.onClick.AddListener(() =>
                {
                    // Evaluate multiple
                    bool IsSelectionOK = true;
                    for (int i = 0; i < Answers.Count; i++)
                    {
                        Answer answer = Answers[i];
                        bool isCorrect = answer.isMultipleSelected == answer.isTarget;

                        if (!isCorrect)
                        {
                            // It's not OK at all
                            IsSelectionOK = false;
                            break;
                        }
                    }
                    AudioManager.instance.PlaySFX("Star");
                    SetAnswers(false);
                    this.AfterTime(0.5f, () =>
                    {
                        ShowResults(0, () =>
                        {
                            this.AfterTime(1, () =>
                            {
                                for (int j = 0; j < Answers.Count; j++)
                                {
                                    Answers[j].Disapear();
                                }

                                if (IsSelectionOK)
                                {
                                    QuestionRt.DOScale(0, 0.5f).SetEase(Ease.InBack).SetDelay(1).OnComplete(() =>
                                    {
                                        QuestionRt.anchoredPosition = Vector2.zero;

                                        for (int i = 0; i < Answers.Count; i++)
                                        {
                                            int temp = i;
                                            Answers[i].transform.DOScale(0, 0.25f).SetEase(Ease.InBack).OnComplete(() =>
                                            {
                                                Answers[temp].ToDown();
                                            });
                                        }

                                        End(onComplete, true, HideBg);
                                    });
                                }
                                else
                                {
                                    End(onComplete, false, HideBg);
                                }
                            });

                        });
                    });
                });
            }
            else
            {
                for (int i = 0; i < answersShuffle.Length; i++)
                {
                    Answers[i].Init(answersShuffle[i].isCorrect, answersShuffle[i].text, null);
                    Answers[i].PrepareTrueFalse();
                }

                btnOK.onClick.AddListener(() =>
                {
                    // Evaluate true false
                    bool IsAllOK = true;
                    for (int i = 0; i < Answers.Count; i++)
                    {
                        bool isCorrect = Answers[i].EvaluateTrueFalseToggles();
                        Answers[i].SetFeedbackSprite(isCorrect);

                        if (!isCorrect)
                        {
                            // It's not OK at all
                            IsAllOK = false;
                            break;
                        }
                    }
                    AudioManager.instance.PlaySFX("Star");
                    SetAnswers(false);

                    this.AfterTime(0.5f, () =>
                    {
                        ShowResults(0, () =>
                        {
                            this.AfterTime(1, () =>
                            {

                                for (int j = 0; j < Answers.Count; j++)
                                {
                                    Answers[j].Disapear();
                                }

                                if (IsAllOK)
                                {
                                    QuestionRt.DOScale(0, 0.5f).SetEase(Ease.InBack).SetDelay(1).OnComplete(() =>
                                    {
                                        QuestionRt.anchoredPosition = Vector2.zero;

                                        for (int i = 0; i < Answers.Count; i++)
                                        {
                                            int temp = i;
                                            Answers[i].transform.DOScale(0, 0.25f).SetEase(Ease.InBack).OnComplete(() =>
                                            {
                                                Answers[temp].ToDown();
                                            });
                                        }

                                        End(onComplete, true, HideBg);
                                    });
                                }
                                else
                                {
                                    End(onComplete, false, HideBg);
                                }

                            });

                        });
                    });
                });
            }

        }

        RectTransform container = QuestionRt.parent.GetComponent<RectTransform>();
        container.SetAnchorPosY(-400);
        questTrans.localScale = Vector2.one;
        QuestionRt.DOScale(1, 0.5f).SetEase(Ease.OutBack).SetDelay(1.0f).OnStart(() => AudioManager.instance.PlaySFX("Question")).OnComplete(() =>
            container.DOAnchorPos(new Vector2(0, 0), 0.5f).SetEase(Ease.OutBack).SetDelay(0.5f).OnComplete(() =>
            {
                LoadImage(data.imageCode);

                MoveAnswers(0, () =>
                {
                    SetAnswers(true);
                    btnOK.transform.DOScale(1, 0.3f).SetEase(Ease.OutBack);
                });
            }));
    }

    void DefaultListener(Answer answer, Action<bool> onComplete)
    {
        btnOK.interactable = true;
        AudioManager.instance.PlaySFX("Star");

        for (int i = 0; i < Answers.Count; i++)
        {
            Answers[i].HideLike();
            Answers[i].isMultipleSelected = false;

        }

        answer.ShowLike(false);
        answer.isMultipleSelected = true;
    }

    void MultipleListener(Answer answer, Action<bool> onComplete)
    {
        btnOK.interactable = true;
        AudioManager.instance.PlaySFX("Star");
        if (!answer.isMultipleSelected)
        {
            answer.ShowLike(false);
            answer.isMultipleSelected = true;
        }
        else
        {
            answer.HideLike();
            answer.isMultipleSelected = false;
        }
    }

    private void LoadImage(string code)
    {
        Sprite loaded = Resources.Load<Sprite>($"Questions/{code}");

        Transform parent = QuestionImage.transform.parent;

        if (loaded == null)
        {
            parent.gameObject.SetActive(false);
            return;
        }

        parent.localScale = Vector2.zero;
        parent.gameObject.SetActive(true);
        QuestionImage.sprite = loaded;

        parent.DOScale(1, 0.5f).SetEase(Ease.OutBack);
    }

    public void End(Action<bool> onComplete, bool result, bool hideBg)
    {
        if(hideBg == false)
        {
            questTrans.DOScale(0, 0.5f).SetEase(Ease.InBack).SetDelay(0.5f).OnComplete(() =>
            {
                QuestionRt.localScale = Vector2.zero;
                onComplete(result);
                ClearAnswers();
            });
        }
        else
        {
            transform.DOScale(0, 0.5f).SetEase(Ease.InBack).SetDelay(0.5f).OnComplete(() =>
            {
                QuestionRt.localScale = Vector2.zero;
                background.localScale = Vector2.zero;
                onComplete(result);
                gameObject.SetActive(false);
                ClearAnswers();
            });
        }
        
    }

    // Answers
    private void SetAnswers(bool x)
    {
        for (int j = 0; j < Answers.Count; j++)
        {
            Answers[j].SetInteractable(x);
        }
    }

    private void MoveAnswers(int n, Action onComplete)
    {
        if (n < Answers.Count)
        {
            AudioManager.instance.PlaySFX("Answer");
            Answers[n].MoveToOrigin(() => MoveAnswers(n + 1, onComplete));
        }
        else onComplete();
    }

    // Results
    private void ShowResults(int n, Action onComplete)
    {
        if (n < Answers.Count)
        {
            Answers[n].ShowResult(() => ShowResults(n + 1, onComplete));
        }
        else onComplete();
    }
}
