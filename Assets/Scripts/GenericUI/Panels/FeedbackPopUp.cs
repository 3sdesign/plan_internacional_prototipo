﻿using System;
using System.Collections;
using DG.Tweening;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using static RotateScreen;

public class FeedbackPopUp : PopupBase
{
    [Serializable]
    private struct Mark
    {
        public int mark;
    }

    [Serializable]
    public class CharactersSprite
    {
        public Sprite[] Male = null;
        public Sprite[] Female = null;
    }
    [SerializeField] private RectTransform PopUp = null;
    [SerializeField] private Transform[] Stars = null;
    [SerializeField] private Text Title = null, Content = null;
    [SerializeField] private Button MenuButton = null, ResetButton = null, NextButton = null;
    [SerializeField] private Transform GroupMoney = null;
    [SerializeField] private Text cant1 = null, cant2 = null;

    [Header("Character")]
    [SerializeField] private Image Character = null;
    [SerializeField] private CharactersSprite[] MyChars = null;
    [SerializeField] private Sprite[] FinalSprt = null;
    [SerializeField] private Sprite PausaSprt = null;
    private Sprite[] Characters = null;

    private Action onNext = null, onReset = null;
    private FEEDBACK _feedback = default;
    private bool _completed = false;

    [Header("Pausa")]
    [SerializeField] private Button Pause = null;

    protected void Awake()
    {
        ResetStars();
        PopUp.localScale = Vector3.zero;

        Interactable(false);

        MenuButton.onClick.AddListener(() =>
        {
            Interactable(false);
            Load("EDA", ORIENTATION.PORTRAIT);
        });

        ResetButton.onClick.AddListener(() =>
        {
            Interactable(false);
            ClosePopup(() =>
            {
                if (Pause) Pause.interactable = true;
                AudioManager.instance.SlideVolume(1f);
                onReset?.Invoke();
            }, true);
        });

        NextButton.onClick.AddListener(() =>
        {
            Interactable(false);
            ClosePopup(() =>
            {
                if (_completed) Load("EDA", ORIENTATION.PORTRAIT, _feedback);
                else
                {
                    if (Pause) Pause.interactable = true;
                    AudioManager.instance.SlideVolume(1f);
                    onNext?.Invoke();
                }

            }, true);
        });

        if (Pause) Pause.onClick.AddListener(ShowPausa);
    }

    protected override void Start()
    {
        base.Start();
        Characters = Session.Friend == 0 ? MyChars[Session.KidType()].Female : MyChars[Session.KidType()].Male;
    }

    private void Interactable(bool state)
    {
        MenuButton.interactable = false;
        NextButton.interactable = state;
        ResetButton.interactable = state;
    }

    public void ShowTryAgain(Action onComplete = null, bool showMenuBtn = false)
    {
        AudioManager.instance.SlideVolume(0.2f);
        SetCharacter(1);
        Title.text = "";
        Content.text = "Vuelve a intentarlo";
        AudioManager.instance.PlaySFX("Lose");

        ConfigInteraction(0, onComplete, null, showMenuBtn, false, onComplete != null);
    }

    public void Show(string title, string message, int stars, Action onComplete = null, bool Menu = false, Action onReset = null)
    {
        AudioManager.instance.SlideVolume(0.2f);
        AudioManager.instance.PlaySFX("Win");
        SetCharacter(0);
        Title.text = title;
        Content.text = message;

        ConfigInteraction(Mathf.Clamp(stars, 0, Stars.Length), onReset, onComplete, Menu, onComplete != null, onReset != null);
    }

    public void ShowMessage(string message, Action onComplete = null, Sprite custom = null)
    {
        if (custom == null) SetCharacter(0);
        else ChangeSprite(custom);
        Content.text = message;
        Title.text = "";

        ConfigInteraction(0, null, onComplete, false, onComplete != null, false);
    }

    public void ShowCorrect(Action onComplete = null)
    {
        AudioManager.instance.SlideVolume(0.2f);
        AudioManager.instance.PlaySFX("Win");
        SetCharacter(0);
        Content.text = "Felicidades";
        Title.text = "Muy Bien";

        ConfigInteraction(0, null, onComplete, false, onComplete != null, false);
    }

    public void ShowFail(string message, Action onComplete = null, Action onReset = null)
    {
        AudioManager.instance.SlideVolume(0.2f);
        AudioManager.instance.PlaySFX("Lose");
        SetCharacter(1);
        Content.text = message;
        Title.text = string.Empty;

        ConfigInteraction(0, onReset, onComplete, false, onComplete != null, onReset != null);
    }

    public void ShowFinalPopup(int corrects, int limit = 5)
    {
        int stars = 0;
        if (corrects == 1 || corrects == 2)
        {
            stars = 1;
        }
        else if (corrects == 3)
        {
            stars = 2;
        }
        else if (corrects == 4 || corrects == 5)
        {
            stars = 3;
        }
        else stars = 0;

        AudioManager.instance.SlideVolume(0.2f);
        AudioManager.instance.PlaySFX("Win");

        if (stars == 1)
        {
            Title.text = "";
            Content.text = "Puedes hacerlo mejor";
            ChangeSprite(FinalSprt[Session.KidType()]);
        }
        else if (stars == 2)
        {
            Title.text = "Bien";
            Content.text = "Regular";
            ChangeSprite(FinalSprt[Session.KidType()]);
        }
        else if (stars >= 3)
        {
            Title.text = "Felicidades";
            Content.text = "Muy Bien";
            ChangeSprite(FinalSprt[Session.KidType()]);
        }
        else
        {
            Title.text = "";
            Content.text = "No lo hiciste bien";
            SetCharacter(1);
        }

        SendMark(20 * corrects, (FEEDBACK feedback) =>
        {
            _completed = true;
            _feedback = feedback;
            ConfigInteraction(Mathf.Clamp(stars, 0, Stars.Length), null, null, false, true, false);
        });
    }

    private void SendMark(int mark, Action<FEEDBACK> onComplete)
    {
        string operation = $"adaMark/{Session.CurrentID}";
        string data = JsonConvert.SerializeObject(new Mark { mark = mark }, Formatting.None);

        this.Put(operation, data, (string json) =>
        {
            Debug.Log("Data sended");
            onComplete(FEEDBACK.ONLINE);

        }, (UnityWebRequest www) =>
        {
            Debug.LogWarning("Data not sended, trying to save...");
            RequestSaver.SavePutData(operation, data);

            onComplete(www.result == UnityWebRequest.Result.ConnectionError ? FEEDBACK.OFFLINE : FEEDBACK.ERROR);
        });
    }

    public void ShowMoney(string title, string message, int cantidad1, int cantidad2, Action onComplete = null)
    {
        AudioManager.instance.SlideVolume(0.2f);
        AudioManager.instance.PlaySFX("Win");
        SetCharacter(0);
        Title.text = title;
        Content.text = message;
        cant1.text = "X 0";
        cant2.text = "X " + (cantidad2 - cantidad1);
        StartCoroutine(AddPoints(0, cantidad1, 1, cant1));
        StartCoroutine(AddPoints(cantidad2 - cantidad1, cantidad2, 2, cant2));

        ConfigInteraction(0, null, onComplete, false, onComplete != null, false);
        GroupMoney.localScale = Vector3.one;
    }
    //-------PAUSA-----------
    private void ShowPausa()
    {
        Time.timeScale = 0;
        AudioManager.instance.SlideVolume(0.2f);
        Title.text = "PAUSA";
        Content.text = "";
        ChangeSprite(PausaSprt);
        ConfigInteraction(0, null, () =>
        {
            AudioManager.instance.SlideVolume(1f);
            Time.timeScale = 1;
        }, false, true, false);
    }
    private void SetCharacter(int n)
    {
        if (n < Characters.Length) ChangeSprite(Characters[n]);
    }
    public void ChangeSprite(Sprite sprite, bool nativeSize = false)
    {
        Character.sprite = sprite;
        if (nativeSize) Character.SetNativeSize();
    }

    private void ConfigInteraction(int stars = 0, Action callReset = null, Action callNext = null, bool menuInteractable = false, bool nextInteractable = false, bool resetInteractable = false)
    {
        ResetStars();
        Interactable(false);
        GroupMoney.localScale = Vector2.zero;
        PopUp.anchoredPosition = Vector2.zero;
        if (Pause) Pause.interactable = false;
        ShowPopup(() =>
        {
            onNext = callNext;
            onReset = callReset;

            PopNextStar(0, stars, () =>
            {
                MenuButton.interactable = menuInteractable;
                NextButton.interactable = nextInteractable;
                ResetButton.interactable = resetInteractable;
            });
        }, true);
    }

    private void ResetStars()
    {
        for (int i = 0; i < Stars.Length; i++) Stars[i].localScale = Vector3.zero;
    }

    private void PopNextStar(int index, int max, Action onComplete)
    {
        if (index < max) Stars[index].DOScale(1.0f, 0.5f).OnComplete(() => PopNextStar(index + 1, max, onComplete));
        else onComplete();
    }

    public IEnumerator AddPoints(int oldvalue, int newvalue, float time, Text txt)
    {
        int temp = oldvalue;
        yield return new WaitForSeconds(time);

        for (int i = 0; i < (newvalue - oldvalue); i++)
        {
            temp++;
            txt.text = "X " + temp.ToString();
            yield return new WaitForSeconds((time - 0.5f) / (newvalue - oldvalue));
        }
    }
}