﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System;

public class ReverseCounterPanel : MonoBehaviour
{
    private RectTransform _recttransform;
    public float currTime = 0f;
    public int counter = 1000;
    Action atEnd = null;
    [SerializeField] private Text Label = null;
    bool isCounting = false;

    protected void Awake()
    {
        _recttransform = transform as RectTransform;
        _recttransform.anchoredPosition = Vector2.up * 1200;
        GetComponent<Canvas>().enabled = false;
    }

    private void Update()
    {
        if (isCounting)
        {
            currTime -= Time.deltaTime;
            if (currTime <= 0)
            {
                counter--;
                if (counter <= 0)
                {
                    counter = 0;
                    currTime = 0f;
                    isCounting = false;
                    Hide();
                }
                else currTime += 1f;

                AudioManager.instance.PlaySFX("Star");
                Label.transform.DOScale(0f, 0.2f).SetEase(Ease.InBack).OnComplete(() =>
                {
                    Label.text = counter.ToString();
                    Label.transform.DOScale(1f, 0.2f).SetEase(Ease.OutBack);
                });
            }
        }
    }

    public void Show(int value, Action onComplete = null)
    {
        counter = Mathf.Clamp(value, 1, 9);
        Label.text = counter.ToString();

        if (onComplete != null) atEnd = onComplete;

        GetComponent<Canvas>().enabled = true;

        _recttransform.DOAnchorPosY(0f, 0.5f).SetEase(Ease.OutBack).OnComplete(() =>
        {
            currTime = 1f;
            isCounting = true;
        });
    }
    public void Hide() => _recttransform.DOAnchorPosY(1200, 0.5f).SetEase(Ease.InBack).SetDelay(0.2f).OnComplete(() => { atEnd?.Invoke(); GetComponent<Canvas>().enabled = false;
    });
}
