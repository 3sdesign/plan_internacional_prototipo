using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
[RequireComponent(typeof(Canvas))]
public class StartPanel : MonoBehaviour
{
    [System.Serializable]
    public class Characters
    {
        public Sprite[] myChars = null;
    }
    [Header("Panels")]
    [SerializeField] private RectTransform myPanel = null;
    [SerializeField] private PopupBase PanelIntruction = null;
    [Header("Buttons")]
    [SerializeField] private Button btn_Play = null;
    [SerializeField] private Button btn_Instruction = null;
    [SerializeField] private Button btn_BackInst = null;
    [Header("Text")]
    [SerializeField] private Text txt_instruction = null;

    [Header("CustomSprite")]
    [SerializeField] private Image Img_BG = null;
    [SerializeField] private Image Img_Char = null;
    [SerializeField] private Image Img_Inst = null;
    [SerializeField] private Sprite[] Background = null;
    [SerializeField] private Characters[] Chars = null;
    [SerializeField] private Sprite[] Inst = null;

    public System.Action OnClickButton = null;

    private void Start()
    {
        myPanel.anchoredPosition = new Vector3(0, 0, 0);
        ChangeSprites();

        btn_Play.onClick.AddListener(() =>
        {
            btn_Play.interactable = false;
            btn_Instruction.interactable = false;
            AudioManager.instance.PlaySFX("Answer");
            OnClickButton?.Invoke();
        });
        btn_Instruction.onClick.AddListener(() =>
        {
            btn_Play.interactable = false;
            btn_Instruction.interactable = false;
            PanelIntruction.ShowPopup(() => btn_BackInst.interactable = true);
        });
        btn_BackInst.onClick.AddListener(() =>
        {
            btn_BackInst.interactable = false;
            PanelIntruction.ClosePopup(() =>
            {
                btn_Play.interactable = true;
                btn_Instruction.interactable = true;
            });
        });
    }
    public void Config(string myInstruction = "", System.Action onClickButton = null, int locksActive = 5)
    {
        txt_instruction.text = myInstruction;
        if (onClickButton != null) OnClickButton = onClickButton;
    }
    public void Hide(System.Action OnComplete = null)
    {
        myPanel.DOAnchorPosY(-1500, 0.5f).OnComplete(() =>
        {
            OnComplete?.Invoke();
            GetComponent<Canvas>().enabled = false;
        });
    }
    private void ChangeSprites()
    {
        Scene scene = SceneManager.GetActiveScene();
        int index = 0;
        if (scene.name == "Jetpack") index = 0;
        else if (scene.name == "Crane") index = 1;
        else if (scene.name == "Chocolate") index = 2;
        else if (scene.name == "Memory") index = 3;
        else if (scene.name == "Ludo") index = 4;

        Img_BG.sprite = Background[index];
        Img_Char.sprite = Chars[Session.KidType()].myChars[index];
        Img_Inst.sprite = Inst[index];
    }
}
