using UnityEngine;

public static class RectTransform_Extensions
{
    /// <summary>
    /// Set the X value in sizeDelta of recTransform
    /// </summary>
    public static void SetAnchorWidth(this RectTransform rect, float X) => rect.sizeDelta = new Vector2(X, rect.sizeDelta.y);

    /// <summary>
    /// Set the Y value in sizeDelta of recTransform
    /// </summary>
    public static void SetAnchorHeight(this RectTransform rect, float Y) => rect.sizeDelta = new Vector2(rect.sizeDelta.x, Y);

    /// <summary>
    /// Set the X and Y value in sizeDelta of recTransform
    /// </summary>
    public static void SetAnchorSize(this RectTransform rect, float X, float Y) => rect.sizeDelta = new Vector2(X, Y);

    /// <summary>
    /// Set the X value in anchoredPosition of recTransform
    /// </summary>
    public static void SetAnchorPosX(this RectTransform rect, float X) => rect.anchoredPosition = new Vector2(X, rect.anchoredPosition.y);

    /// <summary>
    /// Set the Y value in anchoredPosition of recTransform
    /// </summary>
    public static void SetAnchorPosY(this RectTransform rect, float Y) => rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, Y);

    /// <summary>
    /// Set the Y value in anchoredPosition of recTransform
    /// </summary>
    public static void AddAnchorPosX(this RectTransform rect, float X) => rect.anchoredPosition = new Vector2(rect.anchoredPosition.x + X, rect.anchoredPosition.y);

    /// <summary>
    /// Set the Y value in anchoredPosition of recTransform
    /// </summary>
    public static void AddAnchorPosY(this RectTransform rect, float Y) => rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, rect.anchoredPosition.y + Y);

    /// <summary>
    /// Set the X and Y value in anchoredPosition of recTransform
    /// </summary>
    public static void SetAnchorPos(this RectTransform rect, float X, float Y) => rect.anchoredPosition = new Vector2(X, Y);

    /// <summary>
    /// Set the X and Y value in anchoredPosition of recTransform
    /// </summary>
    public static void SetAnchorPos(this RectTransform rect, Vector2 pos) => rect.anchoredPosition = pos;

    /// <summary>
    /// Set the X and Y value in localScale of rectTransform
    /// </summary>
    public static void SetLocalScale(this Transform rect, float X, float Y) => rect.localScale = new Vector2(X, Y);

    /// <summary>
    /// Set the X value in localScale of rectTransform
    /// </summary>
    public static void SetLocalScaleX(this Transform rect, float X) => rect.localScale = new Vector2(X, rect.localScale.y);
    /// <summary>
    /// Set the Y value in localScale of rectTransform
    /// </summary>
    public static void SetLocalScaleY(this Transform rect, float Y) => rect.localScale = new Vector2(rect.localScale.x, Y);

    /// <summary>
    /// Clear/Destroys the children of an object
    /// </summary>
    public static void Clear(this Transform transform, int alowedElementsLength = 0)
    {
        if (transform.childCount > alowedElementsLength)
        {
            for (int i = alowedElementsLength; i < transform.childCount; i++)
            {
                GameObject.Destroy(transform.GetChild(i).gameObject);
            }
        }
    }

    public static void SetAnchorMin(this RectTransform rect, float X, float Y) => rect.anchorMin = new Vector2(X, Y);
    public static void SetAnchorMax(this RectTransform rect, float X, float Y) => rect.anchorMax = new Vector2(X, Y);
    public static void SetPivot(this RectTransform rect, float X, float Y) => rect.pivot = new Vector2(X, Y);
}



