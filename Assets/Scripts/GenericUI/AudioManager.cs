﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioManager : GenericSingletonClass<AudioManager>
{
    public AudioSource BgmSource = null;
    public AudioSource EfxSource = null;

    // Sounds
    private float DesiredBGMVolume = 1;
    private Dictionary<string, AudioClip> Clips = new Dictionary<string, AudioClip>();

    public override void Awake()
    {
        base.Awake();
        VerifySource(ref BgmSource);
        VerifySource(ref EfxSource);
    }

    private void VerifySource(ref AudioSource source)
    {
        if (source) return;
        source = gameObject.AddComponent<AudioSource>();
    }

    float vel = 0;
    private void Update() => BgmSource.volume = Mathf.SmoothDamp(BgmSource.volume, DesiredBGMVolume, ref vel, 0.5f);

    public void PlayBgm(string clipName, float volume = 1.0f)
    {
        AudioClip clip = Resources.Load<AudioClip>($"Sounds/Music/{clipName}");
        SlideVolume(volume);
        PlayBgm(clip);
    }

    public void PlayBgm(AudioClip clip)
    {
        BgmSource.loop = true;
        BgmSource.clip = clip;
        BgmSource.Play();
    }

    public void StopBgm() => BgmSource.Stop();

    public void SlideVolume(float value) => DesiredBGMVolume = value;

    public void PlaySFX(string name, float volume = 1) => EfxSource.PlayOneShot(GetClip(name), volume);
    private AudioClip GetClip(string name)
    {
        string fullName = "Sounds/" + name;
        if (Clips.ContainsKey(fullName)) return Clips[fullName];
        else
        {
            AudioClip clip = Resources.Load<AudioClip>(fullName);
            Clips.Add(fullName, clip);
            return clip;
        }
    }

    public void Initialize() => print("AudioManager ON");
}