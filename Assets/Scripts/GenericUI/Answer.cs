using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;
using static QuizScriptableObject;
//[RequireComponent(typeof(Button))]
public class Answer : MonoBehaviour
{
    private RectTransform _recttransform = null;
    private Vector2 _initPosition = Vector2.zero;
    private Vector2 _downPosition = Vector2.zero;

    [SerializeField] private Button Select = null;
    [SerializeField] private Text Label = null;
    [SerializeField] private Transform Like = null;
    [SerializeField] private Transform Feedback = null;

    [SerializeField] private Toggle checkTrue;
    [SerializeField] private Toggle checkFalse;

    public Action<Answer> onSelect = null;

    [SerializeField] private Sprite CorrectSprite;
    [SerializeField] private Sprite IncorrectSprite;

    [HideInInspector] public bool isTarget;
    [HideInInspector] public bool isTrueFalseFilled;
    [HideInInspector] public bool isMultipleSelected;
    //private void Awake() => Init();
    void EnableTrueFalseToggles(bool enable)
    {
        checkTrue.gameObject.SetActive(enable);
        checkFalse.gameObject.SetActive(enable);
    }

    public void Init(bool isTarget, string text, Action<Answer> OnSelect)
    {
        _recttransform = transform as RectTransform;
        _initPosition = _recttransform.anchoredPosition;
        _downPosition = _initPosition + Vector2.down * 800;

        Like.localScale = Feedback.localScale = Vector3.zero;

        //Select = GetComponent<Button>();
        Select.onClick.RemoveAllListeners();
        Select.onClick.AddListener(() =>
        {
            onSelect?.Invoke(this);
        });

        Select.enabled = false;

        this.isTarget = isTarget;

        SetText(text);

        ToDown();

        if (!gameObject.activeSelf)
            gameObject.SetActive(true);

        onSelect = OnSelect;
    }

    public void PrepareTrueFalse()
    {
        EnableTrueFalseToggles(true);
        checkTrue.onValueChanged.AddListener(value =>
        {
            if (!isTrueFalseFilled) isTrueFalseFilled = true;
            if (value) checkFalse.SetIsOnWithoutNotify(false);
            else checkTrue.SetIsOnWithoutNotify(true);
        });

        checkFalse.onValueChanged.AddListener(value =>
        {
            if (!isTrueFalseFilled) isTrueFalseFilled = true;
            if (value) checkTrue.SetIsOnWithoutNotify(false);
            else checkFalse.SetIsOnWithoutNotify(true);

        });
    }

    public void PrepareDafault()
    {
        EnableTrueFalseToggles(false);
        Feedback.GetComponent<Image>().sprite = isTarget ? CorrectSprite : IncorrectSprite;
    }
    //

    public void SetFeedbackSprite(bool correct) => Feedback.GetComponent<Image>().sprite = correct ? CorrectSprite : IncorrectSprite;

    public bool EvaluateTrueFalseToggles()
    {
        if (!isTrueFalseFilled)
            return false;

        if (checkTrue.isOn && isTarget)
            return true;

        if (checkFalse.isOn && !isTarget)
            return true;

        return false;
    }

    public void ToDown() => _recttransform.anchoredPosition = _downPosition;

    public void MoveToOrigin(Action onComplete)
    {
        Like.localScale = Vector3.zero;
        Feedback.localScale = Vector3.zero;
        transform.localScale = Vector3.one;
        _recttransform.DOAnchorPos(_initPosition, 0.5f).SetEase(Ease.OutBack).OnComplete(() => onComplete());
    }

    public void SetText(string text) => Label.text = text;
    public void SetInteractable(bool x) => Select.enabled = x;
    public void ShowResult(Action onComplete) => Feedback.DOScale(1, 0.5f).SetEase(Ease.OutBack).OnComplete(() => onComplete());

    public void ShowLike(bool disappear = true) => Like.DOScale(1, 0.3f).SetEase(Ease.OutBack).OnComplete(() => { if (disappear) Like.DOScale(0, 0.25f).SetDelay(1.5f); });
    public void HideLike() => Like.DOScale(0, 0.3f);

    public void ReActivate() => Feedback.DOScale(0, 0.25f).OnComplete(() => SetInteractable(true));
    public void Disapear() => Feedback.DOScale(0, 0.25f);
}
