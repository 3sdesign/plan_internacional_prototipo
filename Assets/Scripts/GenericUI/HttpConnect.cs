using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace HttpConnection
{
    //--------------------- SELECTABLE METHODS
    public enum METHOD
    {
        POST,
        GET,
        PUT,
        DELETE
    }

    public enum CODE
    {
        OK_CODE = 200,
        INTERNAL_SERVER_ERROR_CODE = 500,
        NOT_FOUND = 404,
        BAD_GATEWAY_CODE = 502,
        SERVER_UNAVAILABLE_CODE = 503,
        GATEWAY_TIMEOUT_CODE = 504,
        UNAUTHORIZED = 401
    }

    public static class HttpConnect
    {
        #region FUNCTIONS
        //--------------------- FUNCTIONS (NEED DATA TO SEND)

        /// <summary>
        /// Request to server with a desired url sending json. Returns a json
        /// </summary>
        public static void HTTP_REQUEST(this MonoBehaviour thisObj, string actionName, string url, METHOD httpMethod, string json, Action<string> actionIfDone, Action<HttpErrorResponse> actionIfError = null)
        {
            thisObj.StartCoroutine(I_HTTP_REQUEST(actionName, url, httpMethod, json, actionIfDone, actionIfError));
        }

        /// <summary>
        /// Request to server with a desired url sending json. Returns a byte[]
        /// </summary>
        public static void HTTP_REQUEST(this MonoBehaviour thisObj, string actionName, string url, METHOD httpMethod, string json, Action<byte[]> actionIfDone, Action<HttpErrorResponse> actionIfError = null)
        {
            thisObj.StartCoroutine(I_HTTP_REQUEST(actionName, url, httpMethod, json, actionIfDone, actionIfError));
        }

        /// <summary>
        /// Request to download objects from server with a desired url sending json. Returns a byte[]
        /// </summary>
        public static void HTTP_REQUEST_DOWNLOAD(this MonoBehaviour thisObj, string actionName, string url, METHOD httpMethod, string json, Action<byte[]> OnFinishDownload, Action<HttpErrorResponse> OnError = null, Action<float> OnDownloading = null)
        {
            thisObj.StartCoroutine(I_HTTP_REQUEST_DOWNLOAD(actionName, url, httpMethod, json, OnFinishDownload, OnError, OnDownloading));
        }


        //--------------------- FUNCTIONS (NO NEED TO SEND DATA)

        /// <summary>
        /// Get from server with desired url. Returns a json
        /// </summary>
        public static void GET_REQUEST(this MonoBehaviour thisObj, string actionName, string url, Action<string> actionIfSuccess, Action<HttpErrorResponse> actionIfError = null)
        {
            thisObj.StartCoroutine(I_GET_REQUEST(url, actionName, actionIfSuccess, actionIfError));
        }

        /// <summary>
        /// Get from server with a desired url, download bytes. Returns a byte[]
        /// </summary>
        public static void GET_REQUEST_DOWNLOAD(this MonoBehaviour thisObj, string actionName, string url, Action<byte[]> OnFinishDownload, Action<HttpErrorResponse> OnError = null, Action<float> OnDownloading = null)
        {
            thisObj.StartCoroutine(I_GET_REQUEST_DOWNLOAD(actionName, url, OnFinishDownload, OnError, OnDownloading));
        }
        #endregion


        #region COROUTINES

        private static IEnumerator I_GET_REQUEST(string url, string actionName, Action<string> actionIfSuccess, Action<HttpErrorResponse> actionIfError = null)
        {

            Debug.Log("url " + actionName + ": " + url);
            using (UnityWebRequest request = UnityWebRequest.Get(url))
            {

                yield return request.SendWebRequest();

                if (request.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError("(ERROR) in " + actionName + " : " + request.error);
                    string responseError = request.downloadHandler.text;
                    if (!string.IsNullOrEmpty(responseError))
                        Debug.Log(request.downloadHandler.text);

                    actionIfError?.Invoke(HttpErrorResponse.Create(request.responseCode, responseError));
                }
                else
                {
                    string response = request.downloadHandler.text;

                    Debug.Log("Gotten from " + actionName + " : " + response);
                    actionIfSuccess?.Invoke(response);

                }
            }
        }

        private static IEnumerator I_GET_REQUEST_DOWNLOAD(string actionName, string url, Action<byte[]> OnFinishDownload, Action<HttpErrorResponse> OnError = null, Action<float> OnDownloading = null)
        {
            using (UnityWebRequest request = new UnityWebRequest(url))
            {
                Debug.Log("current URL: " + url);
                request.downloadHandler = new DownloadHandlerBuffer();

                request.SendWebRequest();

                while (!request.isDone && (request.result != UnityWebRequest.Result.ConnectionError || request.result != UnityWebRequest.Result.DataProcessingError || request.result != UnityWebRequest.Result.ProtocolError))
                {
                    OnDownloading?.Invoke(request.downloadProgress);
                    Debug.Log("this is downloading");
                    yield return null;
                }
                Debug.Log("this Finish Download");
                if (request.result != UnityWebRequest.Result.Success)
                {
                    //something if get an error...
                    Debug.LogError("Error while downloading: " + request.error);
                    string responseError = request.downloadHandler.text;
                    if (!string.IsNullOrEmpty(responseError))
                        Debug.Log(request.downloadHandler.text);

                    OnError?.Invoke(HttpErrorResponse.Create(request.responseCode, responseError));
                    yield break;
                }
                else
                {
#if !UNITY_WEBPLAYER


                    Debug.Log("current download progress: " + request.downloadProgress);

                    OnFinishDownload?.Invoke(request.downloadHandler.data);

#endif
                }
            }
        }

        private static IEnumerator I_HTTP_REQUEST(string actionName, string url, METHOD httpMethod, string json, Action<string> actionIfDone, Action<HttpErrorResponse> actionIfError = null)
        {
            Debug.Log(actionName + " url: " + url);

            Debug.Log(actionName + "Json: " + json);

            var jsonBinary = System.Text.Encoding.UTF8.GetBytes(json);

            DownloadHandlerBuffer downloadHandlerBuffer = new DownloadHandlerBuffer();

            UploadHandlerRaw uploadHandlerRaw = new UploadHandlerRaw(jsonBinary);
            uploadHandlerRaw.contentType = "application/json";

            string method = "";

            switch (httpMethod)
            {
                case METHOD.POST:
                    method = "POST";
                    break;
                case METHOD.GET:
                    method = "GET";
                    break;
                case METHOD.PUT:
                    method = "PUT";
                    break;
                case METHOD.DELETE:
                    method = "DELETE";
                    break;
                default:
                    break;
            }

            using (UnityWebRequest request = new UnityWebRequest(url, method, downloadHandlerBuffer, uploadHandlerRaw))
            {
                yield return request.SendWebRequest();

                if (request.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError("(Error) in " + actionName + " : " + request.error);

                    string responseError = request.downloadHandler.text;
                    if (!string.IsNullOrEmpty(responseError))
                        Debug.Log(request.downloadHandler.text);

                    actionIfError?.Invoke(HttpErrorResponse.Create(request.responseCode, responseError));
                }
                else
                {
                    Debug.Log("The " + actionName + " is complete");


                    string response = request.downloadHandler.text;

                    Debug.Log("Gotten from " + actionName + ": " + response);

                    actionIfDone?.Invoke(response);

                }
            }
        }

        private static IEnumerator I_HTTP_REQUEST(string actionName, string url, METHOD httpMethod, string json, Action<byte[]> actionIfDone, Action<HttpErrorResponse> actionIfError = null)
        {
            Debug.Log(actionName + " url: " + url);

            Debug.Log(actionName + "Json: " + json);

            var jsonBinary = System.Text.Encoding.UTF8.GetBytes(json);

            DownloadHandlerBuffer downloadHandlerBuffer = new DownloadHandlerBuffer();

            UploadHandlerRaw uploadHandlerRaw = new UploadHandlerRaw(jsonBinary);
            uploadHandlerRaw.contentType = "application/json";

            string method = "";

            switch (httpMethod)
            {
                case METHOD.POST:
                    method = "POST";
                    break;
                case METHOD.GET:
                    method = "GET";
                    break;
                case METHOD.PUT:
                    method = "PUT";
                    break;
                case METHOD.DELETE:
                    method = "DELETE";
                    break;
                default:
                    break;
            }

            using (UnityWebRequest request = new UnityWebRequest(url, method, downloadHandlerBuffer, uploadHandlerRaw))
            {
                yield return request.SendWebRequest();

                if (request.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError("(Error) in " + actionName + " : " + request.error);

                    string responseError = request.downloadHandler.text;
                    if (!string.IsNullOrEmpty(responseError))
                        Debug.Log(request.downloadHandler.text);

                    actionIfError?.Invoke(HttpErrorResponse.Create(request.responseCode, responseError));
                }
                else
                {
                    Debug.Log("The " + actionName + " is complete");

                    byte[] response = request.downloadHandler.data;

                    Debug.Log("Gotten from " + actionName + ": " + response);

                    actionIfDone?.Invoke(response);

                }
            }
        }

        private static IEnumerator I_HTTP_REQUEST_DOWNLOAD(string actionName, string url, METHOD httpMethod, string json, Action<byte[]> OnFinishDownload, Action<HttpErrorResponse> OnError = null, Action<float> OnDownloading = null)
        {

            Debug.Log(actionName + " url: " + url);

            Debug.Log(actionName + "Json: " + json);

            var jsonBinary = System.Text.Encoding.UTF8.GetBytes(json);

            DownloadHandlerBuffer downloadHandlerBuffer = new DownloadHandlerBuffer();

            UploadHandlerRaw uploadHandlerRaw = new UploadHandlerRaw(jsonBinary);
            uploadHandlerRaw.contentType = "application/json";

            string method = "";

            switch (httpMethod)
            {
                case METHOD.POST:
                    method = "POST";
                    break;
                case METHOD.GET:
                    method = "GET";
                    break;
                case METHOD.PUT:
                    method = "PUT";
                    break;
                case METHOD.DELETE:
                    method = "DELETE";
                    break;
                default:
                    break;
            }

            using (UnityWebRequest request = new UnityWebRequest(url, method, downloadHandlerBuffer, uploadHandlerRaw))
            {
                request.downloadHandler = new DownloadHandlerBuffer();

                request.SendWebRequest();

                while (!request.isDone && (request.result != UnityWebRequest.Result.ConnectionError || request.result != UnityWebRequest.Result.DataProcessingError || request.result != UnityWebRequest.Result.ProtocolError))
                {
                    OnDownloading?.Invoke(request.downloadProgress);

                    yield return null;
                }

                if (request.result != UnityWebRequest.Result.Success)
                {
                    //something if get an error...
                    Debug.LogError($"Error while downloading ({actionName}): " + request.error);
                    string responseError = request.downloadHandler.text;
                    if (!string.IsNullOrEmpty(responseError))
                        Debug.Log(request.downloadHandler.text);

                    OnError?.Invoke(HttpErrorResponse.Create(request.responseCode, responseError));
                    yield break;
                }
                else
                {
#if !UNITY_WEBPLAYER


                    Debug.Log(actionName + " response: " + request.downloadHandler.text);

                    OnFinishDownload?.Invoke(request.downloadHandler.data);
#endif
                }
            }
        }
        #endregion
    }

    public struct HttpErrorResponse
    {
        public CODE code { get; }
        public string text { get; }

        public HttpErrorResponse(long code, string text)
        {
            this.code = (CODE)code;
            this.text = text;
        }

        public static HttpErrorResponse Create(long code, string text)
        {
            return new HttpErrorResponse(code, text);
        }
    }
}

