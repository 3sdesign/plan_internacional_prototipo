using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public static void SetAlpha(this UnityEngine.UI.Graphic graphic, float alphaColorValue)
    {
        graphic.color = new Color(graphic.color.r, graphic.color.g, graphic.color.b, alphaColorValue);
    }

    public static string GetString<T>(this T[] arr)
    {
        if (arr == null) return "null";
        if (arr.Length == 0) return "[]";
        string result = "[" + arr[0];
        for (int i = 1; i < arr.Length; i++) result += $", {arr[i]}";
        result += "]";
        return result;
    }

    public static string GetString<T>(this List<T> arr)
    {
        if (arr == null) return "null";
        if (arr.Count == 0) return "[]";
        string result = "[" + arr[0];
        for (int i = 1; i < arr.Count; i++) result += $", {arr[i]}";
        result += "]";
        return result;
    }

    public static void Sort(this Transform t) => t.SetSiblingIndex(Random.Range(0, t.parent.childCount + 1));

    public static void ShuffleChildren(this Transform parent)
    {
        List<int> indexes = new List<int>();
        List<Transform> items = new List<Transform>();
        for (int i = 0; i < parent.childCount; ++i)
        {
            indexes.Add(i);
            items.Add(parent.GetChild(i));
        }

        foreach (var item in items)
        {
            item.SetSiblingIndex(indexes[UnityEngine.Random.Range(0, indexes.Count)]);
        }
    }

    public static T[] GetCopyShuffle<T>(T[] array)
    {
        T[] result = new T[array.Length];

        for (int i = 0; i < result.Length; i++)
            result[i] = array[i];

        for (int i = 0; i < result.Length; i++)
        {
            int rnd = Random.Range(0, result.Length);
            T temp = result[rnd];
            result[rnd] = result[i];
            result[i] = temp;
        }
        return result;
    }

    public static void AfterTime(this MonoBehaviour handler, float time, System.Action callBack) => handler.StartCoroutine(I_AfterTime(time, callBack));

    private static IEnumerator I_AfterTime(float time, System.Action callBack)
    {
        yield return new WaitForSeconds(time);
        callBack?.Invoke();
    }
}
