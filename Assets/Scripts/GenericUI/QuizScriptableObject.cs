using UnityEngine;

[CreateAssetMenu(fileName = "Question", menuName = "ScriptableObjects/Question", order = 1)]
public class QuizScriptableObject : ScriptableObject
{
    public enum QUIZ_TYPE
    {
        Default,
        Multiple,
        TrueFalse
    }

    [System.Serializable]
    public class QuestionData
    {
        public QUIZ_TYPE quizType;
        public string title;
        [TextArea(5, 10)] public string text;
        public string imageCode;
        public AnswerData[] answers;
    }

    [System.Serializable]
    public class AnswerData
    {
        public string text;
        public bool isCorrect;
    }

    public QuestionData[] questions;
}