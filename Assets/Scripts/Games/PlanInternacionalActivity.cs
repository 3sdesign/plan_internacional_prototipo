using UnityEngine;

public abstract class PlanInternacionalActivity : GameActivity
{
    [Header("Plan Internacional")]
    [SerializeField] private TriviaPopup Trivia = null;
    [SerializeField] private CustomItemsScriptableObject CustomItems = null;
    private QuizScriptableObject _data = null;
    private int[] myQuestions;
    protected virtual void Awake()
    {
        _data = Session.GetQuizData();
        Shuffle();
    }

    public void LaunchQuestion(int index, System.Action<bool> onComplete, bool hideBG = true) => Trivia.LaunchQuestion(_data.questions[myQuestions[index]], index + 1, onComplete, hideBG);
    public int QuestionsLength => 5;//_data.questions.Length;

    private void Shuffle()
    {
        myQuestions = new int[_data.questions.Length];
        for (int i = 0; i < _data.questions.Length; i++)
        {
            myQuestions[i] = i;
        }
        for (int i = 0; i < myQuestions.Length; i++)
        {
            int test = myQuestions[i];
            int random = Random.Range(0, myQuestions.Length);
            myQuestions[i] = myQuestions[random];
            myQuestions[random] = test;
        }
    }
    public void UpdateCustomSprite(out Sprite[] mySprites)
    {
        if (!CustomItems)
        {
            mySprites = null;
        }
        else
        {
            for (int i = 0; i < CustomItems.ItemsLength; i++)
            {
                var temp = CustomItems.Get(i);
                if (Session.GlobalGrade + 1 == temp.grade && Session.Eda + 1 == temp.eda && Session.Ada + 1 == temp.ada)
                {
                    mySprites = temp.sprites;
                    return;
                }
            }
            mySprites = null;
        }
    }
}
