using DG.Tweening;
using UnityEngine;

public class LudoManager : PlanInternacionalActivity
{
    [System.Serializable]
    public class BoxesSprite
    {
        public Sprite[] Sprites = null;
    }
    [System.Serializable]
    public class PropsSprite
    {
        public Sprite Background = null;
        public Sprite[] Props = null;
    }
    [System.Serializable]
    public class PathLevel
    {
        public PathGroup[] Paths = null;
    }
    [Header("JetpackData")]
    [SerializeField] private LudoDataScriptableObject ludoData = null;
    [Header("Gameplay")]
    [SerializeField] private Transform Player = null;
    [SerializeField] private Animator[] CharsAnima = null;
    [SerializeField] private PathLevel[] Levels = null;
    [Header("Sprites")]
    [SerializeField] private SpriteRenderer MyBg = null;
    [SerializeField] private Sprite[] NormalBoxes = null;
    [SerializeField] private Sprite[] Boxes = null;
    [SerializeField] private BoxesSprite[] AllSprites = null;
    [SerializeField] private PropsSprite[] AllProps = null;
    private int[] IndexSpecialBoxes = null;
    private Animator Anima = null;

    private Transform[] Paths = null;

    [Header("Carousel")]
    [SerializeField] private Transform TransCarousel = null;
    [SerializeField] private RandomItem Carousel = null;
    private int[] myCarouselArray = new int[] { 0, 1, 3 };
    [Header("Ruleta")]
    [SerializeField] private Rulete Ruleta = null;

    private int currentCorrect = 0;
    private int currentposition = 0, dice = 0, countSpecialBox = 0, currentQuestion = 0, round_level, round_index;
    private bool IsCarousel = false;
    protected override void Start()
    {
        base.Start();
        Session.Friend = 0;
        TransCarousel.transform.localScale = Vector2.zero;

        if (Session.GlobalGrade < 6) { Anima = CharsAnima[0]; CharsAnima[1].gameObject.SetActive(false); }
        else { Anima = CharsAnima[1]; CharsAnima[0].gameObject.SetActive(false); }
        Ruleta.OnComplete = () =>
        {
            Anima.SetBool("isJumping", true);
            Move(dice, 1);
        };
    }
    private void SelectPath()
    {
        round_level = Session.GetLevel();
        round_index = Random.Range(0, Levels[round_level].Paths.Length);
        Paths = Levels[round_level].Paths[round_index].Paths;
        Player.position = Paths[0].position;
        Levels[round_level].Paths[round_index].gameObject.SetActive(true);
        int newBioma = Random.Range(0, AllProps.Length);
        MyBg.sprite = AllProps[newBioma].Background;
        Levels[round_level].Paths[round_index].ChangeSprites(AllProps[newBioma].Props);
    }
    private void ClearPath()
    {
        for (int i = 0; i < Paths.Length; i++)
        {
            if (Paths[i].GetComponent<LudoItem>())
            {
                Paths[i].GetComponent<LudoItem>().ResetSprite(NormalBoxes);
                Destroy(Paths[i].GetComponent<LudoItem>());
            }
        }
        Levels[round_level].Paths[round_index].gameObject.SetActive(false);
    }
    private void InitRulete()
    {
        do
        {
            if (currentposition >= IndexSpecialBoxes[countSpecialBox]) countSpecialBox++;
            dice = Random.Range(1, 5);
        } while (currentposition + dice > IndexSpecialBoxes[countSpecialBox]);
        Ruleta.ShowRuleta(dice - 1);
        print("Dado : "+ (dice - 1));
    }
    private void Move(int steps, int sense)
    {
        int next = currentposition + sense;

        Vector2 target = Paths[next].position;

        float maxY = Mathf.Max(Player.position.y, target.y);

        Player.DOMoveY(maxY + 0.5f, 0.35f).SetDelay(0.5f).OnComplete(() => Player.DOMoveY(target.y, 0.25f));
        Player.DOMoveX(target.x, 0.5f).SetDelay(0.5f).OnComplete(() =>
        {
            AudioManager.instance.PlaySFX("Answer");
            currentposition = next;
            steps -= 1;
            if (steps > 0)
            {
                if (currentposition < Paths.Length - 1) Move(steps, sense);
            }
            else
            {
                Anima.SetBool("isJumping", false);
                if (currentposition == Paths.Length - 1)
                {
                    AddScore(2, 0f);
                    //ShowCorrectMoney("Lo lograste", $"Completaste el tablero, muy bien.", () => ValidateRound(true));
                    ValidateRound(true);
                    return;
                }
                if (Paths[currentposition].GetComponent<LudoItem>())
                {
                    int item = Paths[currentposition].GetComponent<LudoItem>().ItemType;
                    if (item <= 3) ValidateBoxes(item);
                    else if (item == 4) ActivateCarousel();
                }
                else InitRulete();
            }
        });
    }
    private void ConfigSpecialBoxes()
    {
        var level = ludoData.Get(Session.GetLevel());

        IndexSpecialBoxes = new int[level.TotalSpecialBlocks + 1]; //+1 para agregar la meta
        int boxesLimit = (int)((Paths.Length - 2) / level.TotalSpecialBlocks);

        int num;
        int borders = (Paths.Length - 2) - (boxesLimit * level.TotalSpecialBlocks);//dar un borde entre el inicio y final
        if (borders < 2) num = 0;
        else num = borders / 2;

        for (int i = 0; i < level.TotalSpecialBlocks; i++)
        {
            int[] myarray = new int[boxesLimit];
            for (int j = 0; j < boxesLimit; j++)
            {
                myarray[j] = 1 + num + j;
            }
            int tempsaver;
            int random;
            for (int j = 0; j < myarray.Length; j++)
            {
                tempsaver = myarray[j];
                random = Random.Range(0, myarray.Length);
                myarray[j] = myarray[random];
                myarray[random] = tempsaver;
            }
            IndexSpecialBoxes[i] = myarray[0];
            SetCustomBox(myarray[0]);

            for (int j = 0; j < level.CustomBlocks; j++)
            {
                SetCustomBox(myarray[j + 1], Random.Range(0, Boxes.Length));
            }
            num += boxesLimit;
        }
        IndexSpecialBoxes[IndexSpecialBoxes.Length - 1] = Paths.Length - 1;
    }
    private void ShowQuestion()
    {
        LaunchQuestion(currentQuestion, (bool ok) =>
        {
            currentQuestion++;
            if (currentQuestion == QuestionsLength)
            {
                for (int i = 0; i < Paths.Length; i++)
                {
                    if (Paths[i].GetComponent<LudoItem>())
                    {
                        if (Paths[i].GetComponent<LudoItem>().ItemType == 2)
                        {
                            Paths[i].GetComponent<LudoItem>().ChangeValues(AllSprites[3].Sprites, 3);
                        }
                    }
                }
            }
            if (!ok)
            {
                RestartScore();
                ShowFail("No lo lograste.", () => ValidateRound());
                return;
            }
            currentCorrect++;
            AddScore(2, 0f);
            ShowCorrectMoney("Lo lograste", $"Respondiste correctamente, muy bien.", () => ValidateRound());
        });
    }
    private void ValidateRound(bool isFinal = false)
    {
        if (!isFinal)
        {
            InitRulete();
        }
        else ShowFinalPopup(currentCorrect);
        /*if(!isFinal) InitRulete();
        else
        {
            ClearPath();
            ShowMessageAndCounter(); 
        }*/
    }
    private void SetCustomBox(int path, int customBox = 4)
    {
        string[] split = Paths[path].name.Split('|');
        string name = split[1];
        if (customBox != 0 && customBox != 1) name = name.Substring(0, name.Length - 1);

        for (int i = 0; i < AllSprites[customBox].Sprites.Length; i++)
        {
            if (AllSprites[customBox].Sprites[i].name.StartsWith(name))
            {
                Paths[path].GetComponent<SpriteRenderer>().sprite = AllSprites[customBox].Sprites[i];
            }
        }
        Paths[path].gameObject.AddComponent<LudoItem>().ItemType = customBox;
    }
    private void ActivateCarousel()
    {
        IsCarousel = true;
        TransCarousel.DOScale(1, 0.5f).SetEase(Ease.OutBack).OnComplete(() =>
        {
            if (currentQuestion < QuestionsLength)
            {
                Carousel.Select(2, () => TransCarousel.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() => ValidateBoxes(2)));
            }
            else
            {
                int value;
                do
                {
                    value = myCarouselArray[Random.Range(0, myCarouselArray.Length)];
                } while (value == 1);
                Carousel.Select(value, () => TransCarousel.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() => ValidateBoxes(value)));
            }
        });
    }
    private void ValidateBoxes(int num)
    {
        if (num == 0)
        {
            ShowMessage("Avanza una casilla.", () =>
            {
                DisableSpeedBox();
                if (IsCarousel) IsCarousel = false;
                Anima.SetBool("isJumping", true);
                Move(1, 1);
            });
        }
        else if (num == 1)
        {
            ShowMessage("Retrocede una casilla.", () =>
            {
                DisableSpeedBox();
                if (IsCarousel) IsCarousel = false;
                Anima.SetBool("isJumping", true);
                Move(1, -1);
            });
        }
        else if (num == 2)
        {
            ShowMessage("Responde la siguiente pregunta.", () =>
            {
                ShowQuestion();
                if (IsCarousel) IsCarousel = false;
            });
        }
        else if (num == 3)
        {
            AddScore(2, 1f);
            ShowMessage("Ganaste dos monedas.", () =>
            {
                InitRulete();
                if (IsCarousel) IsCarousel = false;
            });
        }
    }
    private void DisableSpeedBox()
    {
        if (IsCarousel) return;
        Paths[currentposition].GetComponent<LudoItem>().ResetSprite(NormalBoxes);
        Destroy(Paths[currentposition].GetComponent<LudoItem>());
    }
    protected override void ConfigGame()
    {
        currentposition = 0;
        countSpecialBox = 0;
        currentQuestion = 0;
        SelectPath();
        ConfigSpecialBoxes();
    }
    protected override void InitGame() => InitRulete();
    protected override void OnDie() { }
}