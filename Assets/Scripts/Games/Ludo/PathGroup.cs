using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathGroup : MonoBehaviour
{
    public Transform[] Paths = null;
    public SpriteRenderer[] Props = null;

    public void ChangeSprites(Sprite[] mysprites)
    {
        for(int i = 0; i < Props.Length; i++)
        {
            for (int j = 0; j < mysprites.Length; j++)
            {
                if (Props[i].name == mysprites[j].name) { Props[i].sprite = mysprites[j]; break; }
            }
        }
    }
}
