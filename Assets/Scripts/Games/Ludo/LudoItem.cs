using UnityEngine;

public class LudoItem : MonoBehaviour
{
    public int ItemType = 0;
    public void ResetSprite(Sprite[] NormalBoxes)
    {
        string temp = GetComponent<SpriteRenderer>().sprite.name;
        if (temp != "S" && temp != "UC" && temp != "DC" && temp != "LC" && temp != "RC") temp = temp.Substring(0, temp.Length - 1);
        for (int i = 0; i < NormalBoxes.Length; i++)
        {
            if (NormalBoxes[i].name.StartsWith(temp))
            {
                GetComponent<SpriteRenderer>().sprite = NormalBoxes[i];
                return;
            }
        }
    }
    public void ChangeValues(Sprite[] newSprt, int newVal)
    {
        ItemType = newVal;
        ResetSprite(newSprt);
    }
}
