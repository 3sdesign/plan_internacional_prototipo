using UnityEngine;
using UnityEngine.UI;
using static QuizScriptableObject;

public class TriviaPopupTest : MonoBehaviour
{
    [Header("Elements")]
    [SerializeField] private Button[] Buttons = null;

    [Header("Popup")]
    [SerializeField] private TriviaPopup Trivia = null;

    private QuizScriptableObject _data;

    private void Awake()
    {
        // Obtener data
        _data = Session.GetQuizData();

        for (int i = 0; i < Buttons.Length; i++)
        {
            int temp = i;
            Buttons[i].onClick.AddListener(() =>
            {
                ConfigureButtons(false);

                QuestionData q = _data.questions[temp];
                Trivia.LaunchQuestion(q, temp + 1, (bool ok) => ConfigureButtons(true));
            });
        }
    }

    private void ConfigureButtons(bool interactable)
    {
        for (int i = 0; i < Buttons.Length; i++)
        {
            Buttons[i].enabled = interactable;
        }
    }
}
