using UnityEngine;

[CreateAssetMenu(fileName = "Trivia", menuName = "ScriptableObjects/Trivia", order = 1)]
public class TriviaScriptableObject : ScriptableObject
{
    [System.Serializable]
    public struct LevelData
    {
        public Sprite[] sprites;
    }

    [SerializeField] private LevelData[] levels = null;

    public Sprite GetBackground(int level, int eda)
    {
        LevelData temp = levels[Mathf.Clamp(level, 0, levels.Length)];
        return temp.sprites[Mathf.Clamp(eda, 0, temp.sprites.Length)];
    }
}