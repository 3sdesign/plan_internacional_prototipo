using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SimpleTrivia : MonoBehaviour
{
    [SerializeField] private QuizScriptableObject Data = null;
    private int _currentQuestion = -1;

    [SerializeField] private ReverseCounterPanel Counter = null;

    [SerializeField] private Image LifeBar = null;
    private int lives = 5;

    [SerializeField] private TimerHUD TimerHUD = null;

    [SerializeField] private Text ScoreLabel = null;
    private int _points = 0;

    [SerializeField] private RectTransform QuestionRt = null;
    [SerializeField] private Text QuestionTitle = null;
    [SerializeField] private Text QuestionText = null;

    [SerializeField] private Answer[] Answers = null;

    [Header("Popup")]
    [SerializeField] private Popup Pop = null;

    private void Awake()
    {
        ScoreLabel.text = _points.ToString();
        Pop.gameObject.SetActive(false);
    }

    private void Start()
    {
        QuestionRt.localScale = Vector2.zero;
        QuestionRt.anchoredPosition = Vector2.left * 150;

        for (int i = 0; i < Answers.Length; i++)
        {
            Answers[i].ToDown();
        }

        Invoke(nameof(Initialize), 2);

        Popup.onReset = ShowQuestion;
        Popup.onNext = NextQuestion;
    }

    private void Initialize() => Counter.Show(3, NextQuestion);

    private void NextQuestion()
    {
        _currentQuestion++;
        ShowQuestion();
    }

    private void ShowQuestion()
    {
        //Invoke(nameof(ResetAnswers), 2);

        if (_currentQuestion < Data.questions.Length)
        {
            QuizScriptableObject.QuestionData q = Data.questions[_currentQuestion];
            QuestionTitle.text = "Pregunta " + (_currentQuestion + 1);
            QuestionText.text = q.text;

            TimerHUD.SetTimer(30);

            for (int i = 0; i < q.answers.Length; i++)
            {
                Answers[i].SetText(q.answers[i].text);

                Answers[i].onSelect = (Answer answer) =>
                {
                    AudioManager.instance.PlaySFX("Star");
                    DisableAnswers();
                    TimerHUD.Pause();

                    ShowResults(0, () =>
                    {
                        if (answer == Answers[0])
                        {
                            StartCoroutine(AddPoints(10));

                            QuestionRt.DOScale(0, 0.5f).SetEase(Ease.InBack).SetDelay(4).OnComplete(() =>
                            {
                                QuestionRt.anchoredPosition = Vector2.left * 150;

                                for (int i = 0; i < Answers.Length; i++)
                                {
                                    int temp = i;
                                    Answers[i].transform.DOScale(0, 0.25f).SetEase(Ease.InBack).OnComplete(() =>
                                    {
                                        Answers[temp].ToDown();
                                    });
                                }
                            });

                            Invoke(nameof(OnCorrect), 4);
                        }
                        else
                        {
                            lives--;
                            LifeBar.DOFillAmount(lives / 5f, 0.5f).SetDelay(3.5f);

                            Pop.Show(false, _points);
                        }
                    });
                };
            }

            QuestionRt.DOScale(1, 0.5f).SetEase(Ease.OutBack).SetDelay(1.0f).OnStart(() => AudioManager.instance.PlaySFX("Question")).OnComplete(() =>
                QuestionRt.DOAnchorPos(new Vector2(-285, 400), 0.5f).SetEase(Ease.OutBack).SetDelay(0.5f).OnComplete(() =>
                {
                    MoveAnswers(0, () =>
                    {
                        EnableAnswers();
                        TimerHUD.Play();
                    });
                }));            
        }
        else
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }

    private void OnCorrect()
    {
        Pop.Show(true, _points);
    }

    private void MoveAnswers(int n, Action onComplete)
    {
        if (n < Answers.Length)
        {
            AudioManager.instance.PlaySFX("Answer");
            Answers[n].MoveToOrigin(() => MoveAnswers(n + 1, onComplete));
        }
        else onComplete();
    }

    private void EnableAnswers()
    {
        SetAnswers(true);
    }

    private void DisableAnswers()
    {
        SetAnswers(false);
    }

    private void SetAnswers(bool x)
    {
        for (int j = 0; j < Answers.Length; j++)
        {
            Answers[j].SetInteractable(x);
        }
    }

    private void ShowResults(int n, Action onComplete)
    {
        if (n < Answers.Length)
        {
            Answers[n].ShowResult(() => ShowResults(n + 1, onComplete));
        }
        else onComplete();
    }

    private void ResetAnswers()
    {
        TimerHUD.Play();
        for (int j = 0; j < Answers.Length; j++)
        {
            Answers[j].ReActivate();
        }
    }

    private IEnumerator AddPoints(int value)
    {
        yield return new WaitForSeconds(2);
        AudioManager.instance.PlaySFX("Point");

        for (int i = 0; i < value; i++)
        {
            _points++;
            ScoreLabel.text = _points.ToString();
            yield return new WaitForSeconds(1.5f / value);
        }
    }
}
