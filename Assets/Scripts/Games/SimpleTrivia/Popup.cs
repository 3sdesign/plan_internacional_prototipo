using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Popup : MonoBehaviour
{
    [SerializeField] private Text Tittle = null;
    [SerializeField] private Text Content = null;
    [SerializeField] private Text Points = null;

    [Header("Character")]
    [SerializeField] private Sprite Win = null;
    [SerializeField] private Sprite Lose = null;
    [SerializeField] private Image Avatar = null;

    [Header("Buttons")]
    [SerializeField] private Button Reset = null;
    [SerializeField] private Button Next = null;
    public static Action onReset = null;
    public static Action onNext = null;
    
    private void Awake()
    {
        Reset.onClick.AddListener(() =>
        {
            Reset.interactable = false;

            transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                onReset?.Invoke();
                gameObject.SetActive(false);
            });
        });

        Next.onClick.AddListener(() =>
        {
            Next.interactable = false;
            transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                onNext?.Invoke();
                gameObject.SetActive(false);
            });
        });
    }

    public void Show(bool win, int points = 0, bool reset = true)
    {
        Points.text = points.ToString();
        transform.localScale = Vector3.zero;
        RectTransform avatarRt = Avatar.transform as RectTransform;

        AudioManager.instance.SlideVolume(0.2f);

        if (win)
        {
            Tittle.text = "BIEN HECHO";
            Content.text = "Felicidades, tu respuesta fue correcta.";

            Avatar.sprite = Win;
            avatarRt.anchoredPosition = new Vector2(-275, -100);

            Points.transform.parent.gameObject.SetActive(true);

            AudioManager.instance.PlaySFX("Win");

            Reset.interactable = false;
            Next.interactable = true;
        }
        else
        {
            Tittle.text = "VUELVE A INTENTARLO";
            Content.text = "No te desanimes, sigue intentando. Cada vez est�s m�s cerca.";

            Avatar.sprite = Lose;
            avatarRt.anchoredPosition = new Vector2(0, -100);

            Points.transform.parent.gameObject.SetActive(false);

            AudioManager.instance.PlaySFX("Lose");

            if (reset){
                Reset.interactable = true;
                Next.interactable = false;
            }
            else { 
                Reset.interactable = false;
                Next.interactable = true;
            }
           
        }

        Avatar.SetNativeSize();

        gameObject.SetActive(true);
        transform.DOScale(1, 0.5f).SetEase(Ease.OutBack);

        Invoke(nameof(UpVolume), 5.0f);
    }

    private void UpVolume() => AudioManager.instance.SlideVolume(1.0f);
}
