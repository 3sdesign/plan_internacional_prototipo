using UnityEngine;

[CreateAssetMenu(fileName = "CustomItems", menuName = "ScriptableObjects/CustomItems", order = 1)]
public class CustomItemsScriptableObject : ScriptableObject
{
    [System.Serializable]
    public struct ItemsData
    {
        public int grade;
        public int eda;
        public int ada;
        public Sprite[] sprites;
    }

    [SerializeField] private ItemsData[] items;
    public int ItemsLength => items.Length;
    public ItemsData Get(int item) => items[Mathf.Clamp(item, 0, items.Length)];
}
