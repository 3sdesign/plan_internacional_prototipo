using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PausePopup : MonoBehaviour
{
    [SerializeField] private Button Menu = null;
    [SerializeField] private Button Reset = null;
    [SerializeField] private Button Pause = null;
    [SerializeField] private Button Continue = null;

    [SerializeField] private Transform Panel = null;
    [SerializeField] private GameObject Container = null;

    public static Action onReset = null;

    private void Awake()
    {
        Container.SetActive(false);
        Panel.localScale = Vector3.zero;

        Pause.onClick.AddListener(() =>
        {
            Time.timeScale = 0;
            Pause.interactable = false;
            AudioManager.instance.SlideVolume(0.2f);

            Interactable(true);
            Container.SetActive(true);
            Panel.DOScale(1, 0.5f).SetEase(Ease.OutBack).SetUpdate(true);
        });

        Menu.onClick.AddListener(() =>
        {
            Interactable(false);
            SceneManager.LoadScene("Games");
        });

        Reset.onClick.AddListener(() =>
        {
            Interactable(false);
            Hide(true);
        });

        Continue.onClick.AddListener(() =>
        {
            Interactable(false);
            Hide(false);
        });
    }

    private void Interactable(bool state)
    {
        Menu.interactable = state;
        Reset.interactable = state && onReset != null;
        Continue.interactable = state;
    }

    private void Hide(bool reset) => Panel.DOScale(0, 0.5f).SetEase(Ease.InBack).SetUpdate(true).OnComplete(() =>
                         {
                             AudioManager.instance.SlideVolume(1f);
                             Container.SetActive(false);
                             Pause.interactable = true;
                             Time.timeScale = 1;

                             if (reset) onReset?.Invoke();
                         });
}
