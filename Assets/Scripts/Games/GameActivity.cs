using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using static RotateScreen;

public abstract class GameActivity : MonoBehaviour
{
    [Header("Navigation")]
    [SerializeField] private string BackScene = string.Empty;
    private int activityLevel = 1;
    protected int countReset = 0;

    [Header("Popups - Game")]
    [SerializeField] private FeedbackPopUp FeedbackPopup = null;
    [SerializeField] private ReverseCounterPanel Counter = null;
    [SerializeField] private StartPanel InitPanel = null;
    [SerializeField] private ScoreHUD Score = null;
    [SerializeField] private HealthHUD health = null;
    private int healthValue = 3;

    [Header("Instructions")]
    [SerializeField] private Sprite[] CustomSpriteIntro = null;
    [TextArea] public string mainInstruction = string.Empty;
    [TextArea] public string roundInstruction = string.Empty;

    protected virtual void Start()
    {
        PlayBGM();

        InitPanel.Config(mainInstruction, () =>
        {
            VariableMeshRenderer[] group = FindObjectsOfType<VariableMeshRenderer>();
            foreach (VariableMeshRenderer vari in group)
            {
                vari.Refresh();
            }
            VariableImage[] images = FindObjectsOfType<VariableImage>();
            foreach (VariableImage img in images)
            {
                img.Refresh();
            }

            ConfigGame();
            if (health) health.SetHealth(healthValue, OnDie);
            HideStartPanel(ShowMessageAndCounter);
        }, 1);
    }

    protected void PlayBGM() => AudioManager.instance.PlayBgm(SceneManager.GetActiveScene().name, 0.75f);

    protected void ShowMessageAndCounter() => ShowMessage(roundInstruction, () => ShowCounter(InitGame), CustomSpriteIntro.Length >= Session.KidType() ? CustomSpriteIntro[Session.KidType()] : null);
    protected abstract void ConfigGame();
    protected abstract void InitGame();
    protected abstract void OnDie();

    protected virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Load("EDA", ORIENTATION.PORTRAIT);
        }
    }
    protected int GetActivityLevel() => activityLevel;
    // ----------- Counter --------------
    public void ShowCounter(Action onComplete) => Counter.Show(3, onComplete);

    // ----------- HealthHUD -----------
    public void AddHealth(int value = 1) => health.AddHealth(value);
    public void SubtractHealth() => health.SubtractHealth();
    public void SetHealth(int life) { healthValue = life; health.SetHealth(healthValue); }
    public float GetHealthValue => health.GetHealthValue();
    public bool IsFullHealth => health.IsFullHealth();
    // ----------- ScoreHUD -----------
    public void RestartScore() => Score.RestartPoints();
    public void SetScore(int score) => Score.SetPoints(score);
    public void AddScore(int score, float time, Action onComplete = null) => Score.PlusPoints(score, time, onComplete);
    public void CompleteRound() => Score.CompleteRound();

    // ----------- StartPanel -----------
    public void ConfigStartPanel(string instruction = null, Action onClick = null, int level = 5) => InitPanel.Config(instruction, onClick, level);
    public void HideStartPanel(Action OnComplete = null) => InitPanel.Hide(OnComplete);

    // ----------- FeedBackPopUp --------------
    public void ShowTryAgain(Action onComplete)
    {
        if (FeedbackPopup) FeedbackPopup.ShowTryAgain(onComplete);
        else onComplete?.Invoke();
    }
    public void ShowCorrectMoney(string title, string message, Action onComplete = null)
    {
        if (FeedbackPopup) FeedbackPopup.ShowMoney(title, message, Score.GetRoundPoints(), Score.GetTotalPoints() + Score.GetRoundPoints(), onComplete);
        else onComplete?.Invoke();
        Score.CompleteRound();
    }
    public void ShowPopUp(string title, string message, int stars, Action onComplete = null, bool Menu = true, Action onReset = null)
    {
        if (onReset != null) countReset++;
        if (FeedbackPopup) FeedbackPopup.Show(title, message, stars, onComplete, Menu, countReset > 2 ? null : onReset);
        else onComplete?.Invoke();
    }
    public void ShowMessage(string message, Action onComplete = null, Sprite sprite = null)
    {
        if (FeedbackPopup) FeedbackPopup.ShowMessage(message, onComplete, sprite);
        else onComplete?.Invoke();
    }
    public void ShowCorrect(Action onComplete = null)
    {
        if (FeedbackPopup) FeedbackPopup.ShowCorrect(onComplete);
        else onComplete?.Invoke();
    }
    public void ShowFail(string message, Action onComplete = null, Action onReset = null)
    {
        if (onReset != null) countReset++;
        if (FeedbackPopup) FeedbackPopup.ShowFail(message, onComplete, countReset > 2 ? null : onReset);
        else onComplete?.Invoke();
    }
    public void ShowFinalPopup(int corrects)
    {
        if (FeedbackPopup) FeedbackPopup.ShowFinalPopup(corrects);
    }

    private void OnDisable() => AudioManager.instance.SlideVolume(0.0f);
}
