using DG.Tweening;
using UnityEngine;

public class LudoSoba : MonoBehaviour
{
    [SerializeField] private RectTransform Character = null;
    [SerializeField] private Transform Path = null;
    private int _currentposition = 0;
    private bool _moving = false;
    private int _sense = 1;

    private void Awake() => Character.anchoredPosition = GetChildPosition(_currentposition);

    private void Update()
    {
        if (Input.GetButtonDown("Jump") && !_moving)
        {
            _moving = true;
            Launch();
        }
    }

    private void Launch()
    {
        int random = Random.Range(1, 6);
        print("#" + random);
        _sense = 1;

        Move(random);
    }

    private void Move(int steps)
    {
        if (steps < 1)
        {
            _moving = false;
            return;
        }

        if (_currentposition == Path.childCount - 1) _sense = -1;
        int next = _currentposition + _sense;

        Character.DOAnchorPos(GetChildPosition(next), 0.5f).SetDelay(0.5f).OnComplete(() =>
        {
            _currentposition = next;
            Move(steps - 1);
        });
    }

    private Vector2 GetChildPosition(int index) => (Path.GetChild(index) as RectTransform).anchoredPosition;
}