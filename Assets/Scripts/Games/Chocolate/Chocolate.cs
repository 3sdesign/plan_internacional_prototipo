using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Chocolate : PlanInternacionalActivity
{
    [Header("Level Data")]
    [SerializeField] private ChocolateDataScriptableObject Data = null;
    private ChocolateDataScriptableObject.LevelData _levelData;

    [Header("Game")]
    [SerializeField] private Transform BasketContainer = null;
    [SerializeField] private GameObject BasketPrefab = null;
    [SerializeField] private Transform SpawnBall = null;
    [SerializeField] private Sprite ItemSprite = null;
    private ChocolateBasquet _basquet = null;
    private int currentRound = 0;

    [Header("HUD")]
    [SerializeField] private TimerHUD Timer = null;
    [SerializeField] private EnergyHUD energyBar = null;
    private bool IsHeart, EndQuest = false;
    private int _currentQuestion = 0;
    private int currentCorrect = 0;
    protected override void Start()
    {
        base.Start();
        Session.Friend = 0;
        UpdateCustomSprite(out Sprite[] NewSprites);
        if (NewSprites != null) ItemSprite = NewSprites[0];
    }
    protected override void ConfigGame()
    {
        _levelData = Data.Get(Session.GetLevel());

        energyBar.Config(_levelData.target, () =>
        {
            ShowPopUp("Ganaste", $"Ahora responde las siguientes preguntas.", 0, () =>
            {
                ShowQuestion();
            }, false, null);
            /* energyBar.ResetEnergyBar();
                EndQuest = true;
                ValidationRound();*/
        });
        Timer.SetTimer(_levelData.time, () =>
        {
            AudioManager.instance.PlaySFX("referee");
            OnDie();
        });
    }

    protected override void InitGame()
    {
        _levelData = Data.Get(Session.GetLevel());

        BasketContainer.GetComponent<HorizontalLayoutGroup>().enabled = true;
        if (_levelData.cups_quantity == 2) BasketContainer.localScale = Vector3.one * 1.5f;
        else if (_levelData.cups_quantity == 3) BasketContainer.localScale = Vector3.one * 1.2f;
        else if (_levelData.cups_quantity == 4) BasketContainer.localScale = Vector3.one * 0.9f;
        else BasketContainer.localScale = Vector3.one * 1f;
        GameObject correctChest = CreateChess();
        correctChest.transform.localScale = Vector2.zero;
        _basquet = correctChest.GetComponent<ChocolateBasquet>();

        correctChest.transform.DOScale(1, 0.5f).OnComplete(() =>
        {
            Transform correctKey = correctChest.transform.Find("key");
            correctKey.GetComponent<Image>().sprite = ItemSprite;
            correctKey.transform.position = SpawnBall.transform.position;
            correctKey.DOScale(1f, 0.5f).SetDelay(1f).OnComplete(() =>
            {
                _basquet.OpenBasquet(() =>
                {
                    /*Vector3 rot = Vector3.zero;

                    float distance = correctKey.position.x - correctChest.transform.position.x;
                    rot.z = distance * 0.8f;

                    correctKey.DORotate(rot, 0.7f).SetRelative();*/
                    correctKey.DOMoveX(correctChest.transform.position.x, 0.75f).OnComplete(() =>
                    {
                        correctKey.DOMoveY(correctChest.transform.position.y + 150, 0.75f).OnComplete(() =>
                        {
                            correctKey.SetSiblingIndex(1);
                            correctKey.DOMoveY(correctChest.transform.position.y - 50, 0.75f).OnComplete(() =>
                            {
                                _basquet.CloseBasquet(Shuffle);
                            });
                        });
                    });
                });
            });
        });
        correctChest.GetComponentInChildren<Button>().onClick.AddListener(() => Answer(0, correctChest));

        int lenght = _levelData.cups_quantity;
        for (int i = 0; i < lenght; i++)
        {
            GameObject chest = CreateChess();
            chest.transform.localScale = Vector2.zero;
            chest.transform.DOScale(1, 0.5f);
            chest.GetComponentInChildren<Button>().onClick.AddListener(() => Answer(1, chest));
            chest.transform.Sort();
        }

        IsHeart = GetProbability(_levelData.heart_probability);
    }
    private GameObject CreateChess() => Instantiate(BasketPrefab, BasketContainer);

    protected override void OnDie()
    {
        Timer.Pause();
        currentRound = 0;
        ShowFail(countReset > 1 ? "Ahora responde las siguientes preguntas." : "Vuelve a intentarlo.", () =>
        {   
            for (int i = 0; i < BasketContainer.childCount; i++) BasketContainer.GetChild(i).DOScale(0, 0.5f);
            /*
            Utils.AfterTime(this, 1, () =>
            {
                RemoveAllChildren(BasketContainer);
                ResetValues();
                ShowMessageAndCounter();
            });*/
            ShowQuestion();
        },() => {
            energyBar.ResetEnergyBar();
            EndQuest = true;
            ValidationRound();
        });
    }

    public void Answer(int n, GameObject chest = null)
    {
        bool ok = n == 0;
        AudioManager.instance.PlaySFX(ok ? "Answer" : "Lose");

        Intercatable(false);
        Timer.Pause();

        if (ok)
        {
            AddScore(1, 1);
            _basquet.OpenBasquet(() =>
            {
                RectTransform ball = chest.transform.Find("key").GetComponent<RectTransform>();
                ball.DOMoveY(chest.transform.position.y + 150, 0.75f).OnComplete(() =>
                {
                    AudioManager.instance.PlaySFX("Answer");
                    ball.SetSiblingIndex(2);
                    float totalTime = 1.5f;
                    ball.DOMoveY(SpawnBall.position.y, totalTime).SetEase(Ease.OutBounce).OnComplete(() =>
                    {
                        _basquet.CloseBasquet(() =>
                        {
                            Utils.AfterTime(this, 1, () =>
                            {
                                ball.SetParent(transform);
                                ball.DOScale(0, 0.5f).OnComplete(() => Destroy(ball.gameObject));
                            });
                            Invoke(nameof(ValidationRound), 1f);
                        });
                    });

                    float delay = 0.55f;
                    float duration = totalTime - delay;
                    ball.DOAnchorPosX(ball.anchoredPosition.x + 30, duration).SetDelay(delay);
                    //ball.DORotate(new Vector3(ball.localRotation.x, ball.localRotation.y, ball.localRotation.z - 50), duration).SetDelay(delay);
                });
            });
        }
        else
        {
            chest.GetComponent<ChocolateBasquet>().OpenBasquet(() =>
            {
                if (IsHeart)
                {
                    IsHeart = false;
                    AudioManager.instance.PlaySFX("Star");
                    chest.GetComponent<ChocolateBasquet>().CloseBasquet();
                    chest.transform.Find("Item_Heart").DOScale(1, 0.5f).SetEase(Ease.OutBack).OnComplete(() =>
                      chest.transform.Find("Item_Heart").DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() => Intercatable(true)
                    ));
                    return;
                }
                SubtractHealth();
                if (GetHealthValue > 0) Invoke(nameof(ResetRound), 2f);
            });
        }
    }
    private void ResetRound()
    {
        for (int i = 0; i < BasketContainer.childCount; i++)
        {
            BasketContainer.GetChild(i).DOScale(0, 0.5f);
        }
        Utils.AfterTime(this, 1, () =>
        {
            RemoveAllChildren(BasketContainer);
            Timer.SetTimer(_levelData.time);
            InitGame();
            return;
        });
    }
    private void ValidationRound()
    {
        for (int i = 0; i < BasketContainer.childCount; i++)
        {
            BasketContainer.GetChild(i).DOScale(0, 0.5f);
        }
        Utils.AfterTime(this, 1, () =>
          {
              RemoveAllChildren(BasketContainer);
              if (EndQuest)
              {
                  EndQuest = false;
                  ResetValues();
                  ShowMessageAndCounter();
              }
              else
              {
                  currentRound++;
                  energyBar.PlusBar();
                  Timer.SetTimer(_levelData.time);
                  if (currentRound < _levelData.target)
                  {
                      InitGame();
                  }
              }
          });
    }

    private void Shuffle() => StartCoroutine(Choko());
    private IEnumerator Choko()
    {
        AudioManager.instance.PlaySFX("referee");
        BasketContainer.GetComponent<HorizontalLayoutGroup>().enabled = false;
        yield return new WaitForSeconds(1.5f);
        GameObject[] myArray = new GameObject[BasketContainer.transform.childCount];
        // Sort

        int lenght = _levelData.shuffleQuantity;
        for (int i = 0; i < lenght; i++)
        {
            int nA = Random.Range(0, BasketContainer.childCount);
            int nB;
            do
            {
                nB = Random.Range(0, BasketContainer.childCount);
            } while (nA == nB);

            float speed = _levelData.velocity;
            Vector3 toA = BasketContainer.GetChild(nA).transform.position;
            Vector3 toB = BasketContainer.GetChild(nB).transform.position;
            for (float t = 0f; t < 1f; t += (Time.deltaTime / .35f) * speed)
            {
                BasketContainer.GetChild(nA).transform.position = Vector3.Lerp(toA, toB, t);
                BasketContainer.GetChild(nB).transform.position = Vector3.Lerp(toB, toA, t);
                if (t < 0.5f)
                {
                    BasketContainer.GetChild(nA).transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 1.2f, t * 2);
                }
                else
                {
                    BasketContainer.GetChild(nA).transform.localScale = Vector3.Lerp(Vector3.one * 1.2f, Vector3.one, (t - 0.5f) * 2);
                }
                yield return null;
            }
            BasketContainer.GetChild(nA).transform.position = toB;
            BasketContainer.GetChild(nB).transform.position = toA;
        }

        yield return new WaitForSeconds(0.5f);
        Intercatable(true);
        Timer.Play();
    }
    private void ShowQuestion()
    {
        LaunchQuestion(_currentQuestion, (bool ok) =>
        {
            if (ok) currentCorrect++;
            _currentQuestion++;
            if (_currentQuestion >= 5)
            {
                /*if (!ok)
                {
                    RestartScore();
                    ShowFail("No lo lograste.", ValidateRound);
                    return;
                }
                currentCorrect++;
                ShowCorrectMoney("Lo lograste", $"Respondiste correctamente, muy bien.", ValidateRound);*/
                ValidateRound();
            }
            else Utils.AfterTime(this, 1, ShowQuestion);
        }, _currentQuestion < 4 ? false : true);
    }
    private void ValidateRound()
    {
        currentRound = 0;
        if (_currentQuestion < 5)
        {
            energyBar.ResetEnergyBar();
            EndQuest = true;
            ValidationRound();
        }
        else ShowFinalPopup(currentCorrect);
    }
    private void Intercatable(bool value)
    {
        for (int i = 0; i < BasketContainer.childCount; i++)
        {
            BasketContainer.GetChild(i).GetComponent<Button>().interactable = value;
        }
    }
    private bool GetProbability(int percent)
    {
        float rnd = UnityEngine.Random.Range(0, 101);

        if (rnd < percent) return true;
        else return false;
    }
    private void ResetValues()
    {
        currentRound = 0;
        SetHealth(3);
        RestartScore();
        energyBar.ResetEnergyBar();
        Timer.SetTimer(_levelData.time);
    }
    public void RemoveAllChildren(Transform t)
    {
        foreach (Transform child in t)
        {
            Destroy(child.gameObject);
        }
    }
}
