using UnityEngine;

[CreateAssetMenu(fileName = "ChocolateData", menuName = "ScriptableObjects/ChocolateData", order = 1)]
public class ChocolateDataScriptableObject : ScriptableObject
{
    [System.Serializable]
    public struct LevelData
    {
        public int shuffleQuantity;
        public int target;
        public float velocity;
        public int cups_quantity;
        public int time;
        public int heart_probability;
    }

    [SerializeField] private LevelData[] levels = null;

    public LevelData Get(int level) => levels[Mathf.Clamp(level - 1, 0, levels.Length)];
}
