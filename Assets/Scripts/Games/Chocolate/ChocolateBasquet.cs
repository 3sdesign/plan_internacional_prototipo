using DG.Tweening;
using UnityEngine;

public class ChocolateBasquet : MonoBehaviour
{
    [SerializeField] private RectTransform Tapa = null;

    public void OpenBasquet(System.Action OnComplete = null)
    {
        Tapa.DOAnchorPosY(250, 0.5f).OnComplete(() => OnComplete?.Invoke()); 
    }
    public void CloseBasquet(System.Action OnComplete = null)
    {
        Tapa.DOAnchorPosY(120, 0.5f).OnComplete(() => OnComplete?.Invoke());
    }
}
