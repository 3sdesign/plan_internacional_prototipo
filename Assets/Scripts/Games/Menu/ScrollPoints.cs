using UnityEngine;
using UnityEngine.UI;

public class ScrollPoints : MonoBehaviour
{
    [SerializeField] private Swipe Scroll = null;
    [SerializeField] private Color FullColor = Color.green;
    [SerializeField] private Color BlankColor = Color.white;

    private void Awake()
    {
        SetMark(0);
        Scroll.onFocus += SetMark;
    }

    private void SetMark(int n)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeInHierarchy) transform.GetChild(i).GetComponent<Image>().color = i == n ? FullColor : BlankColor;
        }
    }
}
