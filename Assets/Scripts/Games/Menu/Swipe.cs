using System;
using UnityEngine;
using UnityEngine.UI;

public class Swipe : MonoBehaviour
{
    [SerializeField] private Scrollbar Scroll = null;
    private float[] Positions = null;
    private float Position = 0;
    private float HalfDis = 0;

    private Vector2 _bigScale = Vector2.one;
    private Vector2 _smallScale = Vector2.one;

    public Action<int> onFocus = null;
    private int Current = 0;

    private string KEY = "SCROLL";

    private void Awake()
    {
        _bigScale = Vector2.one;
        _smallScale = Vector2.one * 0.8f;
    }

    private void Start()
    {
        Positions = new float[0];
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeInHierarchy) Positions = new float[Positions.Length + 1];
        }
        float separation = 1f / (Positions.Length - 1f);
        for (int i = 0; i < Positions.Length; i++)
        {
            Positions[i] = separation * i;
        }
        HalfDis = separation / 2;

        KEY = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name.ToUpper() + "_" + KEY;
        Position = Scroll.value = PlayerPrefs.GetFloat(KEY, Scroll.value);

        for (int i = 0; i < Positions.Length; i++)
        {
            bool current = Mathf.Abs(Positions[i] - Position) < HalfDis;
            transform.GetChild(i).localScale = current ? _bigScale : _smallScale;

            if (current && i != Current)
            {
                Current = i;
                onFocus?.Invoke(i);
            }
        }
    }

    private void Update()
    {
        if (Input.GetMouseButton(0)) Position = Scroll.value;
        else
        {
            for (int i = 0; i < Positions.Length; i++)
            {
                if (Mathf.Abs(Positions[i] - Position) < HalfDis)
                {
                    Scroll.value = Mathf.Lerp(Scroll.value, Positions[i], 5 * Time.deltaTime);
                }
            }
        }

        for (int i = 0; i < Positions.Length; i++)
        {
            Transform child = transform.GetChild(i);
            bool current = Mathf.Abs(Positions[i] - Position) < HalfDis;

            child.localScale = Vector2.Lerp(child.localScale, current ? _bigScale : _smallScale, 5 * Time.deltaTime);

            if (current && i != Current)
            {
                Current = i;
                onFocus?.Invoke(i);
            }
        }
    }

    private void OnDestroy() => PlayerPrefs.SetFloat(KEY, Scroll.value);
}