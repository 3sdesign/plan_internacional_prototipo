using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using static QuizScriptableObject;

public class SmashGame : GameActivity
{
    private QuizScriptableObject Data = null;
    private int currentQuestian = 0;

    [SerializeField] private GameObject CardPrefab;
    [SerializeField] private Transform Container;
    [SerializeField] private Image FrameRound;
    [SerializeField] private Sprite[] sprites = null;

    [SerializeField] private ReverseCounterPanel counter;
    [SerializeField] private EnergyHUD energyBar = null;
    [SerializeField] private HealthHUD healthBar = null;
    [SerializeField] private ScoreHUD coinPanel = null;
    [SerializeField] private TriviaPopup trivia = null;
    [SerializeField] private FeedbackPopUp Popup;
    private Sprite CorretSprite = null;
    private int[] array = new int[] { 9, 12, 15 };
    private int limitArray = 0;
    private int limitArrayIncorrect = 0;
    private int countCorrect = 0;

    protected override void Start()
    {
        base.Start();

        Data = Session.GetQuizData();
        Container.transform.localScale = Vector2.zero;
        FrameRound.transform.parent.localScale = Vector2.zero;

        healthBar.SetHealth(3, () =>
        {
            //ResetGame();
            energyBar.PlusBar();
            if (energyBar.GetActualValue() < 1)
            {
                Container.transform.DOScale(0, 0.5f).SetEase(Ease.InBack);
                FrameRound.transform.parent.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
                {
                    healthBar.SetHealth(3);
                    RemoveAllChildren(Container);
                    GenerateRound();
                });
            }
        });
        energyBar.Config(5, () =>
         {
             foreach (Transform btn in Container)
             {
                 btn.GetComponent<Button>().interactable = false;
             }
             ShowQuestion();
         });
        Popup.ShowMessage("Presiona rapidamente los elementos que sean diferentes a la imagen del lado derecho.", () =>
        {
            ShowCounter(GenerateRound);
        });
    }

    private void GenerateRound()
    {
        countCorrect = 0;
        CorretSprite = GetRoundSprite();
        FrameRound.sprite = CorretSprite;
        limitArray = array[Random.Range(0, array.Length)];
        limitArrayIncorrect = Random.Range(3, 7);
        for (int i = 0; i < limitArray - limitArrayIncorrect; i++)
        {
            CreateCard(true, CorretSprite);
        }
        for (int i = 0; i < limitArrayIncorrect; i++)
        {
            /*int num = Random.Range(0,7);
            if(num == 0 || num == 1 || num == 2){
                //creamos items
                if (num == 2) {
                    countCorrect++;
                    CreateCard(true, items[2], 2);
                }
                else CreateCard(false, items[num], num);
            }
            else{
                CreateCard(false, GetRoundSprite());
            }*/
            CreateCard(false, GetRoundSprite());
        }

        Container.transform.DOScale(1, 0.5f).SetEase(Ease.OutBack);
        FrameRound.transform.parent.DOScale(1, 0.5f).SetEase(Ease.OutBack);
    }
    private void CreateCard(bool isInteractuable, Sprite sprt, int idItem = -1)
    {
        GameObject Card = Instantiate(CardPrefab, Container);
        Card.transform.Find("Image").GetComponent<Image>().sprite = sprt;
        Card.GetComponent<Button>().onClick.AddListener(() =>
        {
            if (isInteractuable)
            {
                AudioManager.instance.PlaySFX("Lose");
                healthBar.SubtractHealth();
                /*if(idItem == 2)
                {
                    AudioManager.instance.PlaySFX("Lose");
                    print("Is Boomb");
                }*/
            }
            else
            {
                /*if (idItem == 0)
                {
                    print("Is Coim");
                }else if(idItem == 1){
                    print("Is Diamond");
                }*/
                AudioManager.instance.PlaySFX("Star");
                Card.transform.DOScale(1.2f, 0.2f).SetEase(Ease.OutBack).OnComplete(() => Card.transform.DOScale(1f, 0.2f).SetEase(Ease.InBack));
                Card.transform.Find("Image").GetComponent<Image>().sprite = CorretSprite;
                Card.GetComponent<Button>().onClick.RemoveAllListeners();
                Card.GetComponent<Button>().onClick.AddListener(() =>
                {
                    AudioManager.instance.PlaySFX("Lose");
                    healthBar.SubtractHealth();
                });
                countCorrect++;
                if (countCorrect >= limitArrayIncorrect)
                {
                    energyBar.PlusBar();
                    AudioManager.instance.PlaySFX("Answer");
                    if (!healthBar.IsFullHealth()) healthBar.AddHealth();
                    if (energyBar.GetActualValue() < 1)
                    {
                        ResetRound();
                    }
                }
            }
        });
        Sort(Card.transform);
    }
    private void ResetRound()
    {
        Container.transform.DOScale(0, 0.5f).SetEase(Ease.InBack);
        FrameRound.transform.parent.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            RemoveAllChildren(Container);
            GenerateRound();
        });
    }
    private void ResetGame()
    {
        energyBar.ResetEnergyBar();
        foreach (Transform btn in Container)
        {
            btn.GetComponent<Button>().interactable = false;
        }
        Container.transform.DOScale(0, 0.5f).SetEase(Ease.InBack);
        FrameRound.transform.parent.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            healthBar.SetHealth(3);
            RemoveAllChildren(Container);
            Popup.ShowMessage("Presiona rapidamente los elementos que sean diferentes a la imagen del lado derecho.", () =>
            {
                ShowCounter(GenerateRound);
            });
        });
    }
    private void ShowQuestion()
    {
        QuestionData q = Data.questions[currentQuestian];
        trivia.LaunchQuestion(q, currentQuestian + 1, (bool ok) =>
        {
            if (!ok)
            {
                Popup.ShowTryAgain(() =>
                {
                    ShowQuestion();
                });
                return;
            }
            Popup.ShowCorrect(() =>
            {
                coinPanel.PlusPoints(10);
                currentQuestian++;
                if (currentQuestian < Data.questions.Length) { ResetRound(); energyBar.ResetEnergyBar(); healthBar.SetHealth(3); }
                else Popup.ShowFinalPopup(4);
            });
        });
    }
    private Sprite GetRoundSprite()
    {
        Sprite test = sprites[Random.Range(0, sprites.Length)];
        if (test == CorretSprite)
        {
            return GetRoundSprite();
        }
        else
        {
            return test;
        }
    }
    private void Sort(Transform t) => t.SetSiblingIndex(UnityEngine.Random.Range(0, t.parent.childCount + 1));
    private void RemoveAllChildren(Transform t)
    {
        foreach (Transform child in t)
        {
            UnityEngine.Object.Destroy(child.gameObject);
        }
    }

    protected override void ConfigGame()
    {

    }

    protected override void InitGame()
    {

    }

    protected override void OnDie()
    {

    }
}
