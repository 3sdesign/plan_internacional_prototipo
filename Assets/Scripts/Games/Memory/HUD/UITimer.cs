using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class UITimer : MonoBehaviour
{
    [SerializeField] private AudioSource effectSource;
    [SerializeField] private AudioClip timeOver;

    private float CurrentTime = 0;
    private bool Running = false;
    private int TimeSense = 1;
    private Text Label = null;

    public Action OnTimeOver = null;

    private int awareTime = 5;

    protected void Awake() => Label = GetComponentInChildren<Text>();

    public void SetTimer(int time, int awareTime = 5)
    {
        CurrentTime = time;
        this.awareTime = awareTime;
        TimeSense = -1;
        Refresh();
    }

    public void Play() => Running = true;
    public void Pause() { Running = false; isOnAwaring = false; effectSource?.Stop(); }

    private void Update()
    {
        if (Running)
        {
            CurrentTime += Time.deltaTime * TimeSense;

            if (TimeSense == -1 && CurrentTime < 0)
            {
                CurrentTime = 0;
                Running = false;
            }

            RefreshAndCheckAware();

            if (!Running)
            {
                effectSource?.Stop();
                OnTimeOver?.Invoke();
            }
        }
    }

    private void Refresh() => Label.text = string.Format("{0:00}:{1:00}", Mathf.FloorToInt(CurrentTime / 60), CurrentTime % 60);

    float tempAware;
    bool isOnAwaring = false;
    private void RefreshAndCheckAware()
    {
        float second = CurrentTime % 60;
        Label.text = string.Format("{0:00}:{1:00}", Mathf.FloorToInt(CurrentTime / 60), second);

        if (second <= awareTime + 0.5f)
        {
            if (!isOnAwaring)
            {
                if(effectSource != null)
                {
                    effectSource.clip = timeOver;
                    effectSource.Play();
                }

                isOnAwaring = true;
                transform.DOPunchScale(Vector2.one * 0.25f, 0.85f, 0, 0);
                tempAware = awareTime - 0.5f;
                return;
            }

            //...
            if (second < tempAware)
            {
                transform.DOPunchScale(Vector2.one * 0.15f, 0.65f, 0);
                tempAware = tempAware - 1f;
            }

        }
        else
            if (isOnAwaring) isOnAwaring = false;


    }
}
