using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CardsGroup : MonoBehaviour
{
    [SerializeField] private MemoryGameManager MemoryGame = null;
    [SerializeField] private Card cardPrefab;

    private Card firstCard;

    private List<Card> cardList = new List<Card>();

    private GridLayoutGroup layoutGroup;
    private CanvasGroup canvasGroup;

    private void Start()
    {
        cardPrefab.gameObject.SetActive(false);

        layoutGroup = GetComponent<GridLayoutGroup>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private bool _locked;
    public bool Locked
    {
        get => _locked;
        set
        {
            if (_locked == value)
                return;

            _locked = value;

            if (value)
            {
                canvasGroup.interactable = false;
            }
            else
            {
                canvasGroup.interactable = true;
            }
        }
    }

    public void CreateCards(int count, System.Action OnComplete)
    {
        Clear();

        AdjustLayout(count);

        List<Sprite> _items = new List<Sprite>(MemoryGame.ItemsSprites);

        for (int i = 0; i < count; i++)
        {
            int targetIndex = Random.Range(0, _items.Count);
            Sprite target = _items[targetIndex];

            Card pair1 = Instantiate(cardPrefab, transform);
            Card pair2 = Instantiate(cardPrefab, transform);

            pair1.Setup(i, Validate);
            pair2.Setup(i, Validate);

            pair1.SetShowSprite(target);
            pair2.SetShowSprite(target);

            cardList.Add(pair1);
            cardList.Add(pair2);

            _items.RemoveAt(targetIndex);

            pair1.Interactable = false;
            pair2.Interactable = false;
        }
        //
        Shuffle();
        //
        transform.localScale = Vector2.zero;
        this.AfterTime(0.1f, () =>
        {
            transform.DOScale(1, 0.4f).SetEase(Ease.OutBack).OnComplete(() =>
            {
                //PreVisualize();
                OnComplete?.Invoke();
            });
        });

    }

    void AdjustLayout(int count)
    {
        int targetSize = 220;
        if (count < 6)
        {
            targetSize = 300;
            layoutGroup.constraintCount = 2;
        }
        else if (count == 6)
        {
            targetSize = 260;
            layoutGroup.constraintCount = 3;
        }
        else
            layoutGroup.constraintCount = 4;

        //int targetSize = (int)Mathf.Lerp(200, 150,(Mathf.InverseLerp(2, 9, count)));
        layoutGroup.cellSize = new Vector2(targetSize, targetSize);
    }

    public void PreVisualize()
    {
        for (int i = 0; i < cardList.Count; i++)
        {
            int temp = i;
            cardList[temp].Show(() => this.AfterTime(1, () => cardList[temp].Hide()));
        }
    }

    public void ShowCards()
    {
        for (int i = 0; i < cardList.Count; i++)
        {
            int temp = i;
            cardList[temp].Show();
        }
    }

    public void HideCards()
    {
        for (int i = 0; i < cardList.Count; i++)
        {
            int temp = i;
            cardList[temp].Hide();
        }
    }

    void Shuffle()
    {
        for (int i = 0; i < cardList.Count; i++)
        {
            cardList[i].transform.SetSiblingIndex(Random.Range(0, cardList.Count));
        }
    }


    void Validate(Card card)
    {
        AudioManager.instance.PlaySFX("Answer");
        if (firstCard == null)
        {
            card.Show();
            firstCard = card;
            return;
        }

        Card tmp = firstCard;
        firstCard = null;

        if (tmp.target == card.target)
            card.Show(() => OnCorrect(tmp, card));
        else
            card.Show(() => OnIncorrect(tmp, card));
    }

    void OnCorrect(Card firstCard, Card secondCard)
    {
        AudioManager.instance.PlaySFX("Star");

        Debug.Log("Correct");
        MemoryGame.CorrectMatch();
        firstCard.MarkCorrect();
        secondCard.MarkCorrect(MemoryGame.Evaluate);
    }

    void OnIncorrect(Card firstCard, Card secondCard)
    {
        AudioManager.instance.PlaySFX("Lose");

        Debug.Log("Incorrect");
        firstCard.Hide();
        secondCard.Hide();
        MemoryGame.YouFail();
    }

    public void Clear()
    {
        if (cardList.Count == 0)
            return;

        for (int i = 0; i < cardList.Count; i++)
        {
            Destroy(cardList[i].gameObject);
        }

        cardList.Clear();
    }
}
