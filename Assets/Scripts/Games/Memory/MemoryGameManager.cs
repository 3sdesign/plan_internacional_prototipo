using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MemoryGameManager : PlanInternacionalActivity
{
    private int pairs, rounds, corrects;

    [Header("Level Data")]
    [SerializeField] private MemoryDataScriptableObject Data = null;
    private MemoryDataScriptableObject.LevelData _levelData;

    [Header("Graphics")]
    [SerializeField] private Sprite _hiden;
    [SerializeField] private Sprite _checked;
    private List<Sprite> _items = new List<Sprite>();

    public Sprite HidenSprite { get => _hiden; }
    public Sprite CheckedSprite { get => _checked; }
    public List<Sprite> ItemsSprites { get => _items; }

    private int currentQuestian = 0;
    private int currentCorrect = 0;

    [SerializeField] private CardsGroup cardsGroup = null;
    [SerializeField] private TimerHUD timer = null;
    [SerializeField] private EnergyHUD energyBar = null;

    private bool EndQuest = false;
    protected override void Awake()
    {
        base.Awake();
        Session.Friend = 1;
    }
    protected override void Start()
    {
        base.Start();

        UpdateCustomSprite(out Sprite[] NewSprites);
        if (NewSprites != null)
        {
            print(NewSprites.Length);
            for (int i = 0; i < NewSprites.Length; i++)
            {
                _items.Add(NewSprites[i]);
            }
        }
        else _items = Data.GetArt(0);
    }

    void Initialize()
    {
        rounds = 0;
        corrects = 0;
        pairs = _levelData.target;
        energyBar.Config(_levelData.rounds, () =>
         {
             Debug.Log("YOU WIN");
             timer.Pause();
             cardsGroup.Locked = true;
             ShowPopUp("Ganaste", $"Ahora responde las siguientes preguntas.", 0, () => {
                 ShowQuestion();
             }, false,null);
             /*EndQuest = false;
                 ResetValues();
                 ShowMessageAndCounter();
              */
         });
    }
    public void CorrectMatch()
    {
        corrects++;
        if (corrects == _levelData.target)
        {
            timer.Pause();
        }
    }
    public void Evaluate()
    {
        if (corrects == _levelData.target)
        {
            energyBar.PlusBar();
            AddScore(1, 0);
            rounds++;
            if (rounds < _levelData.rounds)
            {
                cardsGroup.Locked = true;
                Utils.AfterTime(this, 1, () => { corrects = 0; SetHealth(_levelData.lives); InitGame(); });
            }
        }
    }
    public void YouFail() => SubtractHealth();
    private void ShowQuestion()
    {
        LaunchQuestion(currentQuestian, (bool ok) =>
        {
            if(ok) currentCorrect++;
            currentQuestian++;
            if (currentQuestian >= 5)
            {
                /*if (!ok)
                {
                    RestartScore();
                    ShowFail("No lo lograste.", ValidateRound);
                    return;
                }
                currentCorrect++;
                ShowCorrectMoney("Lo lograste", $"Respondiste correctamente, muy bien.", ValidateRound);*/
                ValidateRound();
            }
            else Utils.AfterTime(this, 1, ShowQuestion);
        }, currentQuestian < 4 ? false : true);
    }
    private void ValidateRound()
    {   
        if (currentQuestian < 5)
        {
            energyBar.ResetEnergyBar();
            EndQuest = true;
            ValidationRound();
            cardsGroup.Clear();
        }
        else ShowFinalPopup(currentCorrect);
    }
    private void ValidationRound()
    {
        if (EndQuest)
        {
            EndQuest = false;
            ResetValues();
            ShowMessageAndCounter();
        }
        else
        {
            InitGame();
        }
    }
    private void ResetValues()
    {
        rounds = 0;
        corrects = 0;
        cardsGroup.Clear();
        energyBar.ResetEnergyBar();
        SetHealth(_levelData.lives);
        timer.SetTimer(_levelData.time);
    }
    protected override void ConfigGame()
    {
        _levelData = Data.Get(Session.GetLevel());
        Initialize();
        SetHealth(_levelData.lives);
        timer.SetTimer(_levelData.time, () =>
        {
            RestartScore();
            AudioManager.instance.PlaySFX("Lose");
            OnDie();
        });
        timer.Pause();
    }

    protected override void InitGame()
    {
        cardsGroup.Locked = true;
        cardsGroup.CreateCards(pairs, () =>
        {
            cardsGroup.ShowCards();

            this.AfterTime(3, () =>
            {
                cardsGroup.HideCards();
                timer.Play();
                cardsGroup.Locked = false;
            });

        });
    }

    protected override void OnDie()
    {
        AudioManager.instance.PlaySFX("Lose");
        cardsGroup.Locked = true;
        RestartScore();
        timer.Pause();
        // ShowFail("Vuelve a intentarlo.", () => { ResetValues(); ShowMessageAndCounter(); });
        ShowFail(countReset > 1 ? "Ahora responde las siguientes preguntas." : "Vuelve a intentarlo.", () => { ResetValues(); ShowQuestion(); },()=>
        {
            EndQuest = false;
            ResetValues();
            ShowMessageAndCounter();
        });
    }
}
