using UnityEngine;
using System.Collections.Generic;
[CreateAssetMenu(fileName = "MemoryData", menuName = "ScriptableObjects/MemoryData", order = 1)]
public class MemoryDataScriptableObject : ScriptableObject
{
    [System.Serializable]
    public struct LevelData
    {
        public int time;
        public int lives;
        public int target;
        public int rounds;
    }

    [System.Serializable]
    public struct ArtGroup
    {
        public string groupId;
        public List<Sprite> sprites;
    }

    [SerializeField] private LevelData[] levels = null;
    public ArtGroup[] artGroups = new ArtGroup[1];

    public LevelData Get(int level) => levels[Mathf.Clamp(level - 1, 0, levels.Length)];

    public List<Sprite> GetArt(string id)
    {
        for (int i = 0; i < artGroups.Length; i++)
            if (artGroups[i].groupId.Equals(id)) return artGroups[i].sprites;
        return null;
    }

    public List<Sprite> GetArt(int index) => artGroups[index].sprites;
}
