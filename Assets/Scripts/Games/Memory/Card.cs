using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class Card : MonoBehaviour
{
    public Image picture;

    private Sprite _showSprite;
    [SerializeField] private MemoryGameManager myGame = null;
    [HideInInspector] public int target;

    public bool Interactable { get => _button.interactable; set => _button.interactable = value; }

    private Button _button;

    public void Setup(int target, Action<Card> Check)
    {
        gameObject.SetActive(true);
        this.target = target;
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() => Check(this));
        picture.sprite = myGame.HidenSprite;
    }

    public void Show(Action OnComplete = null)
    {
        Interactable = false;
        transform.DOScaleX(0, 0.25f).OnComplete(() =>
        {
            picture.sprite = _showSprite;
            transform.DOScaleX(1, 0.25f).OnComplete(() => OnComplete?.Invoke());
        });
    }

    public void SetShowSprite(Sprite showSprite) => _showSprite = showSprite;

    public void Hide()
    {
        transform.DOScaleX(0, 0.25f).OnComplete(() =>
        {
            picture.sprite = myGame.HidenSprite;
            transform.DOScaleX(1, 0.25f);
            Interactable = true;
        });
    }

    public void MarkCorrect(Action OnComplete = null)
    {
        transform.DOScale(1.12f, 0.16f).OnComplete(() =>
        {
            picture.sprite = myGame.CheckedSprite;
            transform.DOScale(1, 0.2f).OnComplete(() => OnComplete?.Invoke());
        });

    }
}
