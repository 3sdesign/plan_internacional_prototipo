﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Game
{
    public class Jetpack : PlanInternacionalActivity
    {
        [Header("JetpackData")]
        [SerializeField] private JetpackDataScriptableObject jetpackData = null;

        private int currentQuestian = 0;
        private int currentCorrect = 0;

        [Space, Header("Game")]
        [SerializeField] private Rigidbody2D Player = null;
        [SerializeField] private Slider Slider = null;
        [SerializeField] private Transform HandPos = null;
        [SerializeField] private Animator Anima = null;
        private Transform transf_player = null;

        [Header("Objects")]
        [SerializeField] private MeshRenderer loopBG = null;

        [Header("Items")]
        [SerializeField] private Transform[] spawner = null;
        [SerializeField] private Transform Container = null;
        [SerializeField] private GameObject[] ItemsPrefab = null;
        [SerializeField] private Sprite[] GameSprites = null;
        [SerializeField] private Image[] MyImages = null;
        private int spawnerIndex, spawnerIndex2 = 0;

        [SerializeField] private Text DiamondsLabel = null;
        private int _diamonds = 0;

        [Header("HUD")]
        [SerializeField] private TimerHUD timer = null;
        [SerializeField] private EnergyHUD energyBar = null;

        private float JumpForce, SpawnTime = 0;
        private int percentDiamont, percentBomb, percentPowerUp, percentCoin, percentHealth, percentTime = 0;
        private int shuffleSum = 0;
        private int powerupShuffleSum = 0;

        public Color Brown = new Color32(72, 27, 16, 255);
        public Color Brownie = new Color32(62, 32, 16, 180);

        private bool StopMoving = true;

        private int diamonsToWin, myStartingTime;

        protected override void Awake()
        {
            base.Awake();
            Session.Friend = 0;
            transf_player = Player.transform;
        }
        protected override void Start()
        {
            base.Start();

            ConfigDifficult();

            Anima.SetBool("isPlay", false);
            Slider.interactable = false;

            timer.SetTimer(myStartingTime, () =>
            {
                RestartScore();
                OnDie();
            });

            UpdateCustomSprite(out Sprite[] NewSprites);
            if (NewSprites != null)
            {
                GameSprites = NewSprites;
            }

            for (int i = 0; i < MyImages.Length; i++)
            {
                MyImages[i].sprite = GameSprites[i];
            }
        }
        public void ConfigDifficult()
        {
            var level = jetpackData.Get(Session.GetLevel());

            diamonsToWin = level.target;
            myStartingTime = level.time;
            JumpForce = 10 * level.velocity;
            SpawnTime = level.spawn_time;

            percentDiamont = level.diamondsValue;
            percentBomb = level.obstaclesValue;
            percentPowerUp = level.poweupsValue;
            shuffleSum = percentDiamont + percentBomb + percentPowerUp;

            percentCoin = level.coinsValue;
            percentHealth = level.heartsValue;
            percentTime = level.clocksValue;
            powerupShuffleSum = percentCoin + percentHealth + percentTime;

            energyBar.Config(diamonsToWin, () =>
            {
                timer.Pause();
            });
        }

        protected override void Update()
        {
            base.Update();

            if (StopMoving == false)
                transf_player.position = new Vector2(HandPos.transform.position.x, transf_player.position.y);
        }
        private void FixedUpdate()
        {
            loopBG.materials[0].mainTextureOffset = new Vector2(0, (transf_player.position.y * 0.025f) - 0.45f); // -0.45f = offset hardcodeado
        }
        private void InvokeItems() => Invoke(nameof(GenerateItems), SpawnTime);

        private void GenerateItems()
        {
            int item1 = GetItem();
            int item2 = GetItem();

            while (item1 == 0 && item2 == 0) item2 = GetItem();
            while (item1 == 3 && item2 == 3) item2 = GetItem();
            while (item1 == 4 && item2 == 4) item2 = GetItem();
            if (Session.GetLevel() == 0) while (item1 == 1 && item2 == 1) item2 = GetItem();

            spawnerIndex = Random.Range(0, spawner.Length);
            spawnerIndex2 = Random.Range(0, spawner.Length);
            while (spawnerIndex2 == spawnerIndex) spawnerIndex2 = Random.Range(0, spawner.Length);

            GenerateItem(item1, spawnerIndex);
            GenerateItem(item2, spawnerIndex2);

            if (StopMoving == false) InvokeItems();
        }

        private int GetItem()
        {
            int item = GetRandom(shuffleSum, percentDiamont, percentBomb, percentPowerUp);

            if (item == 2)
            {
                item += GetRandom(powerupShuffleSum, percentCoin, percentHealth, percentTime);

                if (IsFullHealth && item == 3) item = 2;
            }

            return item;
        }

        private int GetRandom(int total, params float[] percents)
        {
            float cumulative = 0;
            float rnd = Random.Range(0, total + 1);

            for (int i = 0; i < percents.Length; i++)
            {
                cumulative += percents[i];
                if (rnd <= cumulative) return i;
            }

            return percents.Length - 1;
        }

        private void GenerateItem(int test, int spawnIndex)
        {
            GameObject item = Instantiate(ItemsPrefab[test], Container);
            item.transform.position = new Vector2(spawner[spawnIndex].transform.position.x, spawner[spawnIndex].transform.position.y);

            if (test == 0)
            {
                item.GetComponent<Jetpack_item>().SetData(() =>
                {
                    AudioManager.instance.PlaySFX("Answer");
                    _diamonds++;
                    energyBar.PlusBar();
                    DiamondsLabel.text = _diamonds.ToString();
                    DiamondsLabel.transform.DOScale(1.5f, 0.3f).SetEase(Ease.OutBack).OnComplete(() =>
                    {
                        DiamondsLabel.transform.DOScale(1f, 0.2f).SetEase(Ease.InBack);
                    });
                    if (_diamonds >= diamonsToWin)
                    {
                        timer.Pause();
                        Player.velocity = Vector2.zero;
                        StopMoving = true;
                        Anima.SetBool("isPlay", false);
                        Slider.interactable = false;

                        ShowPopUp("Ganaste", $"Ahora responde las siguientes preguntas.", 0, () =>
                        {
                            ShowQuestion();
                        }, false, null);
                        /*()=>
                        {
                            RestartScore();
                            HideGame();
                        }*/
                    }
                });
            }
            else if (test == 1)
            {
                item.GetComponent<Jetpack_item>().SetData(() =>
                {
                    AudioManager.instance.PlaySFX("Lose");
                    SubtractHealth();
                });
            }
            else if (test == 2)
            {
                item.GetComponent<Jetpack_item>().SetData(() =>
                {
                    PlusScore();
                });
            }
            else if (test == 3)
            {
                item.GetComponent<Jetpack_item>().SetData(() =>
                {
                    AudioManager.instance.PlaySFX("Question");
                    AddHealth();
                });
            }
            else if (test == 4)
            {
                item.GetComponent<Jetpack_item>().SetData(() =>
                {
                    AudioManager.instance.PlaySFX("Star");
                    timer.PlusTime(5);
                });
            }
        }

        private void ShowQuestion()
        {
            LaunchQuestion(currentQuestian, (bool ok) =>
            {
                if (ok) currentCorrect++;
                currentQuestian++;
                if (currentQuestian >= 5)
                {
                    /*if (!ok)
                      {
                          RestartScore();
                          ShowFail("No lo lograste.", () => ValidateRound());
                          return;
                      }
                      currentCorrect++;
                      ShowCorrectMoney("Lo lograste", $"Respondiste correctamente, muy bien.", () => ValidateRound());*/
                    ValidateRound();
                }
                else Utils.AfterTime(this, 1, ShowQuestion);
            }, currentQuestian < 4 ? false : true);
        }
        private void ValidateRound()
        {
            if (currentQuestian < 5)
            {
                HideGame();
            }
            else ShowFinalPopup(currentCorrect);
        }
        private int GetRandomItemByPercent(float diamont, float bomb, float coin, float health)
        {
            float random = Random.Range(0.0f, 1.0f);

            int testValue = 0;

            if (random < diamont) testValue = 0;
            else if (random > diamont && random < bomb) testValue = 1;
            else if (random > bomb && random < coin) testValue = 2;
            else if (random > coin && random < health) testValue = 3;
            else if (random > health) testValue = 4;

            return testValue;
        }
        private void HideGame()
        {
            StopMoving = true;
            Anima.SetBool("isPlay", false);
            Slider.interactable = false;
            Slider.value = 0;
            RemoveAllChildren(Container);
            Player.velocity = new Vector2(0, 0);
            transf_player.DOScale(0, 1f).SetEase(Ease.InBack).OnComplete(() =>
            {
                transf_player.position = new Vector2(0, -6.50f);
                Camera.main.transform.position = new Vector3(0, 0, -10f);
                ResetValues();
                ShowMessageAndCounter();
            });
        }
        private void InitRound()
        {
            timer.SetTimer(myStartingTime);
            transf_player.DOScale(0.17f, 0.5f).SetEase(Ease.OutBack).OnComplete(() =>
            { //0.17f
                timer.Play();
                StopMoving = false;
                Anima.SetBool("isPlay", true);
                Slider.interactable = true;
                Player.velocity = new Vector2(0, JumpForce);
                InvokeItems();
            });
        }
        private void ResetValues()
        {
            SetHealth(3);
            _diamonds = 0;
            DiamondsLabel.text = _diamonds.ToString();
            energyBar.ResetEnergyBar();
            timer.SetTimer(myStartingTime);
        }
        private void PlusScore() => AddScore(1, 0);
        public void Sort(Transform t) => t.SetSiblingIndex(Random.Range(0, t.parent.childCount + 1));
        public void RemoveAllChildren(Transform t)
        {
            foreach (Transform child in t)
            {
                Destroy(child.gameObject);
            }
        }

        protected override void ConfigGame()
        {
            ConfigDifficult();
            roundInstruction = "Todos podemos sentirnos enfadados en algún momento pero podemos manejar nuestras reacciones. No dejes que la carita molesta te atrape.";
        }

        protected override void InitGame()
        {
            InitRound();
        }

        protected override void OnDie()
        {
            timer.Pause();
            Player.velocity = Vector2.zero;
            StopMoving = true;
            Anima.SetBool("isPlay", false);
            Slider.interactable = false;
            //ShowTryAgain(ShowQuestion);
            ShowFail(countReset > 1 ? "Ahora responde las siguientes preguntas." : "Vuelve a intentarlo.", ShowQuestion, () =>
            {
                RestartScore();
                HideGame();
            });
            /*RestartScore();
            ShowTryAgain(() => HideGame());*/
        }
    }
}
