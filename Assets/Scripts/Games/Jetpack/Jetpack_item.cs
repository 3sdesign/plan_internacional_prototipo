﻿using System;
using UnityEngine;

public class Jetpack_item : MonoBehaviour
{
    [SerializeField] private Transform Player = null;
    private Action OnCorrect = null;
    private bool isactive = false;

    void Update()
    {
        if (Vector2.Distance(transform.position, Player.position) < 1f && isactive == true)
        {
            OnCorrect?.Invoke();
            isactive = false;
            transform.localScale = Vector2.zero;
        }
        else if (transform.position.y <= Player.position.y - 10f && isactive == true)
        {
            gameObject.SetActive(false);
        }
    }

    public void SetData(Action _onCorrect)
    {
        isactive = true;
        OnCorrect = _onCorrect;
    }
}
