using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class TriviaController : MonoBehaviour
{
    [Header("Data")]
    [SerializeField] private QuizScriptableObject Data = null;
    private int _currentQuestion = -1;

    [Header("HUD")]
    [SerializeField] private RectTransform QuestionRt = null;
    [SerializeField] private Text QuestionTitle = null;
    [SerializeField] private Text QuestionText = null;
    [SerializeField] private Answer[] Answers = null;

    [Header("Popup")]
    [SerializeField] private Popup Pop = null;

    private int maxQuestions;
    private bool isCorLastQuest;
    private Action<bool> onCorrect = null;
    private Action onIncorrect = null;

    public void Config(int _maxQuestions = 10, bool continous = false, Action<bool> _onCorrect = null, Action _onIncorrect = null)
    {
        QuestionRt.localScale = Vector2.zero;
        QuestionRt.anchoredPosition = Vector2.zero;
        Pop.gameObject.SetActive(false);

        for (int i = 0; i < Answers.Length; i++)
        {
            Answers[i].ToDown();
        }


        Popup.onReset = ShowQuestion;

        if (!continous)
        {
            Popup.onNext = () =>
            {
                if(_onIncorrect == null)
                {
                    onCorrect?.Invoke(maxQuestions == 0);
                    Close();
                }
                else
                {
                    if (isCorLastQuest == true)
                    {
                        onCorrect?.Invoke(maxQuestions == 0);
                        Close();
                    }
                    else
                    {
                        onIncorrect?.Invoke();
                        Close();
                    }
                }
            };
        }
        else
        {

            Popup.onNext = () =>
            {
                if (_onIncorrect == null)
                {
                    onCorrect?.Invoke(maxQuestions == 0);
                    NextQuestion();
                }
                else
                {
                    if (isCorLastQuest == true)
                    {
                        onCorrect?.Invoke(maxQuestions == 0);
                        NextQuestion();
                    }
                    else
                    {
                        onIncorrect?.Invoke();
                        NextQuestion();
                    }
                } 
            };
        }

        maxQuestions = _maxQuestions;
        onCorrect = _onCorrect;
        onIncorrect = _onIncorrect;
    }
    public void NextQuestion()
    {
        _currentQuestion++;
        ShowQuestion();
    }

    public void Close(Action _onComplete = null, float delay = 1) => QuestionRt.DOScale(0, 0.5f).SetEase(Ease.InBack).SetDelay(delay).OnComplete(() => _onComplete?.Invoke());

    private void ShowQuestion()
    {
        if (_currentQuestion < Data.questions.Length)
        {
            QuizScriptableObject.QuestionData q = Data.questions[_currentQuestion];
            QuestionTitle.text = "Pregunta " + (_currentQuestion + 1);
            QuestionText.text = q.text;

            for (int i = 0; i < q.answers.Length; i++)
            {
                Answers[i].SetText(q.answers[i].text);

                Answers[i].onSelect = (Answer answer) =>
                {
                    AudioManager.instance.PlaySFX("Star");
                    DisableAnswers();

                    ShowResults(0, () =>
                    {
                        if (answer == Answers[0])
                        {
                            isCorLastQuest = true;
                            Close(() =>
                            {
                                QuestionRt.anchoredPosition = Vector2.zero;

                                for (int i = 0; i < Answers.Length; i++)
                                {
                                    int temp = i;
                                    Answers[i].transform.DOScale(0, 0.25f).SetEase(Ease.InBack).OnComplete(() =>
                                    {
                                        Answers[temp].ToDown();
                                    });
                                }
                            });

                            Invoke(nameof(OnCorrect), 1);
                        }
                        else
                        {
                            isCorLastQuest = false;
                            if (onIncorrect == null)
                            {
                                Pop.Show(false);
                            }
                            else
                            {
                                Close(() =>
                                {
                                    QuestionRt.anchoredPosition = Vector2.zero;

                                    for (int i = 0; i < Answers.Length; i++)
                                    {
                                        int temp = i;
                                        Answers[i].transform.DOScale(0, 0.25f).SetEase(Ease.InBack).OnComplete(() =>
                                        {
                                            Answers[temp].ToDown();
                                        });
                                    }
                                });
                                Pop.Show(false,0,false);
                            }
                        }
                    });
                };
            }

            QuestionRt.DOScale(1, 0.5f).SetEase(Ease.OutBack).SetDelay(1.0f).OnStart(() => AudioManager.instance.PlaySFX("Question")).OnComplete(() =>
                QuestionRt.DOAnchorPos(new Vector2(0, 400), 0.5f).SetEase(Ease.OutBack).SetDelay(0.5f).OnComplete(() =>
                {
                    MoveAnswers(0, () =>
                    {
                        EnableAnswers();
                    });
                }));
        }
        else
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }

    private void OnCorrect()
    {
        Pop.Show(true, 10);

        maxQuestions--;
        //onCorrect?.Invoke(maxQuestions == 0);
    }

    private void MoveAnswers(int n, Action onComplete)
    {
        if (n < Answers.Length)
        {
            AudioManager.instance.PlaySFX("Answer");
            Answers[n].MoveToOrigin(() => MoveAnswers(n + 1, onComplete));
        }
        else onComplete();
    }

    private void EnableAnswers()
    {
        SetAnswers(true);
    }

    private void DisableAnswers()
    {
        SetAnswers(false);
    }

    private void SetAnswers(bool x)
    {
        for (int j = 0; j < Answers.Length; j++)
        {
            Answers[j].SetInteractable(x);
        }
    }

    private void ShowResults(int n, Action onComplete)
    {
        if (n < Answers.Length)
        {
            Answers[n].ShowResult(() => ShowResults(n + 1, onComplete));
        }
        else onComplete();
    }

    private void ResetAnswers()
    {
        for (int j = 0; j < Answers.Length; j++)
        {
            Answers[j].ReActivate();
        }
    }
}
