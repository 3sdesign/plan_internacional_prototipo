using UnityEngine;

[CreateAssetMenu(fileName = "JetpackData", menuName = "ScriptableObjects/JetpackData", order = 1)]
public class JetpackDataScriptableObject : ScriptableObject
{
    [System.Serializable]
    public struct LevelData
    {
        public string name;

        public int time;
        public float velocity;        

        [Header("Items")]
        public int target;
        public float spawn_time;

        [Header("Shuffle")]
        public int diamondsValue;
        public int obstaclesValue;
        public int poweupsValue;

        [Header("Powerup Shuffle")]
        public int coinsValue;
        public int heartsValue;
        public int clocksValue;
    }

    [SerializeField] private LevelData[] levels = null;

    public LevelData Get(int level) => levels[Mathf.Clamp(level - 1, 0, levels.Length)];
}