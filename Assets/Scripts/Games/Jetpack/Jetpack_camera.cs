﻿using UnityEngine;

public class Jetpack_camera : MonoBehaviour
{
    public Transform playerpos;
    public float offsetY = 0;

    void Update()
    {
        if(playerpos.position.y + offsetY >= 0) transform.position = new Vector3(0, playerpos.position.y + offsetY, transform.position.z);
    }
}
