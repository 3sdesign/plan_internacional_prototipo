using UnityEngine;

[CreateAssetMenu(fileName = "LudoData", menuName = "ScriptableObjects/LudoData", order = 1)]
public class LudoDataScriptableObject : ScriptableObject
{
    [System.Serializable]
    public struct LevelData
    {
        public int TotalSpecialBlocks;
        public int CustomBlocks;
    }

    [SerializeField] private LevelData[] levels = null;

    public LevelData Get(int level) => levels[Mathf.Clamp(level - 1, 0, levels.Length)];
}
