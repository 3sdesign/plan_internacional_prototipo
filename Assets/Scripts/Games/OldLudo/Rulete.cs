using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Rulete : MonoBehaviour
{
    [Header("Roulette")]
    [SerializeField] private AnimationCurve Curve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    [SerializeField, Range(2, 16)] private int Sides = 8;
    [SerializeField] private AudioClip SpinSound = null;
    [SerializeField] private Transform Pivot = null;
    private readonly float CompleteSpin = 360.0f;
    [SerializeField] private float myscale = 1;

    [HideInInspector] public int Correct, myLastMove = 0;

    public Button SpinButton = null;
    public System.Action OnComplete = null;

    void Start()
    {
        transform.localScale = Vector2.zero;
        SpinButton.interactable = true;
        SpinButton.onClick.AddListener(() =>
        {
            AudioManager.instance.PlaySFX(SpinSound.name);
            SpinButton.interactable = false;
            Animate();
        });
    }

    private IEnumerator Anim(float angle, float gap, System.Action onComplete)
    {
        int totalTime = 5;

        yield return null;
        float currentTime = 0;
        while (currentTime <= totalTime)
        {
            yield return null;
            currentTime += Time.deltaTime;
            float t = currentTime / totalTime;
            float currentAngle = Mathf.Lerp(gap, angle, Curve.Evaluate(t));
            Pivot.localRotation = Quaternion.Euler(0.0f, 0.0f, currentAngle);
        }
        onComplete?.Invoke();
    }

    private void Animate()
    {
        float unitaryAngle = CompleteSpin / Sides;
        float resultAngle = unitaryAngle * Correct + unitaryAngle / 2;
        float spinAngle = CompleteSpin * 5 + resultAngle;

        StopAllCoroutines();
        StartCoroutine(Anim(-spinAngle, Pivot.localRotation.eulerAngles.z, () =>
        {
            transform.DOScale(0.0f, 0.8f).OnComplete(() =>
            {
                myLastMove = Correct + 1;
                OnComplete?.Invoke();
            }).SetEase(Ease.InBack);
        }));
    }

    public void ShowRuleta(int value = -1)
    {
        Correct = value == -1 ? Random.Range(0, Sides) : value;
        this.transform.DOScale(myscale, 0.8f).SetDelay(0.3f).SetEase(Ease.OutBack);
        this.GetComponent<Rulete>().SpinButton.interactable = true;
    }
}
