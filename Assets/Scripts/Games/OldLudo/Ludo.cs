using DG.Tweening;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using static QuizScriptableObject;
using Random = UnityEngine.Random;

public class Ludo : GameActivity
{
    private QuizScriptableObject Data = null;

    [Header("Data"), SerializeField] private LudoDataScriptableObject ludoData = null;

    [Space, Header("Elements")]
    [Space, SerializeField] private Rulete ruleta = null;
    [Space, SerializeField] private TriviaPopup triviaPop = null;
    [Space, SerializeField] private Sprite[] casillasActivablesS = null;
    [Space, SerializeField] private Transform[] Paths = null;

    [Header("Player")]
    [Space, SerializeField] private RectTransform Character = null;

    private GameObject[] imagenCasillas = null;

    private int[] casillasActivables, nPreguntas = null;
    private int[] casillasPreguntasArray;

    private AudioManager _audioManager = null;

    private LudoDataScriptableObject.LevelData level;

    [HideInInspector] public int nPath = 0;

    private int _currentposition, _typeCasilla, _currentQuestian, _goodCasilla, _badCasilla, _questionCasilla = 0;

    private bool _canAction = true;

    protected override void ConfigGame()
    {
        Data = Session.GetQuizData();
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();

        casillasPreguntasArray = new int[Data.questions.Length];

        nPath = Random.Range(0, Paths.Length - 1);
        level = ludoData.Get(Session.GetLevel());

        imagenCasillas = new GameObject[30 - 1]; //level.path_lenght - 1

        switch (nPath)
        {
            default:
                print("somethig is wrong, the paths are not been detected");
                return;
            case 0:
                StartingCasillasN(30 - 1, 21);//level.path_lenght - 1
                Paths[0].gameObject.SetActive(true);
                break;
            case 1:
                StartingCasillasN(30 - 1, 21);//level.path_lenght - 1
                Paths[1].gameObject.SetActive(true);
                break;
            case 2:
                StartingCasillasN(30 - 1, 21);//level.path_lenght - 1
                Paths[2].gameObject.SetActive(true);
                break;
        }

        LoadingQuestions(() => Shuffle(nPreguntas));

        for (int i = 0; i < 30 - 1; i++)//level.path_lenght - 1
        {
            Paths[nPath].GetChild(i).gameObject.SetActive(true);

            imagenCasillas[i] = Paths[nPath].GetChild(i).gameObject;

            if (i + 1 == 30 - 1) imagenCasillas[i].GetComponent<Image>().sprite = casillasActivablesS[5];//level.path_lenght - 1

            for (int j = 0; j < casillasActivables.Length; j++)
            {
                if (i == casillasActivables[j])
                {
                    _typeCasilla = Random.Range(0, 4);

                    if (i > casillasActivables.Length - 6 && _questionCasilla != Data.questions.Length) _typeCasilla = 0;
                    else
                    {
                        if (_questionCasilla > Data.questions.Length - 1) _typeCasilla = Random.Range(1, 4);
                        if (_goodCasilla > 30 - 1) _typeCasilla = 1;//level.path_lenght - 1
                        if (_badCasilla > 30 - 1) _typeCasilla = Random.Range(2, 4);//level.path_lenght - 1
                    }

                    if (_typeCasilla == 0) _questionCasilla += 1;
                    else if (_typeCasilla == 1) _badCasilla += 1;
                    else _goodCasilla += 1;

                    switch (_typeCasilla)
                    {
                        case 0:
                            imagenCasillas[i].GetComponent<Image>().sprite = casillasActivablesS[0];
                            GetCasillaPregunta(i);
                            break;
                        case 1:
                            imagenCasillas[i].GetComponent<Image>().sprite = casillasActivablesS[1];
                            break;
                        case 2:
                            imagenCasillas[i].GetComponent<Image>().sprite = casillasActivablesS[2];
                            break;
                        case 3:
                            imagenCasillas[i].GetComponent<Image>().sprite = casillasActivablesS[3];
                            break;
                    }
                }
            }
        }
    }

    protected override void InitGame()
    {
        triviaPop.transform.localScale = Vector3.one;

        Character.DOScale(new Vector2(1, 0.6f), 0.8f).SetEase(Ease.OutBack);
        ruleta.ShowRuleta();
        Character.position = Paths[nPath].transform.GetChild(0).transform.position;
    }

    protected override void OnDie()
    {
        RestartScore();

        Vector2 target = GetChildPosition(0);

        float maxY = Mathf.Max(Character.anchoredPosition.y, target.y);

        Character.DOAnchorPosY(maxY + 50, 0.25f).SetDelay(0.5f).OnComplete(() => Character.DOAnchorPosY(target.y, 0.25f));
        Character.DOAnchorPosX(target.x, 0.5f).SetDelay(0.5f).OnComplete(() =>
        {
            LoadingQuestions(() => AddHealth(3));
            _currentposition = 0;
            ruleta.ShowRuleta();
        });
    }

    private void GetCasillaPregunta(int idCasilla)
    {
        for (int j = 0; j < Data.questions.Length; j++)
            if (casillasPreguntasArray[j] == 0)
            {
                casillasPreguntasArray[j] = idCasilla;
                return;
            }
    }
    private int GetRandomCasilla(int limit)
    {
        int casilla = Random.Range(1, limit - 1);
        for (int i = 0; i < casillasActivables.Length; i++) if (casilla == casillasActivables[i]) return GetRandomCasilla(limit);
        return casilla;
    }
    private void StartingCasillasN(int limit, int cantidadCasillasA)
    {
        int[] casillaBackUp = new int[cantidadCasillasA];
        casillasActivables = new int[cantidadCasillasA];

        for (int i = 0; i < cantidadCasillasA; i++)
        {
            casillasActivables[i] = GetRandomCasilla(limit);
            casillaBackUp[i] = casillasActivables[i];

            if (casillasActivables[i] == 0) casillasActivables[i] = Random.Range(1, limit);
            if (casillasActivables[i] == limit + 1) casillasActivables[i] = Random.Range(1, limit - 1);
        }
    }
    private Vector2 GetChildPosition(int index)
    {
        RectTransform rt = Paths[nPath].GetChild(index) as RectTransform;
        return rt.anchoredPosition;
    }
    private void Shuffle(int[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            int rnd = Random.Range(0, array.Length);
            int temp = array[rnd];
            array[rnd] = array[i];
            array[i] = temp;
        }
    }
    public void Launch(int dice) => Move(dice, 1);
    private void Action()
    {
        for (int j = 0; j < casillasActivables.Length; j++)
        {
            if (imagenCasillas[_currentposition].GetComponent<Image>().sprite == casillasActivablesS[0])
            {
                //morado
                ShowMessage("Caiste en una casilla de pregunta, responde correctamente para ganar monedas y no retoceder " + ruleta.myLastMove + " casillas", () =>
                {
                    _typeCasilla = 0;
                    NextQuestion();
                });
                return;
            }
            else if (imagenCasillas[_currentposition].GetComponent<Image>().sprite == casillasActivablesS[1])
            {
                //rojo
                ShowFail("Caiste en una casilla mala, retrocedes 2 casillas", () =>
                {
                    Move(2, -1);
                    imagenCasillas[_currentposition].GetComponent<Image>().sprite = casillasActivablesS[4];
                });
                return;
            }
            else if (imagenCasillas[_currentposition].GetComponent<Image>().sprite == casillasActivablesS[2])
            {
                //verde
                ShowMessage("Caiste en una casilla buena, avanzas 3 casillas", () =>
                {
                    Move(3, 1);
                    imagenCasillas[_currentposition].GetComponent<Image>().sprite = casillasActivablesS[4];
                });
                return;
            }
            else if (imagenCasillas[_currentposition].GetComponent<Image>().sprite == casillasActivablesS[3])
            {
                //coins
                CompleteRound();
                AddScore(3, 0.7f, () => ShowCorrectMoney("Suerte!", "Has caido en una casilla de monedas, ahora tu monedas aumentan en 3", () => ruleta.ShowRuleta()));
                return;
            }
            else ruleta.ShowRuleta();
        }
    }

    private void Move(int steps, int sense)
    {
        if (_currentposition == 30 - 2)//level.path_lenght - 2
        {
            ShowFinalPopup(4);
            return;
        }

        if (steps < 1)
        {
            if (_canAction == true) Action();
            else
            {
                _canAction = true;
                ruleta.ShowRuleta();
            }
            return;
        }

        int next = _currentposition + sense;

        if (_currentposition < 0 || next < 0)
        {
            ruleta.ShowRuleta();
            return;
        }

        Vector2 target = GetChildPosition(next);

        float maxY = Mathf.Max(Character.anchoredPosition.y, target.y);

        Character.DOAnchorPosY(maxY + 50, 0.25f).SetDelay(0.5f).OnComplete(() => Character.DOAnchorPosY(target.y, 0.25f));
        Character.DOAnchorPosX(target.x, 0.5f).SetDelay(0.5f).OnComplete(() =>
        {
            _audioManager.PlaySFX("Answer");
            _currentposition = next;
            Move(steps - 1, sense);
        });
    }
    private void NextQuestion()
    {
        Character.anchoredPosition = GetChildPosition(_currentposition);

        for (int i = 0; i < casillasPreguntasArray.Length; i++) if (_currentposition == casillasPreguntasArray[i]) _currentQuestian = nPreguntas[i];

        QuestionData q = Data.questions[_currentQuestian - 1];

        triviaPop.LaunchQuestion(q, _currentQuestian, (bool ok) =>
        {
            if (!ok)
            {
                ShowFail("Mas suerte la proxima", () =>
                {
                    _canAction = false;
                    SubtractHealth();

                    if (_typeCasilla == 0 && GetHealthValue > 1) Move(ruleta.myLastMove, -1);
                    else
                    {
                        _canAction = true;
                        ruleta.ShowRuleta();
                    }
                });
            }
            else
            {
                _canAction = true;
                ShowCorrect(() =>
                {
                    AddScore(5, 0.7f);
                    ruleta.ShowRuleta();
                });
            }
        });
    }
    private void LoadingQuestions(Action OnComplete)
    {
        nPreguntas = new int[Data.questions.Length];

        for (int i = 0; i < Data.questions.Length; i++)
        {
            nPreguntas[i] += 1;
            if (i == 0) nPreguntas[i] = 1;
            else nPreguntas[i] = nPreguntas[i - 1] + 1;
        }
        Utils.AfterTime(this, 1, OnComplete);
    }
}
