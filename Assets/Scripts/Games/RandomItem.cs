using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(SpriteRenderer))]
public class RandomItem : MonoBehaviour
{
    private int _current = 0;
    [SerializeField] private int _laps = 10;
    [SerializeField] private float _duration = 5;
    private SpriteRenderer _spriteRenderer = null;
    [SerializeField] private Sprite[] _sprites = null;
    private int rollTarget = 0;

    private void Awake() => _spriteRenderer = GetComponent<SpriteRenderer>();

    public void Shuffle() => Shuffle(null);
    public void Shuffle(Action onComplete) => Select(Random.Range(0, _sprites.Length), onComplete);

    public void Select(int target) => Select(target, null);
    public void Select(int target, Action onComplete) => StartCoroutine(Animate(target, onComplete));
    public int SetRollTarget => rollTarget;
    private IEnumerator Animate(int target, Action onComplete)
    {
        AudioManager.instance.PlaySFX("Point");
        rollTarget = target;
        int length = _sprites.Length * _laps + target - _current;
        WaitForSeconds waitForSeconds = new WaitForSeconds(_duration / length);

        for (int i = 0; i < length; i++)
        {
            _current++;
            if (_current >= _sprites.Length) _current = 0;

            _spriteRenderer.sprite = _sprites[_current];

            yield return waitForSeconds;
        }

        yield return new WaitForSeconds(1f);

        onComplete?.Invoke();
    }
}
