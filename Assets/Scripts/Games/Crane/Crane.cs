using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game
{
    public class Crane : PlanInternacionalActivity
    {
        [SerializeField] private CraneDataScriptableObject craneData = null;
        [SerializeField] private AudioClip speedSong = null;
        private Tween Reference1, Reference2;
        private int currentQuestian = 0;
        private int currentCorrect = 0;

        [Header("Sprites")]
        [SerializeField] private Sprite[] sprites = null;
        [SerializeField] private Sprite ClockSpecial = null;
        [SerializeField] private Sprite SpeedSpecial = null;

        [Header("Fichas")]
        [SerializeField] private GameObject FichaPrefab = null;
        [SerializeField] private Transform ContainerFichas = null;
        [SerializeField] private Transform ContainerBlock = null;
        private Transform[] FichaPos = null;
        private Transform[] BlockPos = null;
        private Transform tempParentFicha = null;
        private RectTransform tempFicha = null;
        private Sprite CorrectSprite = null;
        private int quantityCorrect = 2;
        private int quantityIncorrect = 6;

        [Header("Display")]
        [SerializeField] private Image ImageOfWord = null;

        [Header("Components")]
        [SerializeField] private RectTransform ContainerImage = null;
        [SerializeField] private RectTransform Machine = null;
        [SerializeField] private RectTransform HookMachine = null;
        [SerializeField] private Button Button = null;
        [SerializeField] private Joystick joystick = null;

        [Header("HUD")]
        [SerializeField] private TimerHUD Timer = null;
        [SerializeField] private EnergyHUD energyBar = null;

        [Header("Variables")]
        [SerializeField] private bool IsFichaSelected = false;
        private int CountToWin = 0;
        private float speed = 0, stopspeed = 0, movespeed = 0, _shuffleTimer = 0, _speedupTimer = 0, speedupSeconds, speedUpMultiplicator;
        private bool IsPlay = false, IsSpeedUp = false, IsReset = false;
        private int _time = 30, roundsQuantity = 3, correctTarget = 2, roundTargets = 6, itemClockpercent, itemSpeedPercent, _shuffletime = 10, ClockValue;

        [Header("Adjust Container")]
        [SerializeField] private Canvas canvasRef;

        private float containerXPos, limitDistance;
        private bool EndQuest = false;

        // Movement
        private float _delta = 0;
        private const float THRESHOLD = 0.1f;
        private Vector2 _leftLimit = new Vector2(-570, 300);
        private Vector2 _rightLimit = new Vector2(550, 300);

        protected override void Awake()
        {
            base.Awake();
            Session.Friend = 1;
            ImageOfWord.transform.localScale = Vector3.zero;
            ContainerImage.localScale = Vector3.zero;
            Button.onClick.RemoveAllListeners();
            Button.onClick.AddListener(() => GoToFicha());
            RectTransform containerRect = ContainerImage.parent.GetComponent<RectTransform>();
            containerXPos = containerRect.anchoredPosition.x;
        }

        protected override void Start()
        {
            base.Start();
            limitDistance = canvasRef.scaleFactor * 100;

            Button.interactable = false;
            Button.transform.localScale = Vector2.zero;

            UpdateCustomSprite(out Sprite[] NewSprites);
            if (NewSprites != null)
            {
                for (int i = 0; i < sprites.Length; i++)
                {
                    sprites[i] = NewSprites[i];
                }
                ClockSpecial = NewSprites[4];
                SpeedSpecial = NewSprites[5];
            }
        }

        private void OnCounter()
        {
            CreateRound();
            Timer.Play();
            IsReset = false;
            ContainerImage.DOScale(1, 0.5f).SetEase(Ease.OutBack).OnComplete(() => ImageOfWord.transform.DOScale(1, 0.5f).SetEase(Ease.OutBack));
            ContainerFichas.GetComponent<RectTransform>().DOAnchorPosY(-325, 1f).OnComplete(() =>
            {
                for (int i = 0; i < FichaPos.Length; i++)
                {
                    BlockPos[i].SetParent(ContainerBlock);
                }
            });
            Button.transform.DOScale(Vector2.one, 0.5f).SetEase(Ease.OutBack).SetDelay(2f);
            Machine.DOAnchorPosY(_leftLimit.y, 1f).OnComplete(() => { Button.interactable = true; IsPlay = true; /* MovementMachine(true); */});
        }

        protected override void Update()
        {
            base.Update();

            if (!IsPlay) return; // speed 0

            _shuffleTimer += Time.deltaTime;
            if (_shuffleTimer >= _shuffletime)
            {
                InitShuffle();
                _shuffleTimer -= _shuffletime;
            }
            if (IsSpeedUp)
            {
                _speedupTimer += Time.deltaTime;
                if (_speedupTimer > speedupSeconds) StopSpeedUp();
            }
            _delta = joystick.Horizontal;
            if (_delta > THRESHOLD) speed = Mathf.Lerp(IsSpeedUp ? movespeed * speedUpMultiplicator : movespeed, 1, 10 * Time.deltaTime);
            else if (_delta < -THRESHOLD) speed = Mathf.Lerp(IsSpeedUp ? -movespeed * speedUpMultiplicator : -movespeed, -1, 10 * Time.deltaTime);
            else speed = Mathf.Lerp(speed, 0, stopspeed * Time.deltaTime);

            Machine.anchoredPosition = new Vector2(Machine.anchoredPosition.x + speed, Machine.anchoredPosition.y);

            if (Machine.anchoredPosition.x < _leftLimit.x) Machine.anchoredPosition = _leftLimit;
            if (Machine.anchoredPosition.x > _rightLimit.x) Machine.anchoredPosition = _rightLimit;
        }
        public void ConfigDifficult()
        {
            var level = craneData.Get(Session.GetLevel());
            _time = level.time;
            roundsQuantity = level.rounds_quantity;
            roundTargets = level.target_quantity;
            correctTarget = level.correct_targets;
            itemClockpercent = level.itemClockPercent;
            ClockValue = level.ClockValue;
            itemSpeedPercent = level.itemSpeedPercent;
            speedupSeconds = level.SpeedUpSeconds;
            speedUpMultiplicator = level.SpeedUpMultiplicator;
            _shuffletime = level.ShuffleTime;
            movespeed = level.moveSpeed;
            stopspeed = level.stopSpeed;
        }
        private void ResetHealth() => SetHealth(3);
        private void CreateRound()
        {
            RemoveAllChildren(ContainerFichas);
            CountToWin = 0;
            bool Clock = GetProbability(itemClockpercent);
            bool Speed = GetProbability(itemSpeedPercent);
            CorrectSprite = GetRoundSprite();
            ImageOfWord.sprite = CorrectSprite;
            quantityCorrect = UnityEngine.Random.Range(1, correctTarget + 1);
            quantityIncorrect = roundTargets - quantityCorrect;
            FichaPos = new Transform[quantityCorrect + quantityIncorrect];
            BlockPos = new Transform[quantityCorrect + quantityIncorrect];
            for (int i = 0; i < quantityCorrect; i++)
            {
                CreateItem(CorrectSprite, "0", i);
            }
            int itemSpecial = 0;
            if (Clock) itemSpecial += 1;
            if (Speed) itemSpecial += 1;
            quantityIncorrect -= itemSpecial;
            for (int i = 0; i < quantityIncorrect; i++)
            {
                CreateItem(GetRoundSprite(), "1", quantityCorrect + i);
            }
            if (Clock) { CreateItem(ClockSpecial, "2", FichaPos.Length - itemSpecial); itemSpecial--; }
            if (Speed) CreateItem(SpeedSpecial, "3", FichaPos.Length - itemSpecial);
        }
        private void CreateItem(Sprite sprt, string name, int indexArray)
        {
            GameObject obj = Instantiate(FichaPrefab, ContainerFichas);
            obj.transform.Find("Block").Find("Image").GetComponent<Image>().sprite = sprt;
            obj.name = name;
            Sort(obj.transform);
            FichaPos[indexArray] = obj.transform;
            BlockPos[indexArray] = obj.transform.Find("Block");
        }
        void GoToFicha()
        {
            IsPlay = false;
            Button.interactable = false;
            float posX = Machine.anchoredPosition.x;
            float velocity = 100f * movespeed;
            Reference1 = Machine.DOAnchorPos(new Vector3(posX, -190, 0), IsSpeedUp ? velocity * speedUpMultiplicator : velocity).SetEase(Ease.Linear).SetSpeedBased().OnComplete(() =>
            {
                if (IsReset) return;
                IsFichaSelected = GetFicha();
                Reference1 = Machine.DOAnchorPosY(300, IsSpeedUp ? velocity * speedUpMultiplicator : velocity).SetEase(Ease.Linear).SetSpeedBased().OnComplete(() =>
                {
                    if (IsReset) return;
                    if (IsFichaSelected)
                    {
                        float diference = Machine.position.x - tempFicha.position.x;
                        Reference2 = Machine.DOAnchorPosX(containerXPos + diference, IsSpeedUp ? velocity * speedUpMultiplicator : velocity).SetEase(Ease.Linear).SetSpeedBased().OnComplete(() =>
                        {
                            if (IsReset) return;
                            tempFicha.SetParent(ContainerBlock);
                            Reference1 = tempFicha.DOAnchorPosY(-150, 0.75f).OnComplete(() =>
                            {
                                Validation();
                            });
                        });
                    }
                    else
                    {
                        IsPlay = true;
                        Button.interactable = true;
                        IsFichaSelected = false;
                    }
                });
            });
        }

        void Validation()
        {
            Reference2.Kill();
            if (tempParentFicha.name == "0")
            {
                tempFicha.localScale = Vector2.zero;
                CountToWin++;
                if (!IsFullHealth) AddHealth();
                AddScore(1, 0);
                ContainerImage.DOScale(1.2f, 0.5f).SetEase(Ease.OutBack).OnComplete(() =>
                {

                    ContainerImage.DOScale(1f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
                    {
                        if (CountToWin >= quantityCorrect)
                        {
                            if (energyBar.GetActualValue() < 1)
                            {
                                NextRound();
                            }
                        }
                    });
                });
                if (CountToWin >= quantityCorrect)
                {
                    Timer.Pause();
                    if (IsSpeedUp) StopSpeedUp();
                    energyBar.PlusBar();
                    return;
                }
            }
            else
            {
                if (tempParentFicha.name == "2" || tempParentFicha.name == "3")
                {
                    if (tempParentFicha.name == "3")
                    {
                        InitSpeedUp();
                    }
                    else Timer.PlusTime(ClockValue);

                    AudioManager.instance.PlaySFX("Star");
                    tempFicha.Find("Image").GetComponent<Image>().sprite = GetRoundSprite();
                    tempParentFicha.name = "1";
                }
                else
                {
                    AudioManager.instance.PlaySFX("lose");
                    SubtractHealth();
                }
                tempFicha.localScale = Vector2.zero;
                tempFicha.position = tempParentFicha.Find("base").transform.position;
                tempFicha.transform.DOScale(1f, 0.5f).SetEase(Ease.OutBack);
            }

            if (GetHealthValue > 0)
            {
                Reference2 = Machine.DOAnchorPosX(550, 0.5f).OnComplete(() =>
                {
                    IsPlay = true;
                    Button.interactable = true;
                    IsFichaSelected = false;
                });
            }
        }
        bool GetFicha()
        {
            for (int i = 0; i < BlockPos.Length; i++)
            {
                if (Vector2.Distance(HookMachine.transform.position, BlockPos[i].position) < limitDistance)
                {
                    if (BlockPos[i].localScale.x < 0.5f) return false;
                    AudioManager.instance.PlaySFX("Star");
                    tempParentFicha = FichaPos[i];
                    tempFicha = BlockPos[i].GetComponent<RectTransform>();
                    tempFicha.transform.SetParent(Machine.transform);
                    return true;
                }
            }
            return false;
        }
        private void HideRound()
        {
            Button.interactable = false;
            Button.transform.DOScale(0, 0.5f);
            ContainerImage.DOScale(0, 0.5f);
            Machine.DOScale(0, 0.5f);
            for (int i = 0; i < FichaPos.Length; i++) if (FichaPos[i] != null) BlockPos[i].SetParent(FichaPos[i]);
        }
        private void NextRound()
        {
            HideRound();
            ContainerFichas.GetComponent<RectTransform>().DOAnchorPosY(-924, 0.5f).OnComplete(() =>
            {
                RemoveAllChildren(ContainerFichas);
                CreateRound();
                ContainerImage.DOScale(1, 0.5f).SetEase(Ease.OutBack).OnComplete(() => ImageOfWord.transform.DOScale(1, 0.5f).SetEase(Ease.OutBack));
                ContainerFichas.GetComponent<RectTransform>().DOAnchorPosY(-325, 1f).OnComplete(() =>
                {
                    for (int i = 0; i < FichaPos.Length; i++) BlockPos[i].SetParent(ContainerBlock);
                });

                Button.transform.localScale = Vector2.zero;
                Button.transform.DOScale(Vector2.one, 0.5f).SetEase(Ease.OutBack);
                Timer.Play();
                Machine.anchoredPosition = new Vector2(_leftLimit.x, 794);
                Machine.DOScale(1, 0.5f);
                Machine.DOAnchorPosY(300, 1f).OnComplete(() => { Button.interactable = true; IsPlay = true; });
            });
        }
        private void ResetRound(bool youWin = false)
        {
            HideRound();
            ContainerFichas.GetComponent<RectTransform>().DOAnchorPosY(-924, 0.5f).OnComplete(() =>
            {
                RemoveAllChildren(ContainerFichas);
                Button.transform.localScale = Vector2.zero;
                Machine.anchoredPosition = new Vector2(_leftLimit.x, 794);
                Machine.DOScale(1, 0.5f);
                if (EndQuest == true)
                {
                    EndQuest = false;
                    ResetValues();
                    ShowMessageAndCounter();
                    return;
                }
                if (youWin)
                {
                    ResetValues();
                    ShowMessageAndCounter();
                }
                else
                {
                    //ShowFail("Vuelve a intentarlo.", () => { ResetValues(); ShowMessageAndCounter(); });
                    ShowFail(countReset > 1 ? "Ahora responde las siguientes preguntas." : "Vuelve a intentarlo.", () => { ResetValues(); ShowQuestion(); }, () =>
                    {
                        ResetValues(); ShowMessageAndCounter();
                    });
                }
            });
        }
        private void ShowQuestion()
        {
            LaunchQuestion(currentQuestian, (bool ok) =>
            {
                if (ok) currentCorrect++;
                currentQuestian++;
                if (currentQuestian >= 5)
                {
                    /*if (!ok)
                    {
                        RestartScore();
                        ShowFail("No lo lograste.", ValidateRound);
                        return;
                    }
                    currentCorrect++;
                    ShowCorrectMoney("Lo lograste", $"Respondiste correctamente, muy bien.", ValidateRound);*/
                    ValidateRound();
                }
                else Utils.AfterTime(this, 1, ShowQuestion);
            }, currentQuestian < 4 ? false : true);
        }
        private void ValidateRound()
        {
            _shuffleTimer = 0;
            if (currentQuestian < 5)
            {
                energyBar.ResetEnergyBar();
                energyBar.Config(roundsQuantity);
                Reference2.Kill();
                EndQuest = true;
                ResetRound();
            }
            else ShowFinalPopup(currentCorrect);
        }
        private Sprite GetRoundSprite()
        {
            Sprite test = sprites[UnityEngine.Random.Range(0, sprites.Length)];
            if (test == CorrectSprite)
                return GetRoundSprite();
            else return test;
        }
        private bool GetProbability(int percent)
        {
            float rnd = UnityEngine.Random.Range(0, 101);

            if (rnd < percent) return true;
            else return false;
        }
        private void InitSpeedUp()
        {
            AudioManager.instance.PlayBgm(speedSong);
            IsSpeedUp = true;
        }
        private void StopSpeedUp()
        {
            PlayBGM();
            IsSpeedUp = false;
            _speedupTimer = 0;
        }
        private void InitShuffle()
        {
            if (IsPlay) StartCoroutine("Shuffle");
        }
        GameObject[] temp = null;
        private IEnumerator Shuffle()
        {
            if (IsPlay == true)
            {
                AudioManager.instance.PlaySFX("Star");
                Button.interactable = false;
                temp = new GameObject[FichaPos.Length];
                for (int i = 0; i < FichaPos.Length; i++) temp[i] = ContainerFichas.GetChild(i).gameObject;
                do
                {
                    for (int i = 0; i < FichaPos.Length; i++)
                    {
                        Sort(FichaPos[i]);
                    }
                } while (CheckShuffle());
                Canvas.ForceUpdateCanvases();
                for (int i = 0; i < FichaPos.Length; i++)
                {
                    BlockPos[i].DOMoveX(FichaPos[i].Find("base").position.x, 0.5f);
                }
                yield return new WaitForSeconds(0.5f);
                Button.interactable = true;
            }
        }
        private bool CheckShuffle()
        {
            for (int i = 0; i < FichaPos.Length; i++)
            {
                if (temp[i] != ContainerFichas.GetChild(i).gameObject) return false;
            }
            return true;
        }
        private void ResetValues()
        {
            energyBar.ResetEnergyBar();
            energyBar.Config(roundsQuantity);
            Reference2.Kill();
            Timer.SetTimer(_time);
            ResetHealth();
        }
        public void Sort(Transform t) => t.SetSiblingIndex(UnityEngine.Random.Range(0, t.parent.childCount + 1));
        public void RemoveAllChildren(Transform t)
        {
            foreach (Transform child in t)
            {
                UnityEngine.Object.Destroy(child.gameObject);
            }
        }

        protected override void ConfigGame()
        {
            ConfigDifficult();
            roundInstruction = $"Recoge los s�mbolos que representan las acciones positivas.";
            Timer.SetTimer(_time, () =>
            {
                AudioManager.instance.PlaySFX("lose");
                OnDie();
            });
            energyBar.Config(roundsQuantity, () =>
            {
                Timer.Pause();
                ShowPopUp("Ganaste", $"Ahora responde las siguientes preguntas.", 0, () =>
                {
                    ShowQuestion();
                }, false, null);
                /* PrepareToReset();
                    ResetRound(true);
                 */
            });
        }

        private void PrepareToReset()
        {
            IsPlay = false;
            IsReset = true;
            _shuffleTimer = 0;
            _speedupTimer = 0;
            Timer.Pause();
            RestartScore();
            StopSpeedUp();
            Invoke("ResetHealth", 1);
            Reference2.Kill();
            Reference1.Kill();
        }
        protected override void InitGame()
        {
            OnCounter();
        }

        protected override void OnDie()
        {
            PrepareToReset();
            ResetRound();
        }
    }

}
