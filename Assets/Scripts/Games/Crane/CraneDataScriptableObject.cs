using UnityEngine;

[CreateAssetMenu(fileName = "CraneData", menuName = "ScriptableObjects/CraneData", order = 1)]
public class CraneDataScriptableObject : ScriptableObject
{
    [System.Serializable]
    public struct LevelData
    {
        public int time;
        public int rounds_quantity;
        public int correct_targets;
        public int target_quantity;
        public int itemClockPercent;
        public int ClockValue;
        public int itemSpeedPercent;
        public float SpeedUpSeconds;
        public float SpeedUpMultiplicator;
        public int ShuffleTime;
        public int moveSpeed;
        public int stopSpeed;
    }

    [SerializeField] private LevelData[] levels = null;

    public LevelData Get(int level) => levels[Mathf.Clamp(level - 1, 0, levels.Length)];
}
