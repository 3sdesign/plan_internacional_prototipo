using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class FontChanger : EditorWindow
{
    [MenuItem("Sergiosoba/FontChanger")]
    public static void ShowWindow() => GetWindow<FontChanger>("Font Changer");

    private Font _font = null;

    private void OnGUI()
    {
        _font = (Font)EditorGUILayout.ObjectField("Font", _font, typeof(Font), true);

        if (GUILayout.Button("Change"))
        {
            Text[] texts = FindObjectsOfType<Text>();
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].font = _font;
            }
        }
    }
}
