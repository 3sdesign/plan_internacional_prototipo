using System.Collections;
using UnityEngine;
using static UnityEngine.SceneManagement.SceneManager;

public class RotateScreen : MonoBehaviour
{
    public enum ORIENTATION
    {
        LANDSCAPE,
        PORTRAIT
    }

    public enum FEEDBACK
    {
        NONE,
        ERROR,
        ONLINE,
        OFFLINE
    }

    private static string _scene = string.Empty;
    private static ORIENTATION _orientation = default;

    private static FEEDBACK _feedback = default;
    [SerializeField] private Sprite Error = null;
    [SerializeField] private Sprite Online = null;
    [SerializeField] private Sprite Offline = null;
    [SerializeField] private UnityEngine.UI.Image Character = null;

    public static void Load(string scene, ORIENTATION orientation, FEEDBACK feedback = FEEDBACK.NONE)
    {
        _scene = scene;
        _orientation = orientation;

        _feedback = feedback;

        LoadScene("Rotation");
    }

    private void Awake()
    {
        switch (_feedback)
        {
            case FEEDBACK.ERROR:
                Character.sprite = Error;
                break;
            case FEEDBACK.ONLINE:
                Character.sprite = Online;
                break;
            case FEEDBACK.OFFLINE:
                Character.sprite = Offline;
                break;
        }
    }

    private IEnumerator Start()
    {
        if (_orientation == ORIENTATION.LANDSCAPE)
        {
            RotationManager.ForceLandscape();
            yield return Screen.orientation == ScreenOrientation.Landscape;
        }
        else
        {
            RotationManager.ForcePortrait();
            yield return Screen.orientation == ScreenOrientation.Portrait;
        }

        yield return new WaitForSecondsRealtime(0.5f);
        Time.timeScale = 1.0f;
        LoadScene(_scene);
    }
}