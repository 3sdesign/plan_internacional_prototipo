using Paroxe.PdfRenderer;
using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PDFLaucher : MonoBehaviour
{
    [SerializeField] private PDFViewer Viewer = null;

    [Header("Buttons")]
    [SerializeField] private Button DownloadAndOpen = null;
    [SerializeField] private Button CloseViewer = null;

    private void Awake()
    {
        Viewer.gameObject.SetActive(false);

        Text buttonText = DownloadAndOpen.GetComponentInChildren<Text>();
        buttonText.text = PdfExists("logo") ? "Abrir PDF" : "Descargar PDF";

        DownloadAndOpen.onClick.AddListener(() =>
        {
            DownloadAndOpen.interactable = false;

            string file = "logo";

            if (PdfExists(file))
            {
                //Application.OpenURL(path);
                Viewer.FileName = file + ".pdf";
                Viewer.gameObject.SetActive(true);

                Screen.autorotateToPortrait = true;
                Screen.autorotateToLandscapeLeft = true;
                Screen.autorotateToLandscapeRight = true;
                Screen.autorotateToPortraitUpsideDown = true;
                Screen.orientation = ScreenOrientation.AutoRotation;
            }
            else
            {
                StartCoroutine(DownloadPDF(file, () =>
                {
                    buttonText.text = "Abrir PDF";
                    DownloadAndOpen.interactable = true;
                }));
            }
        });

        CloseViewer.onClick.AddListener(() =>
        {
            Viewer.gameObject.SetActive(false);
            DownloadAndOpen.interactable = true;

            Screen.autorotateToPortrait = false;
            Screen.autorotateToLandscapeLeft = true;
            Screen.autorotateToLandscapeRight = true;
            Screen.autorotateToPortraitUpsideDown = false;
            Screen.orientation = ScreenOrientation.Landscape;
            Screen.orientation = ScreenOrientation.AutoRotation;
        });
    }

    private bool PdfExists(string file) => File.Exists(Application.persistentDataPath + "/" + file + ".pdf");

    private IEnumerator DownloadPDF(string file, Action onDownload)
    {
        string uri = "https://gameanimationlabcampus.com/" + file + ".pdf";

        using (UnityWebRequest www = UnityWebRequest.Get(uri))
        {
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.Success)
            {
                string path = Application.persistentDataPath + "/" + file + ".pdf";
                File.WriteAllBytes(path, www.downloadHandler.data);

#if UNITY_EDITOR
                UnityEditor.AssetDatabase.Refresh();
#endif
                onDownload();
            }
            else print(www.error);
        }
    }
}
